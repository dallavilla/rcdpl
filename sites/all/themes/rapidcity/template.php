<?php

/**
 * @file
 * template.php
 */

/**
	* Prints breadcrumbs based on url separators and includes current page
	*/
function get_breadcrumb(){
  global $base_url;
  $breadcrumb = array();
  $path = $_SERVER['REQUEST_URI'];
  $firstpath = explode('?',$path);
  $paths = explode('/',$firstpath[0]);
  $actualpath = '';
  $last = end($paths);
  foreach($paths as $test){
    $actualpath .= $test. '/';
    if($test == $last) {
      $breadcrumb[] = t(ucwords(str_replace("-"," ",$test)));
    }
    else {
      $breadcrumb[] = l(ucwords(str_replace("-"," ",$test)), rtrim($base_url.$actualpath, "/"));
    }
  }
  $breadcrumb[0] = l('Home','<front>');
  return $breadcrumb;
}

/**
	* Trims text field by word length
	*/
function rapidcity_summarise($paragraph, $limit) {
  $textfield = strtok($paragraph, " ");
  while ($textfield) {
    $text .= " $textfield";
    $words++;
    if (($words >= $limit) && ((substr($textfield, -1) == "!") || (substr($textfield, -1) == "."))) {
      break;
    }
    $textfield = strtok(" ");
  }
  return ltrim($text);
}

function rapidcity_preprocess_page(&$vars) {
  $header = drupal_get_http_header("status");  
  if($header == "404 Not Found") {      
    $vars['theme_hook_suggestions'][] = 'page__404';
  }
  if($header == "403 Forbidden") {      
    $vars['theme_hook_suggestions'][] = 'page__403';
  }

  if (isset ($vars['node'])) {
    if (!is_array($vars['theme_hook_suggestions'])) {
      $vars['theme_hook_suggestions'] = array();
    }
  $vars['theme_hook_suggestions'][] = 'page__'.$vars['node']->type;
  }
  
    // Do we have a node?
  if (isset($vars['node'])) {

    // Ref suggestions cuz it's stupid long.
    $suggests = &$vars['theme_hook_suggestions'];

    // Get path arguments.
    $args = arg();
    // Remove first argument of "node".
    unset($args[0]);

    // Set type.
     $type = "page__type_{$vars['node']->type}";

    // Bring it all together.
    $suggests = array_merge(
      $suggests,
      array($type),
      theme_get_suggestions($args, $type)
    );

    // if the url is: 'http://domain.com/node/123/edit'
    // and node type is 'blog'..
    // 
    // This will be the suggestions:
    //
    // - page__node
    // - page__node__%
    // - page__node__123
    // - page__node__edit
    // - page__type_blog
    // - page__type_blog__%
    // - page__type_blog__123
    // - page__type_blog__edit
    // 
    // Which connects to these templates:
    //
    // - page--node.tpl.php
    // - page--node--%.tpl.php
    // - page--node--123.tpl.php
    // - page--node--edit.tpl.php
    // - page--type-blog.tpl.php          << this is what you want.
    // - page--type-blog--%.tpl.php
    // - page--type-blog--123.tpl.php
    // - page--type-blog--edit.tpl.php
    // 
    // Latter items take precedence.
    
    
     // Add the terms to the body classes for MY_NODE_TYPE'.

  if (drupal_get_path_alias("node/{$vars['#node']->nid}") == 'big-happenings') {
    drupal_add_css(drupal_get_path('theme', 'rapidcity') . "/css/big-happenings.css");
  }


  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_term_load(arg(2));
    $vars['theme_hook_suggestions'][] = 'page__vocabulary__' . $term->vocabulary_machine_name;
  }  
    
  }
  

  
     //Page template suggestions based off of content types
   // if (isset($variables['node'])) { 
   //              $variables['theme_hook_suggestions'][] = 'page__type__'. $variables['node']->type;
   //              $variables['theme_hook_suggestions'][] = "page__node__" . $variables['node']->nid;
   // }
   
   // Page template suggestions based off URL alias
   if (module_exists('path')) {
    $alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
    if ($alias != $_GET['q']) {
      $template_filename = 'page';
      foreach (explode('/', $alias) as $path_part) {
        $template_filename = $template_filename . '__' . $path_part;
        $vars['theme_hook_suggestions'][] = $template_filename;
      }
    }
  }
   
}


/* Removes the Date module's "(All day)" text */
function rapidcity_date_all_day_label() {
  return t('');
}



/**
 * Implements hook_views_query_alter
 *
 * @param type $view
 * @param type $query
 */
function rapidcity_views_query_alter(&$view, &$query) {
  // Fix bug where multiple terms in search index filter returns no results
  if (isset($query->relationships['search_total'])
      && !empty($query->fields['score'])
      && empty($query->fields['score']['table'])) {
    $query->fields['score']['table'] = 'search_total';
  }
}





 ?>