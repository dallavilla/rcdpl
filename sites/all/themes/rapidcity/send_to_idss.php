<?php

header('Location:/email-thank-you');
   
if(isset($_POST['salutation'])) { $salutation = $_POST['salutation']; } else { $salutation = ''; }
if(isset($_POST['firstName'])) { $firstName = $_POST['firstName']; } else { $firstName = ''; }
if(isset($_POST['middleName'])) { $middleName = $_POST['middleName']; } else { $middleName = ''; }
if(isset($_POST['lastName'])) { $lastName = $_POST['lastName']; } else { $lastName = ''; }
if(isset($_POST['title'])) { $title = $_POST['title']; } else { $title = ''; }
if(isset($_POST['companyName'])) { $companyName = $_POST['companyName']; } else { $companyName = ''; }
if(isset($_POST['phone'])) { $phone = $_POST['phone']; } else { $phone = ''; }
if(isset($_POST['homePhone'])) { $homePhone = $_POST['homePhone']; } else { $homePhone = ''; }
//if(isset($_POST['faxPhone'])) { $faxPhone = $_POST['faxPhone']; } else { $faxPhone = ''; }
if(isset($_POST['emailFrom'])) { $emailFrom = $_POST['emailFrom']; } else { $emailFrom = ''; }
if(isset($_POST['otherEmail'])) { $otherEmail = $_POST['otherEmail']; } else { $otherEmail = ''; }
if(isset($_POST['street'])) { $street = $_POST['street']; } else { $street = ''; }
if(isset($_POST['suite'])) { $suite = $_POST['suite']; } else { $suite = ''; }
if(isset($_POST['city'])) { $city = $_POST['city']; } else { $city = ''; }
if(isset($_POST['countryAbbr'])) { $countryAbbr = $_POST['countryAbbr']; } else { $countryAbbr = ''; }
if( $countryAbbr == '' ){ $countryAbbr='US'; }
if(isset($_POST['stateAbbr'])) { $stateAbbr = $_POST['stateAbbr']; } else { $stateAbbr = ''; }
if( $stateAbbr == 'Other-International' ){ $stateAbbr = $_POST['countryAbbr']; }
if(isset($_POST['postalCode'])) { $postalCode = $_POST['postalCode']; } else { $postalCode = ''; }
if(isset($_POST['comments'])) { $comments = $_POST['comments']; } else { $comments = ''; }


// distributionListIDs from Rapid City IDSS
$newsletterType = $_POST['newsletterType'];
		// Website Newsletter = 844
		// Website Newsletter - Meetings = 796
		// Website Newsletter - Sports = 797
		// Website Newsletter - Travel = 798


$clientWS = new SoapClient('http://ws.idssasp.com/Prospects.asmx?wsdl');
$namespaceWS = 'http://ws.idssasp.com/Prospects.asmx';

$dmsClientU = 'rapidcity';
$dmsClientP = '783889d7-a690-42f1-b54c-1bad2fe0e852';

$headerBodyWS = array('UserName' => $dmsClientU, 'Password' => $dmsClientP);
$headerWS = new SOAPHeader($namespaceWS, 'AuthorizeHeader', $headerBodyWS, false);
$clientWS->__setSoapHeaders(array($headerWS));

$parameters = array('salutation' => $salutation, 'firstName' => $firstName, 'middleName' => $middleName, 'lastName' => $lastName, 'title' => $title, 'companyName' => $companyName, 'phone' => $phone, 'homePhone' => $homePhone, 'mobilePhone' => '', 'fax' => '', 'email' => $emailFrom, 'otherEmail' => $otherEmail, 'street' => $street, 'suite' => $suite, 'city' => $city, 'countryAbbr' => $countryAbbr, 'stateAbbr' => $stateAbbr, 'postalCode' => $postalCode, 'sourceID' => 1796, 'comments' => $comments, 'validateCountryState' => 'false' );

$results = $clientWS->CreateProspect($parameters);

$newID = get_object_vars($results);
$newNum = $newID[CreateProspectResult];

//print_r($newNum);
//print_r('<br/><br/>');

$parametersd = array('prospectID' => $newNum, 'distributionListID' => $newsletterType, );
$resultsd = $clientWS->AddProspectToDistributionList($parametersd);

$today = date('Y-m-d');

$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Newsletter _ Leisure', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, 'sentViaCode' => 'Email', 'sentDate' => $today, 'sentBy' => 'Jessica Ross', );
$resultsk = $clientWS->CreateProspectRequestWithSentVia($parametersk);

//print_r( $resultsd );


exit();


?>