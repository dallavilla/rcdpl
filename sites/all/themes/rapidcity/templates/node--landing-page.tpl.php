<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="row">
		<?php if($node->field_video): ?>
		<div class="col-md-6">
		<?php else: ?>
		<div class="col-md-12">
		<?php endif; ?>
			<?php print render($content['body']); ?>
		</div>
		<?php if($node->field_video): ?>
		<div class="col-md-6">
				<div class="video-container">
					<iframe src="//www.youtube.com/embed/<?php print($node->field_video['und'][0]['value']); ?>?controls=0&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
		</div>
		<?php endif; ?>
	</div>
	
	<?php
		// Add Listing/Event Category Squares
		if (drupal_get_path_alias(current_path()) == 'events') {
			//print views_embed_view('listing_categories','event_categories');
		} else if (drupal_get_path_alias(current_path()) == 'things-to-do/attractions') {
			print views_embed_view('listing_categories','attractions_categories');
		} else if (drupal_get_path_alias(current_path()) == 'dining') {
			print views_embed_view('listing_categories','dining_categories');
		} else if (drupal_get_path_alias(current_path()) == 'stay') {
			print views_embed_view('listing_categories','stay_categories');
		}
	?>

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>

</article> <!-- /.node -->
