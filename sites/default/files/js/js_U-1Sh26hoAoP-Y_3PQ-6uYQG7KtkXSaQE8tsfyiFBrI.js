/* ===========================================================
 * jquery.zoom-scroller.js v1.0
 * ===========================================================
 * Copyright 2014 Pete Rojwongsuriya.
 * http://www.thepetedesign.com
 *
 * Image will smoothly zooms in/out as you scroll through them
 * creating a very nice subtle animation that grabs attention
 *
 * https://github.com/peachananr/zoom-scroller
 * 
 * License: GPL v3
 *
 * ========================================================== */

!function($){
  
  var defaults = {
    zoom: 1,
    initZoom: 1.15,
    animationTime: 2000,
    easing: "ease",
    onZoom: null,
    beforeZoom: null,
    afterZoom: null,
    offsetTop: 0,
    offsetBottom: 200,
  };  
  
  
  $.fn.zoomScroller = function(options){
    return this.each(function(){
      var settings = $.extend({}, defaults, options),
          el = $(this),
          originY = 0,
          bg = el.find("> img");

      // Wrap all list items in a scroller to be used to scroll    
      el.addClass("zs-wrapper").css("overflow", "hidden").prepend("<div class='zs-img'></div>");
      bg.remove();
      var img = el.find("> .zs-img");
      
      img.css({
        background: "url(" + bg.attr("src") + ") center center no-repeat",
        "background-size": "cover",
        overflow: "hidden",
        width: "100%",
        height: "100%",
        "-webkit-transform": "scale(" + settings.initZoom + ")",
        "-moz-transform": "scale(" + settings.initZoom + ")",
        "-o-transform": "scale(" + settings.initZoom + ")",
        "transform": "scale(" + settings.initZoom + ")",
        "-webkit-transition": "-webkit-transform " + settings.animationTime + "ms " + settings.easing,
        "-moz-transition": "-moz-transform " + settings.animationTime + "ms " + settings.easing,
        "-ms-transition": "-o-transform " + settings.animationTime + "ms " + settings.easing,
        "transition": "transform " + settings.animationTime + "ms " + settings.easing
      });
  
      // Swipe Support
      var debut,
          isTouching = false;
      $("body").on('touchstart', function() {
        if (event.touches.length == 1) {
          debut = event.touches[0].pageY;
          isTouching = true;
        }
      });   
      
      $("body").on('touchend', function() {
        isTouching = false;
        debut = null;
      })
      
      // bind on scroll to create zoom effect on the background 
      $(document).on('touchmove mousewheel DOMMouseScroll', function(e, delta) {
        originY = $(document).scrollTop();
        
        // Zoom startes/stops only when object is on screen
        if (el.is_on_screen($(document).scrollTop())) {     
          if (typeof settings.beforeZoom == 'function') settings.beforeZoom(img.parent(), "in"); 
          img.css({
            "-webkit-transform": "scale(" + settings.zoom + ")",
            "-moz-transform": "scale(" + settings.zoom + ")",
            "-o-transform": "scale(" + settings.zoom + ")",
            "transform": "scale(" + settings.zoom + ")"
          }).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
            if (typeof settings.afterZoom == 'function') settings.afterZoom(img.parent(), "in"); 
          });
          if (typeof settings.onZoom == 'function') settings.onZoom(img.parent(), "in"); 
        } else {
          if (typeof settings.beforeZoom == 'function') settings.beforeZoom(img.parent(), "out"); 
          img.css({
            "-webkit-transform": "scale(" + settings.initZoom + ")",
            "-moz-transform": "scale(" + settings.initZoom + ")",
            "-o-transform": "scale(" + settings.initZoom + ")",
            "transform": "scale(" + settings.initZoom + ")"
          }).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
            if (typeof settings.afterZoom == 'function') settings.afterZoom(img.parent(), "in"); 
          });
          if (typeof settings.onZoom == 'function') settings.onZoom(img.parent(), "out"); 
        }
      
      });
      
      // Function Check if the when the element appears on the screen
      
      $.fn.is_on_screen = function(originY){
        var win = el;
        var viewport = {
            top : originY
        };

        viewport.bottom = viewport.top + $(window).height() ;
        
        var bounds = this.offset();
        
        
        bounds.top = this.offset().top;
        bounds.bottom = this.offset().top + this.height();
        return (!(viewport.bottom - settings.offsetBottom < bounds.top || viewport.top - settings.offsetTop > bounds.bottom ));
      };
      
      
    });
    
  }
  

}(window.jQuery);;
/*
 * jQuery.autopager v1.0.0
 *
 * Copyright (c) lagos
 * Dual licensed under the MIT and GPL licenses.
 */
(function($) {
	var window = this, options = {},
		content, currentUrl, nextUrl,
		active = false,
		defaults = {
			autoLoad: true,
			page: 1,
			content: '.content',
			link: 'a[rel=next]',
			insertBefore: null, 
			appendTo: null, 
			start: function() {},
			load: function() {},
			disabled: false
		};

	$.autopager = function(_options) {
		var autopager = this.autopager;

		if (typeof _options === 'string' && $.isFunction(autopager[_options])) {
			var args = Array.prototype.slice.call(arguments, 1),
				value = autopager[_options].apply(autopager, args);

			return value === autopager || value === undefined ? this : value;
		}

		_options = $.extend({}, defaults, _options);
		autopager.option(_options);

		content = $(_options.content).filter(':last');
		if (content.length) {
			if (!_options.insertBefore && !_options.appendTo) {
				var insertBefore = content.next();
				if (insertBefore.length) {
					set('insertBefore', insertBefore);
				} else {
					set('appendTo', content.parent());
				}
			}
		}

		setUrl();

		return this;
	};

	$.extend($.autopager, {
		option: function(key, value) {
			var _options = key;

			if (typeof key === "string") {
				if (value === undefined) {
					return options[key];
				}
				_options = {};
				_options[key] = value;
			}

			$.each(_options, function(key, value) {
				set(key, value);
			});
			return this;
		},

		enable: function() {
			set('disabled', false);
			return this;
		},

		disable: function() {
			set('disabled', true);
			return this;
		},

		destroy: function() {
			this.autoLoad(false);
			options = {};
			content = currentUrl = nextUrl = undefined;
			return this;
		},

		autoLoad: function(value) {
			return this.option('autoLoad', value);
		},

		load: function() {
			if (active || !nextUrl || options.disabled) {
				return;
			}

			active = true;
			options.start(currentHash(), nextHash());
			$.get(nextUrl, insertContent);
			return this;
		}

	});

	function set(key, value) {
		switch (key) {
			case 'autoLoad':
				if (value && !options.autoLoad) {
					$(window).scroll(loadOnScroll);
				} else if (!value && options.autoLoad) {
					$(window).unbind('scroll', loadOnScroll);
				}
				break;
			case 'insertBefore':
				if (value) {
					options.appendTo = null;
				}
				break
			case 'appendTo':
				if (value) {
					options.insertBefore = null;
				}
				break
		}
		options[key] = value;
	}

	function setUrl(context) {
		currentUrl = nextUrl || window.location.href;
		nextUrl = $(options.link, context).attr('href');
	}

	function loadOnScroll() {
		if (content.offset().top + content.height() < $(document).scrollTop() + $(window).height()) {
			$.autopager.load();
		}
	}

	function insertContent(res) {
		var _options = options,
			nextPage = $('<div/>').append(res.replace(/<script(.|\s)*?\/script>/g, "")),
			nextContent = nextPage.find(_options.content); 

		set('page', _options.page + 1);
		setUrl(nextPage);
		if (nextContent.length) {
			if (_options.insertBefore) {
				nextContent.insertBefore(_options.insertBefore);
			} else {
				nextContent.appendTo(_options.appendTo);
			}
			_options.load.call(nextContent.get(), currentHash(), nextHash());
			content = nextContent.filter(':last');
		}
		active = false;
	}

	function currentHash() {
		return {
			page: options.page,
			url: currentUrl
		};
	}

	function nextHash() {
		return {
			page: options.page + 1,
			url: nextUrl
		};
	}
})(jQuery);
;
// $Id:

(function ($) {
var views_infinite_scroll_was_initialised = false;
Drupal.behaviors.views_infinite_scroll = {
  attach:function() {
    // Make sure that autopager plugin is loaded
    if($.autopager) {
      if(!views_infinite_scroll_was_initialised) {
        views_infinite_scroll_was_initialised = true;
        // There should not be multiple Infinite Scroll Views on the same page
        if(Drupal.settings.views_infinite_scroll.length == 1) { 
          var settings = Drupal.settings.views_infinite_scroll[0];
          var use_ajax = false;
          // Make sure that views ajax is disabled
          if(Drupal.settings.views && Drupal.settings.views.ajaxViews) {
            $.each(Drupal.settings.views.ajaxViews, function(key, value) {
              if((value.view_name == settings.view_name) && (value.view_display_id == settings.display)) {
                use_ajax = true;
              }
            });
          }
          if(!use_ajax) {
            var view_selector    = 'div.view-id-' + settings.view_name + '.view-display-id-' + settings.display;
            var content_selector = view_selector + ' > ' + settings.content_selector;
            var items_selector   = content_selector + ' ' + settings.items_selector;
            var pager_selector   = view_selector + ' > div.item-list ' + settings.pager_selector;
            var next_selector    = view_selector + ' ' + settings.next_selector;
            var img_location     = view_selector + ' > div.view-content';
            var img_path         = settings.img_path;
            var img              = '<div id="views_infinite_scroll-ajax-loader"><img src="' + img_path + '" alt="loading..."/></div>';
            $(pager_selector).hide();
            var handle = $.autopager({
              appendTo: content_selector,
              content: items_selector,
              link: next_selector,
              page: 0,
              start: function() {
                $(img_location).after(img);
              },
              load: function() {
                $('div#views_infinite_scroll-ajax-loader').remove();
				addthis.toolbox('.addthis_toolbox');
                Drupal.attachBehaviors(this);
              }
            });

            // Trigger autoload if content height is less than doc height already
            var prev_content_height = $(content_selector).height();
            do {
              var last = $(items_selector).filter(':last');
              if(last.offset().top + last.height() < $(document).scrollTop() + $(window).height()) {
                last = $(items_selector).filter(':last');
                handle.autopager('load');
              }
              else {
                break;
              }
            }
            while ($(content_selector).height() > prev_content_height);

          }
          else {  
            alert(Drupal.t('Views infinite scroll pager is not compatible with Ajax Views. Please disable "Use Ajax" option.'));
          }
        }
        else if(Drupal.settings.views_infinite_scroll.length > 1) {
          alert(Drupal.t('Views Infinite Scroll module can\'t handle more than one infinite view in the same page.'));
        }
      }
    }
    else {
      alert(Drupal.t('Autopager jquery plugin in not loaded.'));
    }
  }
}

})(jQuery);
;
(function ($) {
  Drupal.behaviors.viewsBootstrapCarousel = {
    attach: function(context, settings) {
      $(function () {
        $.each(settings.viewsBootstrap.carousel, function(id, carousel) {
          try {
            $('#views-bootstrap-carousel-' + carousel.id, context).carousel(carousel.attributes);
          }
          catch(err) {
            console.log(err);
          }
        });
      });
    }
  };
})(jQuery);
;
(function($){
/**
 * To make a form auto submit, all you have to do is 3 things:
 *
 * ctools_add_js('auto-submit');
 *
 * On gadgets you want to auto-submit when changed, add the ctools-auto-submit
 * class. With FAPI, add:
 * @code
 *  '#attributes' => array('class' => array('ctools-auto-submit')),
 * @endcode
 *
 * If you want to have auto-submit for every form element,
 * add the ctools-auto-submit-full-form to the form. With FAPI, add:
 * @code
 *   '#attributes' => array('class' => array('ctools-auto-submit-full-form')),
 * @endcode
 *
 * If you want to exclude a field from the ctool-auto-submit-full-form auto submission,
 * add the class ctools-auto-submit-exclude to the form element. With FAPI, add:
 * @code
 *   '#attributes' => array('class' => array('ctools-auto-submit-exclude')),
 * @endcode
 *
 * Finally, you have to identify which button you want clicked for autosubmit.
 * The behavior of this button will be honored if it's ajaxy or not:
 * @code
 *  '#attributes' => array('class' => array('ctools-use-ajax', 'ctools-auto-submit-click')),
 * @endcode
 *
 * Currently only 'select', 'radio', 'checkbox' and 'textfield' types are supported. We probably
 * could use additional support for HTML5 input types.
 */

Drupal.behaviors.CToolsAutoSubmit = {
  attach: function(context) {
    // 'this' references the form element
    function triggerSubmit (e) {
      var $this = $(this);
      if (!$this.hasClass('ctools-ajaxing')) {
        $this.find('.ctools-auto-submit-click').click();
      }
    }

    // the change event bubbles so we only need to bind it to the outer form
    $('form.ctools-auto-submit-full-form', context)
      .add('.ctools-auto-submit', context)
      .filter('form, select, input:not(:text, :submit)')
      .once('ctools-auto-submit')
      .change(function (e) {
        // don't trigger on text change for full-form
        if ($(e.target).is(':not(:text, :submit, .ctools-auto-submit-exclude)')) {
          triggerSubmit.call(e.target.form);
        }
      });

    // e.keyCode: key
    var discardKeyCode = [
      16, // shift
      17, // ctrl
      18, // alt
      20, // caps lock
      33, // page up
      34, // page down
      35, // end
      36, // home
      37, // left arrow
      38, // up arrow
      39, // right arrow
      40, // down arrow
       9, // tab
      13, // enter
      27  // esc
    ];
    // Don't wait for change event on textfields
    $('.ctools-auto-submit-full-form input:text, input:text.ctools-auto-submit', context)
      .filter(':not(.ctools-auto-submit-exclude)')
      .once('ctools-auto-submit', function () {
        // each textinput element has his own timeout
        var timeoutID = 0;
        $(this)
          .bind('keydown keyup', function (e) {
            if ($.inArray(e.keyCode, discardKeyCode) === -1) {
              timeoutID && clearTimeout(timeoutID);
            }
          })
          .keyup(function(e) {
            if ($.inArray(e.keyCode, discardKeyCode) === -1) {
              timeoutID = setTimeout($.proxy(triggerSubmit, this.form), 500);
            }
          })
          .bind('change', function (e) {
            if ($.inArray(e.keyCode, discardKeyCode) === -1) {
              timeoutID = setTimeout($.proxy(triggerSubmit, this.form), 500);
            }
          });
      });
  }
}
})(jQuery);
;
