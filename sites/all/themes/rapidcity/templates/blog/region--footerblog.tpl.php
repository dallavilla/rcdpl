<footer class="footer">
	<div class="container">

		<div class="foot-social-icons">
				<a class="foot-social-icon foot-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_foot_facebook.png" alt="Facebook" /></a>
				<a class="foot-social-icon foot-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_foot_twitter.png" alt="Twitter" /></a>
				<a class="foot-social-icon foot-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_foot_pinterest.png" alt="Pinterest" /></a>
				<a class="foot-social-icon foot-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_foot_youtube.png" alt="YouTube" /></a>
				<a class="foot-social-icon foot-instagram" href="http://instagram.com/VisitRapidCity" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_foot_instagram.png" alt="Instagram" /></a>
		</div>

		<p>Rapid City Convention &amp; Visitors Bureau<br />
		<a href="http://www.VisitRapidCity.com">www.VisitRapidCity.com</a></p>

		<p>444 Mt. Rushmore Road N. &bull; Rapid City, SD 57701<br />
		1-800-487-3223 &bull; 605-718-8484 &bull; Fax: 605-348-9217</p>

   		<div class="languages">					
			<a href="/languages/Español/" title="Español"><img src="/sites/all/themes/rapidcity/images/flag-spanish-small.png" alt="Español" /></a>
			<a href="/languages/Française-Canada/" title="Française Canada"><img src="/sites/all/themes/rapidcity/images/flag-french-canada-small.png" alt="Française Canada" /></a>
			<a href="/languages/Deutsch/" title="Deutsch"><img src="/sites/all/themes/rapidcity/images/flag-german-small.png" alt="Deutsch" /></a>
			<a href="/languages/Italiano/" title="Italiano"><img src="/sites/all/themes/rapidcity/images/flag-italian-small.png" alt="Italiano" /></a>
			<a href="/languages/Française-France/" title="Française France"><img src="/sites/all/themes/rapidcity/images/flag-french-france-small.png" alt="Française France" /></a>
			<a href="/languages/Chinese" title="漢語"><img src="/sites/all/themes/rapidcity/images/flag-chinese-small.png" alt="漢語" /></a>
			<a href="/languages/Japanese/" title="標準語"><img src="/sites/all/themes/rapidcity/images/flag-japanese-small.png" alt="標準語" /></a>
		</div>

		<div class="foot-logos">
			<a class="foot-logo" href="/" target="_blank" title="Do Big Things"><img src="/sites/all/themes/rapidcity/images/dobigthings_foot.png" alt="Do Big Things" /></a>
			<a class="foot-logo" href="http://www.discoveramerica.com/usa/states/south-dakota.aspx" target="_blank" title="USA DiscoverAmerica.com"><img src="/sites/all/themes/rapidcity/images/usa_logo.png" alt="USA DiscoverAmerica.com" /></a>
			<a class="foot-logo" href="http://www.tripadvisor.com/Tourism-g54774-Rapid_City_South_Dakota-Vacations.html" target="_blank" title="Trip Advisor"><img src="/sites/all/themes/rapidcity/images/tripadvisor.png" alt="Trip Advisor" /></a>
			<a class="foot-logo" href="https://www.travelsouthdakota.com/" target="_blank" title="Travel South Dakota"><img src="/sites/all/themes/rapidcity/images/SDT_FacesGFGP_wTag_BLK.png" alt="Travel South Dakota" style="max-width: 188px;" /></a>

		</div>
		
	</div>
</footer>