(function ($) {

/**
 * A progressbar object. Initialized with the given id. Must be inserted into
 * the DOM afterwards through progressBar.element.
 *
 * method is the function which will perform the HTTP request to get the
 * progress bar state. Either "GET" or "POST".
 *
 * e.g. pb = new progressBar('myProgressBar');
 *      some_element.appendChild(pb.element);
 */
Drupal.progressBar = function (id, updateCallback, method, errorCallback) {
  var pb = this;
  this.id = id;
  this.method = method || 'GET';
  this.updateCallback = updateCallback;
  this.errorCallback = errorCallback;

  // The WAI-ARIA setting aria-live="polite" will announce changes after users
  // have completed their current activity and not interrupt the screen reader.
  this.element = $('<div class="progress" aria-live="polite"></div>').attr('id', id);
  this.element.html('<div class="bar"><div class="filled"></div></div>' +
                    '<div class="percentage"></div>' +
                    '<div class="message">&nbsp;</div>');
};

/**
 * Set the percentage and status message for the progressbar.
 */
Drupal.progressBar.prototype.setProgress = function (percentage, message) {
  if (percentage >= 0 && percentage <= 100) {
    $('div.filled', this.element).css('width', percentage + '%');
    $('div.percentage', this.element).html(percentage + '%');
  }
  $('div.message', this.element).html(message);
  if (this.updateCallback) {
    this.updateCallback(percentage, message, this);
  }
};

/**
 * Start monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.startMonitoring = function (uri, delay) {
  this.delay = delay;
  this.uri = uri;
  this.sendPing();
};

/**
 * Stop monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.stopMonitoring = function () {
  clearTimeout(this.timer);
  // This allows monitoring to be stopped from within the callback.
  this.uri = null;
};

/**
 * Request progress data from server.
 */
Drupal.progressBar.prototype.sendPing = function () {
  if (this.timer) {
    clearTimeout(this.timer);
  }
  if (this.uri) {
    var pb = this;
    // When doing a post request, you need non-null data. Otherwise a
    // HTTP 411 or HTTP 406 (with Apache mod_security) error may result.
    $.ajax({
      type: this.method,
      url: this.uri,
      data: '',
      dataType: 'json',
      success: function (progress) {
        // Display errors.
        if (progress.status == 0) {
          pb.displayError(progress.data);
          return;
        }
        // Update display.
        pb.setProgress(progress.percentage, progress.message);
        // Schedule next timer.
        pb.timer = setTimeout(function () { pb.sendPing(); }, pb.delay);
      },
      error: function (xmlhttp) {
        pb.displayError(Drupal.ajaxError(xmlhttp, pb.uri));
      }
    });
  }
};

/**
 * Display errors on the page.
 */
Drupal.progressBar.prototype.displayError = function (string) {
  var error = $('<div class="messages error"></div>').html(string);
  $(this.element).before(error).hide();

  if (this.errorCallback) {
    this.errorCallback(this);
  }
};

})(jQuery);
;
(function ($) {

/**
 * Toggle the visibility of a fieldset using smooth animations.
 */
Drupal.toggleFieldset = function (fieldset) {
  var $fieldset = $(fieldset);
  if ($fieldset.is('.collapsed')) {
    var $content = $('> .fieldset-wrapper', fieldset).hide();
    $fieldset
      .removeClass('collapsed')
      .trigger({ type: 'collapsed', value: false })
      .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Hide'));
    $content.slideDown({
      duration: 'fast',
      easing: 'linear',
      complete: function () {
        Drupal.collapseScrollIntoView(fieldset);
        fieldset.animating = false;
      },
      step: function () {
        // Scroll the fieldset into view.
        Drupal.collapseScrollIntoView(fieldset);
      }
    });
  }
  else {
    $fieldset.trigger({ type: 'collapsed', value: true });
    $('> .fieldset-wrapper', fieldset).slideUp('fast', function () {
      $fieldset
        .addClass('collapsed')
        .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Show'));
      fieldset.animating = false;
    });
  }
};

/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView = function (node) {
  var h = document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = document.documentElement.scrollTop || document.body.scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    }
    else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};

Drupal.behaviors.collapse = {
  attach: function (context, settings) {
    $('fieldset.collapsible', context).once('collapse', function () {
      var $fieldset = $(this);
      // Expand fieldset if there are errors inside, or if it contains an
      // element that is targeted by the URI fragment identifier.
      var anchor = location.hash && location.hash != '#' ? ', ' + location.hash : '';
      if ($fieldset.find('.error' + anchor).length) {
        $fieldset.removeClass('collapsed');
      }

      var summary = $('<span class="summary"></span>');
      $fieldset.
        bind('summaryUpdated', function () {
          var text = $.trim($fieldset.drupalGetSummary());
          summary.html(text ? ' (' + text + ')' : '');
        })
        .trigger('summaryUpdated');

      // Turn the legend into a clickable link, but retain span.fieldset-legend
      // for CSS positioning.
      var $legend = $('> legend .fieldset-legend', this);

      $('<span class="fieldset-legend-prefix element-invisible"></span>')
        .append($fieldset.hasClass('collapsed') ? Drupal.t('Show') : Drupal.t('Hide'))
        .prependTo($legend)
        .after(' ');

      // .wrapInner() does not retain bound events.
      var $link = $('<a class="fieldset-title" href="#"></a>')
        .prepend($legend.contents())
        .appendTo($legend)
        .click(function () {
          var fieldset = $fieldset.get(0);
          // Don't animate multiple times.
          if (!fieldset.animating) {
            fieldset.animating = true;
            Drupal.toggleFieldset(fieldset);
          }
          return false;
        });

      $legend.append(summary);
    });
  }
};

})(jQuery);
;

/**
 * @file: Popup dialog interfaces for the media project.
 *
 * Drupal.media.popups.mediaBrowser
 *   Launches the media browser which allows users to pick a piece of media.
 *
 * Drupal.media.popups.mediaStyleSelector
 *  Launches the style selection form where the user can choose
 *  what format / style they want their media in.
 *
 */

(function ($) {
namespace('Drupal.media.popups');

/**
 * Media browser popup. Creates a media browser dialog.
 *
 * @param {function}
 *          onSelect Callback for when dialog is closed, received (Array
 *          media, Object extra);
 * @param {Object}
 *          globalOptions Global options that will get passed upon initialization of the browser.
 *          @see Drupal.media.popups.mediaBrowser.getDefaults();
 *
 * @param {Object}
 *          pluginOptions Options for specific plugins. These are passed
 *          to the plugin upon initialization.  If a function is passed here as
 *          a callback, it is obviously not passed, but is accessible to the plugin
 *          in Drupal.settings.variables.
 *
 *          Example
 *          pluginOptions = {library: {url_include_patterns:'/foo/bar'}};
 *
 * @param {Object}
 *          widgetOptions Options controlling the appearance and behavior of the
 *          modal dialog.
 *          @see Drupal.media.popups.mediaBrowser.getDefaults();
 */
Drupal.media.popups.mediaBrowser = function (onSelect, globalOptions, pluginOptions, widgetOptions) {
  var options = Drupal.media.popups.mediaBrowser.getDefaults();
  options.global = $.extend({}, options.global, globalOptions);
  options.plugins = pluginOptions;
  options.widget = $.extend({}, options.widget, widgetOptions);

  // Create it as a modal window.
  var browserSrc = options.widget.src;
  if ($.isArray(browserSrc) && browserSrc.length) {
    browserSrc = browserSrc[browserSrc.length - 1];
  }
  // Params to send along to the iframe.  WIP.
  var params = {};
  $.extend(params, options.global);
  params.plugins = options.plugins;

  browserSrc += '&' + $.param(params);
  var mediaIframe = Drupal.media.popups.getPopupIframe(browserSrc, 'mediaBrowser');
  // Attach the onLoad event
  mediaIframe.bind('load', options, options.widget.onLoad);
  /**
   * Setting up the modal dialog
   */

  var ok = 'OK';
  var cancel = 'Cancel';
  var notSelected = 'You have not selected anything!';

  if (Drupal && Drupal.t) {
    ok = Drupal.t(ok);
    cancel = Drupal.t(cancel);
    notSelected = Drupal.t(notSelected);
  }

  // @todo: let some options come through here. Currently can't be changed.
  var dialogOptions = options.dialog;

  dialogOptions.buttons[ok] = function () {
    var selected = this.contentWindow.Drupal.media.browser.selectedMedia;
    if (selected.length < 1) {
      alert(notSelected);
      return;
    }
    onSelect(selected);
    $(this).dialog("close");
  };

  dialogOptions.buttons[cancel] = function () {
    $(this).dialog("close");
  };

  Drupal.media.popups.setDialogPadding(mediaIframe.dialog(dialogOptions));
  // Remove the title bar.
  mediaIframe.parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
  Drupal.media.popups.overlayDisplace(mediaIframe.parents(".ui-dialog"));
  return mediaIframe;
};

Drupal.media.popups.mediaBrowser.mediaBrowserOnLoad = function (e) {
  var options = e.data;
  if (this.contentWindow.Drupal.media.browser.selectedMedia.length > 0) {
    var ok = (Drupal && Drupal.t) ? Drupal.t('OK') : 'OK';
    var ok_func = $(this).dialog('option', 'buttons')[ok];
    ok_func.call(this);
    return;
  }
};

Drupal.media.popups.mediaBrowser.getDefaults = function () {
  return {
    global: {
      types: [], // Types to allow, defaults to all.
      activePlugins: [] // If provided, a list of plugins which should be enabled.
    },
    widget: { // Settings for the actual iFrame which is launched.
      src: Drupal.settings.media.browserUrl, // Src of the media browser (if you want to totally override it)
      onLoad: Drupal.media.popups.mediaBrowser.mediaBrowserOnLoad // Onload function when iFrame loads.
    },
    dialog: Drupal.media.popups.getDialogOptions()
  };
};

Drupal.media.popups.mediaBrowser.finalizeSelection = function () {
  var selected = this.contentWindow.Drupal.media.browser.selectedMedia;
  if (selected.length < 1) {
    alert(notSelected);
    return;
  }
  onSelect(selected);
  $(this).dialog("close");
}

/**
 * Style chooser Popup. Creates a dialog for a user to choose a media style.
 *
 * @param mediaFile
 *          The mediaFile you are requesting this formatting form for.
 *          @todo: should this be fid?  That's actually all we need now.
 *
 * @param Function
 *          onSubmit Function to be called when the user chooses a media
 *          style. Takes one parameter (Object formattedMedia).
 *
 * @param Object
 *          options Options for the mediaStyleChooser dialog.
 */
Drupal.media.popups.mediaStyleSelector = function (mediaFile, onSelect, options) {
  var defaults = Drupal.media.popups.mediaStyleSelector.getDefaults();
  // @todo: remove this awful hack :(
  defaults.src = defaults.src.replace('-media_id-', mediaFile.fid);
  options = $.extend({}, defaults, options);
  // Create it as a modal window.
  var mediaIframe = Drupal.media.popups.getPopupIframe(options.src, 'mediaStyleSelector');
  // Attach the onLoad event
  mediaIframe.bind('load', options, options.onLoad);

  /**
   * Set up the button text
   */
  var ok = 'OK';
  var cancel = 'Cancel';
  var notSelected = 'Very sorry, there was an unknown error embedding media.';

  if (Drupal && Drupal.t) {
    ok = Drupal.t(ok);
    cancel = Drupal.t(cancel);
    notSelected = Drupal.t(notSelected);
  }

  // @todo: let some options come through here. Currently can't be changed.
  var dialogOptions = Drupal.media.popups.getDialogOptions();

  dialogOptions.buttons[ok] = function () {

    var formattedMedia = this.contentWindow.Drupal.media.formatForm.getFormattedMedia();
    if (!formattedMedia) {
      alert(notSelected);
      return;
    }
    onSelect(formattedMedia);
    $(this).dialog("close");
  };

  dialogOptions.buttons[cancel] = function () {
    $(this).dialog("close");
  };

  Drupal.media.popups.setDialogPadding(mediaIframe.dialog(dialogOptions));
  // Remove the title bar.
  mediaIframe.parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
  Drupal.media.popups.overlayDisplace(mediaIframe.parents(".ui-dialog"));
  return mediaIframe;
};

Drupal.media.popups.mediaStyleSelector.mediaBrowserOnLoad = function (e) {
};

Drupal.media.popups.mediaStyleSelector.getDefaults = function () {
  return {
    src: Drupal.settings.media.styleSelectorUrl,
    onLoad: Drupal.media.popups.mediaStyleSelector.mediaBrowserOnLoad
  };
};


/**
 * Style chooser Popup. Creates a dialog for a user to choose a media style.
 *
 * @param mediaFile
 *          The mediaFile you are requesting this formatting form for.
 *          @todo: should this be fid?  That's actually all we need now.
 *
 * @param Function
 *          onSubmit Function to be called when the user chooses a media
 *          style. Takes one parameter (Object formattedMedia).
 *
 * @param Object
 *          options Options for the mediaStyleChooser dialog.
 */
Drupal.media.popups.mediaFieldEditor = function (fid, onSelect, options) {
  var defaults = Drupal.media.popups.mediaFieldEditor.getDefaults();
  // @todo: remove this awful hack :(
  defaults.src = defaults.src.replace('-media_id-', fid);
  options = $.extend({}, defaults, options);
  // Create it as a modal window.
  var mediaIframe = Drupal.media.popups.getPopupIframe(options.src, 'mediaFieldEditor');
  // Attach the onLoad event
  // @TODO - This event is firing too early in IE on Windows 7,
  // - so the height being calculated is too short for the content.
  mediaIframe.bind('load', options, options.onLoad);

  /**
   * Set up the button text
   */
  var ok = 'OK';
  var cancel = 'Cancel';
  var notSelected = 'Very sorry, there was an unknown error embedding media.';

  if (Drupal && Drupal.t) {
    ok = Drupal.t(ok);
    cancel = Drupal.t(cancel);
    notSelected = Drupal.t(notSelected);
  }

  // @todo: let some options come through here. Currently can't be changed.
  var dialogOptions = Drupal.media.popups.getDialogOptions();

  dialogOptions.buttons[ok] = function () {
    var formattedMedia = this.contentWindow.Drupal.media.formatForm.getFormattedMedia();
    if (!formattedMedia) {
      alert(notSelected);
      return;
    }
    onSelect(formattedMedia);
    $(this).dialog("close");
  };

  dialogOptions.buttons[cancel] = function () {
    $(this).dialog("close");
  };

  Drupal.media.popups.setDialogPadding(mediaIframe.dialog(dialogOptions));
  // Remove the title bar.
  mediaIframe.parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
  Drupal.media.popups.overlayDisplace(mediaIframe.parents(".ui-dialog"));
  return mediaIframe;
};

Drupal.media.popups.mediaFieldEditor.mediaBrowserOnLoad = function (e) {

};

Drupal.media.popups.mediaFieldEditor.getDefaults = function () {
  return {
    // @todo: do this for real
    src: '/media/-media_id-/edit?render=media-popup',
    onLoad: Drupal.media.popups.mediaFieldEditor.mediaBrowserOnLoad
  };
};


/**
 * Generic functions to both the media-browser and style selector
 */

/**
 * Returns the commonly used options for the dialog.
 */
Drupal.media.popups.getDialogOptions = function () {
  return {
    buttons: {},
    dialogClass: 'media-wrapper',
    modal: true,
    draggable: false,
    resizable: false,
    minWidth: 600,
    width: 800,
    height: 550,
    position: 'center',
    overlay: {
      backgroundColor: '#000000',
      opacity: 0.4
    },
    zIndex: 10000,
    close: function (event, ui) {
      $(event.target).remove();
    }
  };
};

/**
 * Created padding on a dialog
 *
 * @param jQuery dialogElement
 *  The element which has .dialog() attached to it.
 */
Drupal.media.popups.setDialogPadding = function (dialogElement) {
  // @TODO: Perhaps remove this hardcoded reference to height.
  // - It's included to make IE on Windows 7 display the dialog without
  //   collapsing. 550 is the height that displays all of the tab panes
  //   within the Add Media overlay. This is either a bug in the jQuery
  //   UI library, a bug in IE on Windows 7 or a bug in the way the
  //   dialog is instantiated. Or a combo of the three.
  //   All browsers except IE on Win7 ignore these defaults and adjust
  //   the height of the iframe correctly to match the content in the panes
  dialogElement.height(dialogElement.dialog('option', 'height'));
  dialogElement.width(dialogElement.dialog('option', 'width'));
};

/**
 * Get an iframe to serve as the dialog's contents. Common to both plugins.
 */
Drupal.media.popups.getPopupIframe = function (src, id, options) {
  var defaults = {width: '800px', scrolling: 'auto'};
  var options = $.extend({}, defaults, options);

  return $('<iframe class="media-modal-frame"/>')
  .attr('src', src)
  .attr('width', options.width)
  .attr('id', id)
  .attr('scrolling', options.scrolling);
};

Drupal.media.popups.overlayDisplace = function (dialog) {
  if (parent.window.Drupal.overlay) {
    var overlayDisplace = parent.window.Drupal.overlay.getDisplacement('top');
    if (dialog.offset().top < overlayDisplace) {
      dialog.css('top', overlayDisplace);
    }
  }
}

})(jQuery);
;

/**
 *  @file
 *  This file handles the JS for Media Module functions.
 */

(function ($) {

/**
 * Loads media browsers and callbacks, specifically for media as a field.
 */
Drupal.behaviors.mediaElement = {
  attach: function (context, settings) {
    // Options set from media.fields.inc for the types, etc to show in the browser.

    // For each widget (in case of multi-entry)
    $('.media-widget', context).once('mediaBrowserLaunch', function () {
      var options = settings.media.elements[this.id];
      globalOptions = {};
      if (options.global != undefined) {
        var globalOptions = options.global;
      }
      //options = Drupal.settings.media.fields[this.id];
      var fidField = $('.fid', this);
      var previewField = $('.preview', this);
      var removeButton = $('.remove', this); // Actually a link, but looks like a button.

      // Show the Remove button if there's an already selected media.
      if (fidField.val() != 0) {
        removeButton.css('display', 'inline-block');
      }

      // When someone clicks the link to pick media (or clicks on an existing thumbnail)
      $('.launcher', this).bind('click', function () {
        // Launch the browser, providing the following callback function
        // @TODO: This should not be an anomyous function.
        Drupal.media.popups.mediaBrowser(function (mediaFiles) {
          if (mediaFiles.length < 0) {
            return;
          }
          var mediaFile = mediaFiles[0];
          // Set the value of the filefield fid (hidden).
          fidField.val(mediaFile.fid);
          // Set the preview field HTML.
          previewField.html(mediaFile.preview);
          // Show the Remove button.
          removeButton.show();
        }, globalOptions);
        return false;
      });

      // When someone clicks the Remove button.
      $('.remove', this).bind('click', function () {
        // Set the value of the filefield fid (hidden).
        fidField.val(0);
        // Set the preview field HTML.
        previewField.html('');
        // Hide the Remove button.
        removeButton.hide();
        return false;
      });

      $('.media-edit-link', this).bind('click', function () {
        var fid = fidField.val();
        if (fid) {
          Drupal.media.popups.mediaFieldEditor(fid, function (r) { alert(r); });
        }
        return false;
      });

    });
  }
};

})(jQuery);
;

(function ($) {

/**
 * Auto-hide summary textarea if empty and show hide and unhide links.
 */
Drupal.behaviors.textSummary = {
  attach: function (context, settings) {
    $('.text-summary', context).once('text-summary', function () {
      var $widget = $(this).closest('div.field-type-text-with-summary');
      var $summaries = $widget.find('div.text-summary-wrapper');

      $summaries.once('text-summary-wrapper').each(function(index) {
        var $summary = $(this);
        var $summaryLabel = $summary.find('label');
        var $full = $widget.find('.text-full').eq(index).closest('.form-item');
        var $fullLabel = $full.find('label');

        // Create a placeholder label when the field cardinality is
        // unlimited or greater than 1.
        if ($fullLabel.length == 0) {
          $fullLabel = $('<label></label>').prependTo($full);
        }

        // Setup the edit/hide summary link.
        var $link = $('<span class="field-edit-link">(<a class="link-edit-summary" href="#">' + Drupal.t('Hide summary') + '</a>)</span>').toggle(
          function () {
            $summary.hide();
            $(this).find('a').html(Drupal.t('Edit summary')).end().appendTo($fullLabel);
            return false;
          },
          function () {
            $summary.show();
            $(this).find('a').html(Drupal.t('Hide summary')).end().appendTo($summaryLabel);
            return false;
          }
        ).appendTo($summaryLabel);

        // If no summary is set, hide the summary field.
        if ($(this).find('.text-summary').val() == '') {
          $link.click();
        }
        return;
      });
    });
  }
};

})(jQuery);
;
(function ($) {

/**
 * Automatically display the guidelines of the selected text format.
 */
Drupal.behaviors.filterGuidelines = {
  attach: function (context) {
    $('.filter-guidelines', context).once('filter-guidelines')
      .find(':header').hide()
      .closest('.filter-wrapper').find('select.filter-list')
      .bind('change', function () {
        $(this).closest('.filter-wrapper')
          .find('.filter-guidelines-item').hide()
          .siblings('.filter-guidelines-' + this.value).show();
      })
      .change();
  }
};

})(jQuery);
;
(function ($) {

Drupal.behaviors.menuFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.menu-link-form', context).drupalSetSummary(function (context) {
      if ($('.form-item-menu-enabled input', context).is(':checked')) {
        return Drupal.checkPlain($('.form-item-menu-link-title input', context).val());
      }
      else {
        return Drupal.t('Not in menu');
      }
    });
  }
};

/**
 * Automatically fill in a menu link title, if possible.
 */
Drupal.behaviors.menuLinkAutomaticTitle = {
  attach: function (context) {
    $('fieldset.menu-link-form', context).each(function () {
      // Try to find menu settings widget elements as well as a 'title' field in
      // the form, but play nicely with user permissions and form alterations.
      var $checkbox = $('.form-item-menu-enabled input', this);
      var $link_title = $('.form-item-menu-link-title input', context);
      var $title = $(this).closest('form').find('.form-item-title input');
      // Bail out if we do not have all required fields.
      if (!($checkbox.length && $link_title.length && $title.length)) {
        return;
      }
      // If there is a link title already, mark it as overridden. The user expects
      // that toggling the checkbox twice will take over the node's title.
      if ($checkbox.is(':checked') && $link_title.val().length) {
        $link_title.data('menuLinkAutomaticTitleOveridden', true);
      }
      // Whenever the value is changed manually, disable this behavior.
      $link_title.keyup(function () {
        $link_title.data('menuLinkAutomaticTitleOveridden', true);
      });
      // Global trigger on checkbox (do not fill-in a value when disabled).
      $checkbox.change(function () {
        if ($checkbox.is(':checked')) {
          if (!$link_title.data('menuLinkAutomaticTitleOveridden')) {
            $link_title.val($title.val());
          }
        }
        else {
          $link_title.val('');
          $link_title.removeData('menuLinkAutomaticTitleOveridden');
        }
        $checkbox.closest('fieldset.vertical-tabs-pane').trigger('summaryUpdated');
        $checkbox.trigger('formUpdated');
      });
      // Take over any title change.
      $title.keyup(function () {
        if (!$link_title.data('menuLinkAutomaticTitleOveridden') && $checkbox.is(':checked')) {
          $link_title.val($title.val());
          $link_title.val($title.val()).trigger('formUpdated');
        }
      });
    });
  }
};

})(jQuery);
;

(function ($) {

Drupal.behaviors.xmlsitemapFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-xmlsitemap', context).drupalSetSummary(function (context) {
      var vals = [];

      // Inclusion select field.
      var status = $('#edit-xmlsitemap-status option:selected').text();
      vals.push(Drupal.t('Inclusion: @value', { '@value': status }));

      // Priority select field.
      var priority = $('#edit-xmlsitemap-priority option:selected').text();
      vals.push(Drupal.t('Priority: @value', { '@value': priority }));

      return vals.join('<br />');
    });
  }
};

})(jQuery);
;
(function ($) {

Drupal.behaviors.pathFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.path-form', context).drupalSetSummary(function (context) {
      var path = $('.form-item-path-alias input').val();
      var automatic = $('.form-item-path-pathauto input').attr('checked');

      if (automatic) {
        return Drupal.t('Automatic alias');
      }
      if (path) {
        return Drupal.t('Alias: @alias', { '@alias': path });
      }
      else {
        return Drupal.t('No alias');
      }
    });
  }
};

})(jQuery);
;

(function ($) {

Drupal.behaviors.metatagFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.metatags-form', context).drupalSetSummary(function (context) {
      var vals = [];
      $("input[type='text'], select, textarea", context).each(function() {
        var input_field = $(this).attr('name');
        // Verify the field exists before proceeding.
        if (input_field === undefined) {
          return false;
        }
        var default_name = input_field.replace(/\[value\]/, '[default]');
        var default_value = $("input[type='hidden'][name='" + default_name + "']", context);
        if (default_value.length && default_value.val() == $(this).val()) {
          // Meta tag has a default value and form value matches default value.
          return true;
        }
        else if (!default_value.length && !$(this).val().length) {
          // Meta tag has no default value and form value is empty.
          return true;
        }
        var label = $("label[for='" + $(this).attr('id') + "']").text();
        vals.push(Drupal.t('@label: @value', {
          '@label': $.trim(label),
          '@value': Drupal.truncate($(this).val(), 25) || Drupal.t('None')
        }));
      });
      if (vals.length === 0) {
        return Drupal.t('Using defaults');
      }
      else {
        return vals.join('<br />');
      }
    });
  }
};

/**
 * Encode special characters in a plain-text string for display as HTML.
 */
Drupal.truncate = function (str, limit) {
  if (str.length > limit) {
    return str.substr(0, limit) + '...';
  }
  else {
    return str;
  }
};

})(jQuery);
;
(function ($) {

/**
 * Attaches the autocomplete behavior to all required fields.
 */
Drupal.behaviors.autocomplete = {
  attach: function (context, settings) {
    var acdb = [];
    $('input.autocomplete', context).once('autocomplete', function () {
      var uri = this.value;
      if (!acdb[uri]) {
        acdb[uri] = new Drupal.ACDB(uri);
      }
      var $input = $('#' + this.id.substr(0, this.id.length - 13))
        .attr('autocomplete', 'OFF')
        .attr('aria-autocomplete', 'list');
      $($input[0].form).submit(Drupal.autocompleteSubmit);
      $input.parent()
        .attr('role', 'application')
        .append($('<span class="element-invisible" aria-live="assertive"></span>')
          .attr('id', $input.attr('id') + '-autocomplete-aria-live')
        );
      new Drupal.jsAC($input, acdb[uri]);
    });
  }
};

/**
 * Prevents the form from submitting if the suggestions popup is open
 * and closes the suggestions popup when doing so.
 */
Drupal.autocompleteSubmit = function () {
  return $('#autocomplete').each(function () {
    this.owner.hidePopup();
  }).length == 0;
};

/**
 * An AutoComplete object.
 */
Drupal.jsAC = function ($input, db) {
  var ac = this;
  this.input = $input[0];
  this.ariaLive = $('#' + this.input.id + '-autocomplete-aria-live');
  this.db = db;

  $input
    .keydown(function (event) { return ac.onkeydown(this, event); })
    .keyup(function (event) { ac.onkeyup(this, event); })
    .blur(function () { ac.hidePopup(); ac.db.cancel(); });

};

/**
 * Handler for the "keydown" event.
 */
Drupal.jsAC.prototype.onkeydown = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 40: // down arrow.
      this.selectDown();
      return false;
    case 38: // up arrow.
      this.selectUp();
      return false;
    default: // All other keys.
      return true;
  }
};

/**
 * Handler for the "keyup" event.
 */
Drupal.jsAC.prototype.onkeyup = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 16: // Shift.
    case 17: // Ctrl.
    case 18: // Alt.
    case 20: // Caps lock.
    case 33: // Page up.
    case 34: // Page down.
    case 35: // End.
    case 36: // Home.
    case 37: // Left arrow.
    case 38: // Up arrow.
    case 39: // Right arrow.
    case 40: // Down arrow.
      return true;

    case 9:  // Tab.
    case 13: // Enter.
    case 27: // Esc.
      this.hidePopup(e.keyCode);
      return true;

    default: // All other keys.
      if (input.value.length > 0 && !input.readOnly) {
        this.populatePopup();
      }
      else {
        this.hidePopup(e.keyCode);
      }
      return true;
  }
};

/**
 * Puts the currently highlighted suggestion into the autocomplete field.
 */
Drupal.jsAC.prototype.select = function (node) {
  this.input.value = $(node).data('autocompleteValue');
};

/**
 * Highlights the next suggestion.
 */
Drupal.jsAC.prototype.selectDown = function () {
  if (this.selected && this.selected.nextSibling) {
    this.highlight(this.selected.nextSibling);
  }
  else if (this.popup) {
    var lis = $('li', this.popup);
    if (lis.length > 0) {
      this.highlight(lis.get(0));
    }
  }
};

/**
 * Highlights the previous suggestion.
 */
Drupal.jsAC.prototype.selectUp = function () {
  if (this.selected && this.selected.previousSibling) {
    this.highlight(this.selected.previousSibling);
  }
};

/**
 * Highlights a suggestion.
 */
Drupal.jsAC.prototype.highlight = function (node) {
  if (this.selected) {
    $(this.selected).removeClass('selected');
  }
  $(node).addClass('selected');
  this.selected = node;
  $(this.ariaLive).html($(this.selected).html());
};

/**
 * Unhighlights a suggestion.
 */
Drupal.jsAC.prototype.unhighlight = function (node) {
  $(node).removeClass('selected');
  this.selected = false;
  $(this.ariaLive).empty();
};

/**
 * Hides the autocomplete suggestions.
 */
Drupal.jsAC.prototype.hidePopup = function (keycode) {
  // Select item if the right key or mousebutton was pressed.
  if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
    this.input.value = $(this.selected).data('autocompleteValue');
  }
  // Hide popup.
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function () { $(popup).remove(); });
  }
  this.selected = false;
  $(this.ariaLive).empty();
};

/**
 * Positions the suggestions popup and starts a search.
 */
Drupal.jsAC.prototype.populatePopup = function () {
  var $input = $(this.input);
  var position = $input.position();
  // Show popup.
  if (this.popup) {
    $(this.popup).remove();
  }
  this.selected = false;
  this.popup = $('<div id="autocomplete"></div>')[0];
  this.popup.owner = this;
  $(this.popup).css({
    top: parseInt(position.top + this.input.offsetHeight, 10) + 'px',
    left: parseInt(position.left, 10) + 'px',
    width: $input.innerWidth() + 'px',
    display: 'none'
  });
  $input.before(this.popup);

  // Do search.
  this.db.owner = this;
  this.db.search(this.input.value);
};

/**
 * Fills the suggestion popup with any matches received.
 */
Drupal.jsAC.prototype.found = function (matches) {
  // If no value in the textfield, do not show the popup.
  if (!this.input.value.length) {
    return false;
  }

  // Prepare matches.
  var ul = $('<ul></ul>');
  var ac = this;
  for (key in matches) {
    $('<li></li>')
      .html($('<div></div>').html(matches[key]))
      .mousedown(function () { ac.select(this); })
      .mouseover(function () { ac.highlight(this); })
      .mouseout(function () { ac.unhighlight(this); })
      .data('autocompleteValue', key)
      .appendTo(ul);
  }

  // Show popup with matches, if any.
  if (this.popup) {
    if (ul.children().length) {
      $(this.popup).empty().append(ul).show();
      $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
    }
    else {
      $(this.popup).css({ visibility: 'hidden' });
      this.hidePopup();
    }
  }
};

Drupal.jsAC.prototype.setStatus = function (status) {
  switch (status) {
    case 'begin':
      $(this.input).addClass('throbbing');
      $(this.ariaLive).html(Drupal.t('Searching for matches...'));
      break;
    case 'cancel':
    case 'error':
    case 'found':
      $(this.input).removeClass('throbbing');
      break;
  }
};

/**
 * An AutoComplete DataBase object.
 */
Drupal.ACDB = function (uri) {
  this.uri = uri;
  this.delay = 300;
  this.cache = {};
};

/**
 * Performs a cached and delayed search.
 */
Drupal.ACDB.prototype.search = function (searchString) {
  var db = this;
  this.searchString = searchString;

  // See if this string needs to be searched for anyway.
  searchString = searchString.replace(/^\s+|\s+$/, '');
  if (searchString.length <= 0 ||
    searchString.charAt(searchString.length - 1) == ',') {
    return;
  }

  // See if this key has been searched for before.
  if (this.cache[searchString]) {
    return this.owner.found(this.cache[searchString]);
  }

  // Initiate delayed search.
  if (this.timer) {
    clearTimeout(this.timer);
  }
  this.timer = setTimeout(function () {
    db.owner.setStatus('begin');

    // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
    // encodeURIComponent to allow autocomplete search terms to contain slashes.
    $.ajax({
      type: 'GET',
      url: db.uri + '/' + Drupal.encodePath(searchString),
      dataType: 'json',
      success: function (matches) {
        if (typeof matches.status == 'undefined' || matches.status != 0) {
          db.cache[searchString] = matches;
          // Verify if these are still the matches the user wants to see.
          if (db.searchString == searchString) {
            db.owner.found(matches);
          }
          db.owner.setStatus('found');
        }
      },
      error: function (xmlhttp) {
        alert(Drupal.ajaxError(xmlhttp, db.uri));
      }
    });
  }, this.delay);
};

/**
 * Cancels the current autocomplete request.
 */
Drupal.ACDB.prototype.cancel = function () {
  if (this.owner) this.owner.setStatus('cancel');
  if (this.timer) clearTimeout(this.timer);
  this.searchString = '';
};

})(jQuery);
;

(function ($) {

Drupal.behaviors.nodeFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.node-form-revision-information', context).drupalSetSummary(function (context) {
      var revisionCheckbox = $('.form-item-revision input', context);

      // Return 'New revision' if the 'Create new revision' checkbox is checked,
      // or if the checkbox doesn't exist, but the revision log does. For users
      // without the "Administer content" permission the checkbox won't appear,
      // but the revision log will if the content type is set to auto-revision.
      if (revisionCheckbox.is(':checked') || (!revisionCheckbox.length && $('.form-item-log textarea', context).length)) {
        return Drupal.t('New revision');
      }

      return Drupal.t('No revision');
    });

    $('fieldset.node-form-author', context).drupalSetSummary(function (context) {
      var name = $('.form-item-name input', context).val() || Drupal.settings.anonymous,
        date = $('.form-item-date input', context).val();
      return date ?
        Drupal.t('By @name on @date', { '@name': name, '@date': date }) :
        Drupal.t('By @name', { '@name': name });
    });

    $('fieldset.node-form-options', context).drupalSetSummary(function (context) {
      var vals = [];

      $('input:checked', context).parent().each(function () {
        vals.push(Drupal.checkPlain($.trim($(this).text())));
      });

      if (!$('.form-item-status input', context).is(':checked')) {
        vals.unshift(Drupal.t('Not published'));
      }
      return vals.join(', ');
    });
  }
};

})(jQuery);
;
/**
 * @file
 * Drupal to Google Maps API bridge.
 */

/*global jQuery, Drupal, GLatLng, GSmallZoomControl, GLargeMapControl, GMap2 */
/*global GMapTypeControl, GSmallMapControl, G_HYBRID_MAP, G_NORMAL_MAP */
/*global G_PHYSICAL_MAP, G_SATELLITE_MAP, GHierarchicalMapTypeControl */
/*global GKeyboardHandler, GLatLngBounds, GMenuMapTypeControl, GEvent */
/*global GOverviewMapControl, GScaleControl, GUnload */

(function () { // BEGIN closure
    var handlers = {};
    var maps = {};
    var ajaxoffset = 0;

    Drupal.gmap = {

        /**
         * Retrieve a map object for use by a non-widget.
         * Use this if you need to be able to fire events against a certain map
         * which you have the mapid for.
         * Be a good GMap citizen! Remember to send change()s after modifying variables!
         */
        getMap: function (mapid) {
            if (maps[mapid]) {
                return maps[mapid];
            }
            else {
                // Perhaps the user passed a widget id instead?
                mapid = mapid.split('-').slice(1, -1).join('-');
                if (maps[mapid]) {
                    return maps[mapid];
                }
            }
            return false;
        },

        unloadMap: function (mapid) {
            delete maps[mapid];
        },

        addHandler: function (handler, callback) {
            if (!handlers[handler]) {
                handlers[handler] = [];
            }
            handlers[handler].push(callback);
        },

        globalChange: function (name, userdata) {
            for (var mapid in Drupal.settings.gmap) {
                if (Drupal.settings.gmap.hasOwnProperty(mapid)) {
                    // Skip maps that are set up but not shown, etc.
                    if (maps[mapid]) {
                        maps[mapid].change(name, -1, userdata);
                    }
                }
            }
        },

        setup: function (settings) {
            var obj = this;

            var initcallback = function (mapid) {
                return (function () {
                    maps[mapid].change("bootstrap_options", -1);
                    maps[mapid].change("boot", -1);
                    maps[mapid].change("init", -1);
                    // Send some changed events to fire up the rest of the initial settings..
                    maps[mapid].change("maptypechange", -1);
                    maps[mapid].change("controltypechange", -1);
                    maps[mapid].change("alignchange", -1);
                    // Set ready to put the event system into action.
                    maps[mapid].ready = true;
                    maps[mapid].change("ready", -1);
                });
            };

            if (settings || (Drupal.settings && Drupal.settings.gmap)) {
                var mapid = obj.id.split('-');
                if (Drupal.settings['gmap_remap_widgets']) {
                    if (Drupal.settings['gmap_remap_widgets'][obj.id]) {
                        jQuery.each(Drupal.settings['gmap_remap_widgets'][obj.id].classes, function () {
                            jQuery(obj).addClass(this);
                        });
                        mapid = Drupal.settings['gmap_remap_widgets'][obj.id].id.split('-');
                    }
                }
                var instanceid = mapid.pop();
                mapid.shift();
                mapid = mapid.join('-');
                var control = instanceid.replace(/\d+$/, '');

                // Lazy init the map object.
                if (!maps[mapid]) {
                    if (settings) {
                        maps[mapid] = new Drupal.gmap.map(settings);
                    }
                    else {
                        maps[mapid] = new Drupal.gmap.map(Drupal.settings.gmap[mapid]);
                    }
                    // Prepare the initialization callback.
                    var callback = initcallback(mapid);
                    setTimeout(callback, 0);
                }

                if (handlers[control]) {
                    for (var i = 0; i < handlers[control].length; i++) {
                        handlers[control][i].call(maps[mapid], obj);
                    }
                }
                else {
                    // Element with wrong class?
                }
            }
        }
    };

    jQuery.fn.createGMap = function (settings, mapid) {
        return this.each(function () {
            if (!mapid) {
                mapid = 'auto' + ajaxoffset + 'ajax';
                ajaxoffset++;
            }
            settings.id = mapid;
            jQuery(this)
                .attr('id', 'gmap-' + mapid + '-gmap0')
                .css('width', settings.width)
                .css('height', settings.height)
                .addClass('gmap-control')
                .addClass('gmap-gmap')
                .addClass('gmap')
                .addClass('gmap-map')
                .addClass('gmap-' + mapid + '-gmap')
                .addClass('gmap-processed')
                .each(function () {
                    Drupal.gmap.setup.call(this, settings)
                });
        });
    };

})(); // END closure

Drupal.gmap.factory = {};

Drupal.gmap.map = function (v) {
    this.vars = v;
    this.map = undefined;
    this.ready = false;
    var _bindings = {};

    /**
     * Register interest in a change.
     */
    this.bind = function (name, callback) {
        if (!_bindings[name]) {
            _bindings[name] = [];
        }
        return _bindings[name].push(callback) - 1;
    };

    /**
     * Change notification.
     * Interested parties can act on changes.
     */
    this.change = function (name, id, userdata) {
        var c;
        if (_bindings[name]) {
            for (c = 0; c < _bindings[name].length; c++) {
                if (c !== id) {
                    _bindings[name][c](userdata);
                }
            }
        }
        if (name !== 'all') {
            this.change('all', -1, name, userdata);
        }
    };

    /**
     * Deferred change notification.
     * This will cause a change notification to be tacked on to the *end* of the event queue.
     */
    this.deferChange = function (name, id, userdata) {
        var obj = this;
        // This will move the function call to the end of the event loop.
        setTimeout(function () {
            obj.change(name, id, userdata);
        }, 0);
    };

    this.getMapTypeName = function (type) {
        if (type == 'map' || type == 'roadmap') return 'Map';
        if (type == 'hybrid') return 'Hybrid';
        if (type == 'physical' || type == 'terrain') return 'Physical';
        if (type == 'satellite') return 'Satellite';
    };

    this.getMapTypeId = function (type) {
        if (type == 'Map' || type == 'Roadmap') return google.maps.MapTypeId.ROADMAP;
        if (type == 'Hybrid') return google.maps.MapTypeId.HYBRID;
        if (type == 'Physical' || type == 'Terrain') return google.maps.MapTypeId.TERRAIN;
        if (type == 'Satellite') return google.maps.MapTypeId.SATELLITE;
    };
};

////////////////////////////////////////
//             Map widget             //
////////////////////////////////////////
Drupal.gmap.addHandler('gmap', function (elem) {
    var obj = this;
    var _ib = {};

    // Respond to incoming zooms
    _ib.zoom = obj.bind("zoom", function (zoom) {
        obj.map.setZoom(obj.vars.zoom);
    });

    // Respond to incoming moves
    _ib.move = obj.bind("move", function () {
        obj.map.panTo(new google.maps.LatLng(obj.vars.latitude, obj.vars.longitude));
    });

    // Respond to incoming width changes.
    _ib.width = obj.bind("widthchange", function (w) {
        obj.map.getDiv().style.width = w;
        google.maps.event.trigger(obj.map);
    });
    // Send out outgoing width changes.
    // N/A
    // Respond to incoming height changes.
    _ib.height = obj.bind("heightchange", function (h) {
        obj.map.getDiv().style.height = h;
        google.maps.event.trigger(obj.map);
    });
    // Send out outgoing height changes.
    // N/A

    // Respond to incoming control type changes.
    _ib.ctc = obj.bind("controltypechange", function () {
        if (obj.vars.controltype === 'Small') {
            obj.map.setOptions({zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL}});
        }
        else if (obj.vars.controltype === 'Large') {
            obj.map.setOptions({zoomControlOptions: {style: google.maps.ZoomControlStyle.LARGE}});
        }
        // obsolete
        else if (obj.vars.controltype === 'Android') {
            obj.map.setOptions({zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL}});
        }
    });
    // Send out outgoing control type changes.
    // N/A

    // Respond to incoming map type changes.
    _ib.mtc = obj.bind("maptypechange", function () {
        obj.map.setMapTypeId(obj.getMapTypeId(obj.vars.maptype));
    });
    // Send out outgoing map type changes.
    // N/A

    obj.bind("bootstrap_options", function () {
        // Bootup options.
        var opts = {}; // Object literal google.maps.MapOptions
        obj.opts = opts;

        // Disable default UI for custom options
        opts.disableDefaultUI = true;

        // Set draggable property
        if (obj.vars.behavior.nodrag) {
            opts.draggable = false;
        }
        else if (obj.vars.behavior.nokeyboard) {
            opts.keyboardShortcuts = false;
        }

        // Set default map type (set to road map if nothing selected)
        switch (obj.vars.maptype) {
            case 'Hybrid':
                opts.mapTypeId = google.maps.MapTypeId.HYBRID;
                break;
            case 'Physical':
                opts.mapTypeId = google.maps.MapTypeId.TERRAIN;
                break;
            case 'Satellite':
                opts.mapTypeId = google.maps.MapTypeId.SATELLITE;
                break;
            case 'Map':
            default:
                opts.mapTypeId = google.maps.MapTypeId.ROADMAP;
                break;
        }

        // Null out the enabled types.
        opts.mapTypeIds = [];

        if (obj.vars.baselayers.Map) {
            opts.mapTypeIds.push(google.maps.MapTypeId.ROADMAP);
        }
        if (obj.vars.baselayers.Hybrid) {
            opts.mapTypeIds.push(google.maps.MapTypeId.HYBRID);
        }
        if (obj.vars.baselayers.Physical) {
            opts.mapTypeIds.push(google.maps.MapTypeId.TERRAIN);
        }
        if (obj.vars.baselayers.Satellite) {
            opts.mapTypeIds.push(google.maps.MapTypeId.SATELLITE);
        }

        if (obj.vars.draggableCursor) {
            opts.draggableCursor = obj.vars.draggableCursor;
        }
        if (obj.vars.draggingCursor) {
            opts.draggingCursor = obj.vars.draggingCursor;
        }
        if (obj.vars.backgroundColor) {
            opts.backgroundColor = obj.vars.backgroundColor;
        }

        // Map type control
        opts.mapTypeControl = true;
        opts.mapTypeControlOptions = {};
        if (obj.vars.mtc === 'standard') {
            opts.mapTypeControlOptions.style = google.maps.MapTypeControlStyle.DEFAULT;
        }
        else if (obj.vars.mtc === 'horiz') {
            opts.mapTypeControlOptions.style = google.maps.MapTypeControlStyle.HORIZONTAL_BAR;
        }
        else if (obj.vars.mtc === 'menu') {
            opts.mapTypeControlOptions.style = google.maps.MapTypeControlStyle.DROPDOWN_MENU;
        }
        else if (obj.vars.mtc === 'none') {
            opts.mapTypeControl = false;
        }

        // Navigation control type
        if (obj.vars.controltype !== 'None') {
            opts.zoomControl = true;
        }
        if (obj.vars.pancontrol) {
          opts.panControl = true;
        }
        if (obj.vars.streetviewcontrol) {
          opts.streetViewControl = true;
        }
        if (obj.vars.controltype === 'Small') {
            obj.zoomControlOptions = {style: google.maps.ZoomControlStyle.SMALL};
        }
        else if (obj.vars.controltype === 'Large') {
            obj.zoomControlOptions = {style: google.maps.ZoomControlStyle.LARGE};
        }


        // Set scale control visibility
        opts.scaleControl = obj.vars.behavior.scale;

        // Scroll wheel control
        if (obj.vars.behavior.nomousezoom) {
            opts.scrollwheel = false;
        }
        // Disable double-click zoom
        if (obj.vars.behavior.nocontzoom) {
            opts.disableDoubleClickZoom = true;
        }
        // Overview Map
        if (obj.vars.behavior.overview) {
            opts.overviewMapControl = true;
            opts.overviewMapControlOptions = {opened: true};
        }

    });

    obj.bind("boot", function () {
        obj.map = new google.maps.Map(elem, obj.opts);
        //console.log(obj.map);
    });

    obj.bind("init", function () {
        var map = obj.map;

        // Not implemented in API v3
        // if (obj.vars.behavior.overview) {
        //   map.addControl(new GOverviewMapControl());
        // }
        // if (obj.vars.behavior.googlebar) {
        //   map.enableGoogleBar();
        // }

        if (obj.vars.extent) {
            var c = obj.vars.extent;
            var extent = new google.maps.LatLngBounds(new google.maps.LatLng(c[0][0], c[0][1]), new google.maps.LatLng(c[1][0], c[1][1]));
            obj.vars.latitude = extent.getCenter().lat();
            obj.vars.longitude = extent.getCenter().lng();
            obj.vars.zoom = map.getBoundsZoomLevel(extent);
        }
        if (obj.vars.behavior.collapsehack) {
            // Modify collapsable fieldsets to make maps check dom state when the resize handle
            // is clicked. This may not necessarily be the correct thing to do in all themes,
            // hence it being a behavior.
            setTimeout(function () {
                var r = function () {
                  var coord = map.getCenter();
                  google.maps.event.trigger(map, "resize");
                  map.setCenter(new google.maps.LatLng(coord.lat(), coord.lng()), obj.vars.zoom);
                };
                jQuery(elem).parents('fieldset.collapsible').children('legend').children('a').click(r);
                jQuery('.vertical-tab-button', jQuery(elem).parents('.vertical-tabs')).children('a').click(r);
                jQuery(window).bind('hashchange', r);
                // Would be nice, but doesn't work.
                //$(elem).parents('fieldset.collapsible').children('.fieldset-wrapper').scroll(r);
            }, 0);
        }
        map.setCenter(new google.maps.LatLng(obj.vars.latitude, obj.vars.longitude));
        map.setZoom(obj.vars.zoom);

        // Send out outgoing zooms
        google.maps.event.addListener(map, "zoom_changed", function () {
            obj.vars.zoom = map.getZoom();
            obj.change("zoom", _ib.zoom);
        });

        // Send out outgoing moves
        google.maps.event.addListener(map, "center_changed", function () {
            var coord = map.getCenter();
            obj.vars.latitude = coord.lat();
            obj.vars.longitude = coord.lng();
            obj.change("move", _ib.move);
        });

        // Send out outgoing map type changes.
        google.maps.event.addListener(map, "maptypeid_changed", function () {
            // If the map isn't ready yet, ignore it.
            if (obj.ready) {
                obj.vars.maptype = obj.getMapTypeName(map.getMapTypeId());
                obj.change("maptypechange", _ib.mtc);
            }
        });

        /*
         google.maps.event.addListener(map, 'click', function(event) {
         alert(Drupal.gmap.getIcon("big blue", 0));
         var marker = new google.maps.Marker({
         position: event.latLng,
         map: map
         });
         google.maps.event.addListener(marker, 'click', function() {
         marker.setMap(null);
         });
         });
         */
    });
});

////////////////////////////////////////
//            Zoom widget             //
////////////////////////////////////////
Drupal.gmap.addHandler('zoom', function (elem) {
    var obj = this;
    // Respond to incoming zooms
    var binding = obj.bind("zoom", function () {
        elem.value = obj.vars.zoom;
    });
    // Send out outgoing zooms
    jQuery(elem).change(function () {
        obj.vars.zoom = parseInt(elem.value, 10);
        obj.change("zoom", binding);
    });
});

////////////////////////////////////////
//          Latitude widget           //
////////////////////////////////////////
Drupal.gmap.addHandler('latitude', function (elem) {
//  var obj = this;
//  // Respond to incoming movements.
//  var binding = obj.bind("move", function () {
//    elem.value = '' + obj.vars.latitude;
//  });
//  // Send out outgoing movements.
//  $(elem).change(function () {
//    obj.vars.latitude = Number(this.value);
//    obj.change("move", binding);
//  });
});

////////////////////////////////////////
//         Longitude widget           //
////////////////////////////////////////
Drupal.gmap.addHandler('longitude', function (elem) {
//  var obj = this;
//  // Respond to incoming movements.
//  var binding = obj.bind("move", function () {
//    elem.value = '' + obj.vars.longitude;
//  });
//  // Send out outgoing movements.
//  $(elem).change(function () {
//    obj.vars.longitude = Number(this.value);
//    obj.change("move", binding);
//  });
});

////////////////////////////////////////
//          Latlon widget             //
////////////////////////////////////////
Drupal.gmap.addHandler('latlon', function (elem) {
    var obj = this;
    // Respond to incoming movements.
    var binding = obj.bind("move", function () {
        elem.value = '' + obj.vars.latitude + ',' + obj.vars.longitude;
    });
    // Send out outgoing movements.
    jQuery(elem).change(function () {
        var t = this.value.split(',');
        obj.vars.latitude = Number(t[0]);
        obj.vars.longitude = Number(t[1]);
        obj.change("move", binding);
    });
});

////////////////////////////////////////
//          Maptype widget            //
////////////////////////////////////////
Drupal.gmap.addHandler('maptype', function (elem) {
    var obj = this;
    // Respond to incoming movements.
    var binding = obj.bind("maptypechange", function () {
        elem.value = obj.vars.maptype;
    });
    // Send out outgoing movements.
    jQuery(elem).change(function () {
        obj.vars.maptype = elem.value;
        obj.change("maptypechange", binding);
    });
});

(function () { // BEGIN CLOSURE
    var re = /([0-9.]+)\s*(em|ex|px|in|cm|mm|pt|pc|%)/;
    var normalize = function (str) {
        var ar;
        if ((ar = re.exec(str.toLowerCase()))) {
            return ar[1] + ar[2];
        }
        return null;
    };

    ////////////////////////////////////////
    //           Width widget             //
    ////////////////////////////////////////
    Drupal.gmap.addHandler('width', function (elem) {
        var obj = this;
        // Respond to incoming width changes.
        var binding = obj.bind("widthchange", function (w) {
            elem.value = normalize(w);
        });
        // Send out outgoing width changes.
        jQuery(elem).change(function () {
            var n;
            if ((n = normalize(elem.value))) {
                elem.value = n;
                obj.change('widthchange', binding, n);
            }
        });
        obj.bind('init', function () {
            jQuery(elem).change();
        });
    });

    ////////////////////////////////////////
    //           Height widget            //
    ////////////////////////////////////////
    Drupal.gmap.addHandler('height', function (elem) {
        var obj = this;
        // Respond to incoming height changes.
        var binding = obj.bind("heightchange", function (h) {
            elem.value = normalize(h);
        });
        // Send out outgoing height changes.
        jQuery(elem).change(function () {
            var n;
            if ((n = normalize(elem.value))) {
                elem.value = n;
                obj.change('heightchange', binding, n);
            }
        });
        obj.bind('init', function () {
            jQuery(elem).change();
        });
    });
})(); // END CLOSURE

////////////////////////////////////////
//        Control type widget         //
////////////////////////////////////////
Drupal.gmap.addHandler('controltype', function (elem) {
    var obj = this;
    // Respond to incoming height changes.
    var binding = obj.bind("controltypechange", function () {
        elem.value = obj.vars.controltype;
    });
    // Send out outgoing height changes.
    jQuery(elem).change(function () {
        obj.vars.controltype = elem.value
        obj.change("controltypechange", binding);
    });
});

// // Map cleanup.
// if (Drupal.jsEnabled) {
//   $(document).unload(GUnload);
// }

Drupal.behaviors.GMap = {
    attach: function (context, settings) {
        if (Drupal.settings && Drupal.settings['gmap_remap_widgets']) {
            jQuery.each(Drupal.settings['gmap_remap_widgets'], function (key, val) {
                jQuery('#' + key).addClass('gmap-control');
            });
        }
        jQuery('.gmap-gmap:not(.gmap-processed)', context).addClass('gmap-processed').each(function () {
            Drupal.gmap.setup.call(this)
        });
        jQuery('.gmap-control:not(.gmap-processed)', context).addClass('gmap-processed').each(function () {
            Drupal.gmap.setup.call(this)
        });
    },
    detach: function (context, settings) {
        jQuery('.gmap-processed', context).each(function (element) {
            //find mapid
            var id = jQuery(this).attr('id');
            var mapid = id.split('-', 2);

            //unload map
            Drupal.gmap.unloadMap(mapid[1]);
        });
    }
};
;
