<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	<?php if ($teaser): ?>
	<!-- teaser template HTML -->
	
		<div class="listing-teaser row">
	
			<div class="listing-teaser-image col-sm-3">
				<a href="<?php print url($node_url, array('absolute' => TRUE)); ?>"><img src="/sites/all/themes/rapidcity/images/pic_thumb.jpg" class="img-responsive" /></a>
			</div>
	
			<div class="listing-teaser-content col-sm-5">

				<?php print render($title_prefix); ?>
			    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
				<?php print render($title_suffix); ?>
		
				<?php if($node->body): ?>
					<?php if($node->body['und'][0]['value']): ?>
					  <?php //print $node->body['und'][0]['value']; ?>
					  <?php print render(field_view_value('node', $node, 'body', $body[0],'teaser'))?> <a class="readmore" href="<?php print url($node_url, array('absolute' => TRUE)); ?>">Read More</a>
					<?php endif; ?>
				<?php endif; ?>
			
		
				<div class="listing-teaser-extras">
		
					<?php if($node->idss_venue_website_url): ?>
						<?php
							$urlStr = $node->idss_venue_website_url['und'][0]['value'];
							$urlparsed = parse_url($urlStr);
							if (empty($urlparsed['scheme'])) {
							    $urlStr = 'http://' . ltrim($urlStr, '/');
							}
						?>
						<div class="listing-teaser-links">
							<a href="<?php print($urlStr); ?>" target="_blank">Visit Website</a>
						</div>
					<?php endif; ?>
				
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php print url($node_url, array('absolute' => TRUE)); ?>">
					<a class="addthis_button_facebook"></a>
					<a class="addthis_button_twitter"></a>
					<a class="addthis_button_google_plusone_share"></a>
					<a class="addthis_button_compact"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
					<!-- AddThis Button END -->
			
				</div>
		
			</div>
	
			<div class="listing-teaser-map col-sm-4">
				<?php //print views_embed_view('map','gmap_block'); ?>
		
				<?php //if($node->idss_vatt_latitude['und'][0]['value']): ?>
					<?php //print views_embed_view('map','gmap_block'); ?>
				<?php //endif; ?>
			</div>
	
		</div>
	
	<?php else: ?>
	<!-- regular node view template HTML -->

		<div class="listing-page">
		
			<?php print render($title_prefix); ?>
		    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
			<?php print render($title_suffix); ?>

	<div class="listing_image">
	IMAGE
	</div>
	
	
	<div class="listing_fields_section">
		
		<?php if($node->idss_venue_address && !empty($node->idss_venue_address['und'][0]['value'])): ?>
			<div class="field_section">
			  <strong>Address:</strong><br /> <?php print($node->idss_venue_address['und'][0]['value']); ?>
			  
				<?php if($node->idss_venue_address_suite && !empty($node->idss_venue_address_suite['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_suite['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_address_3  && !empty($node->idss_venue_address_3['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_3['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_address_4  && !empty($node->idss_venue_address_4['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_4['und'][0]['value']); ?>
				<?php endif; ?>
			  
				<?php if($node->idss_venue_city): ?>
				  <br /><?php print($node->idss_venue_city['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_state): ?>
				  <br /><?php print($node->idss_venue_state['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_postal_code): ?>
				  <br /><?php print($node->idss_venue_postal_code['und'][0]['value']); ?>
				<?php endif; ?>
			</div>
		  
		<?php endif; ?>
		
		
		<?php if($node->idss_venue_phone1): ?>
			<div class="field_section">
			  <strong>Phone:</strong> <?php print($node->idss_venue_phone1['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		<?php if($node->idss_venue_phone2): ?>
			<div class="field_section">
			  <strong>Secondary Phone:</strong> <?php print($node->idss_venue_phone2['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		
		
		<?php if($node->idss_venue_email): ?>
			<div class="field_section">
			  <strong>Email:</strong> <?php print($node->idss_venue_email['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_website_url): ?>
			<div class="field_section">
				<?php
					$urlStr = $node->idss_venue_website_url['und'][0]['value'];
					$urlparsed = parse_url($urlStr);
					if (empty($urlparsed['scheme'])) {
					    $urlStr = 'http://' . ltrim($urlStr, '/');
					}
				?>
			  <strong>Website:</strong> <?php print($urlStr); ?>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_facebook_url || $node->idss_venue_twitter_url || $node->idss_venue_youtube_url || $node->idss_venue_linkedin_url): ?>
			<div class="field_section social_section">
			  <strong>Social:</strong> 
			  
			  <?php if($node->idss_venue_facebook_url): ?>
				  <br /><?php print($node->idss_venue_facebook_url['und'][0]['value']); ?>
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_twitter_url): ?>
				  <br /><?php print($node->idss_venue_twitter_url['und'][0]['value']); ?>
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_youtube_url): ?>
				  <br /><?php print($node->idss_venue_youtube_url['und'][0]['value']); ?>
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_linkedin_url): ?>
				  <br /><?php print($node->idss_venue_linkedin_url['und'][0]['value']); ?>
			  <?php endif; ?>
			  
			</div>
		<?php endif; ?>
		
		<div class="dynamic_fields">
			<ul>
		<?php if($node->idss_vatt_suites_available['und'][0]['value']): ?>
			<li>Suites Available</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_number_of_suites['und'][0]['value']): ?>
			<li>Number of Suites: <?php print($node->idss_vatt_number_of_suites['und'][0]['value']); ?></li>
		<?php endif; ?>
		<?php if($node->idss_vatt_units['und'][0]['value']): ?>
			<li>Units: <?php print($node->idss_vatt_units['und'][0]['value']); ?></li>
		<?php endif; ?>
			
		<?php if($node->idss_vatt_airport_shuttle['und'][0]['value']): ?>
			<li>Airport Shuttle</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_business_center['und'][0]['value']): ?>
			<li>Business Center</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_cabins['und'][0]['value']): ?>
			<li>Cabins</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_comp_breakfast['und'][0]['value']): ?>
			<li>Breakfast</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_fitness_center['und'][0]['value']): ?>
			<li>Fitness Center</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_handicapped_access['und'][0]['value']): ?>
			<li>Handicapped Access</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_hot_tub_jacuzzi['und'][0]['value']): ?>
			<li>Hot Tub / Jacuzzi</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_interior_hallways['und'][0]['value']): ?>
			<li>Interior Hallways</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_internet_in_room['und'][0]['value']): ?>
			<li>Internet in Room</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_kitchenettes_or_kitch): ?>
			<li>Kitchenettes / Kitchen</li>
		<?php endif; ?>
		
		<?php if($node->idss_vatt_laundry_facilities['und'][0]['value']): ?>
			<li>Laundry Facilities</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_lounge_on_site): ?>
			<li>Lounge on Site</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_open_seasonally['und'][0]['value']): ?>
			<li>Open Seasonally</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_peak_season_rates['und'][0]['value']): ?>
			<li>Peak Season Rates: <?php print($node->idss_vatt_peak_season_rates['und'][0]['value']); ?></li>
		<?php endif; ?>
		<?php if($node->idss_vatt_pet_friendly['und'][0]['value']): ?>
			<li>Pet Friendly</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_pool_indoor['und'][0]['value']): ?>
			<li>Indoor Pool</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_pool_outdoor['und'][0]['value']): ?>
			<li>Outdoor Pool</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_refrigerator['und'][0]['value']): ?>
			<li>Refrigerator</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_restaurant_on_site['und'][0]['value']): ?>
			<li>Restaurant on Site</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_rv_dump_station['und'][0]['value']): ?>
			<li>RV Dump Station</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_rv_full_hookups['und'][0]['value']): ?>
			<li>RV Full Hookups</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_rv_grocery['und'][0]['value']): ?>
			<li>RV Grocery</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_rv_pull_through_sites['und'][0]['value']): ?>
			<li>RV Pull Through Sites</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_showers['und'][0]['value']): ?>
			<li>Showers</li>
		<?php endif; ?>
		
		
			</ul>
		</div>
		
		
		<?php if($node->body): ?>
			<?php if($node->body['und'][0]['value']): ?>
			  <?php print($node->body['und'][0]['value']); ?>
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if($node->idss_vatt_latitude['und'][0]['value']): ?>
			<div class="listing-map">
				<?php print views_embed_view('map','gmap_block'); ?>
			</div>
		<?php endif; ?>
		
	</div>
	
  
<?php endif; ?>

</div>