<?php

//load data from xml file

//region.xml
//id
//short_title
//title
//image
//description
//long
//lat



//listings.xml
//<home>
//</home>

//home >
//id
//short_title
//title
//image
//description
//long
//lat


//<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
//<script src="/sites/all/themes/rapidcity/templates/rapid-city-map/js/jquery.rwdImageMaps.min.js"></script>
//<script src="/sites/all/themes/rapidcity/templates/rapid-city-map/js/explore_map-homepage.js"></script>
//<script src="/sites/all/themes/rapidcity/templates/rapid-city-map/js/dynomap-master/js/dynomap.js"></script>
//<script src="/sites/all/themes/rapidcity/templates/rapid-city-map/js/bigtext.js"></script>


//<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
//<script src="/sites/all/themes/rapidcity/templates/rapid-city-map/js/explore_map_homepage.js"></script>

?>
<link type="text/css" rel="stylesheet" href="/sites/all/themes/rapidcity/templates/rapid-city-map/css/style.css" media="all" />




<div class="container">
	<div class="row">
		
		<!-- <div id="explore_map_mobile" class="col-sm-12 mobile760 explore-page-mobile">
			<div class="header"><img src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/explore_map_mobile.jpg" alt="Explore Rapid City" /></div>
			<div class="em-list"></div>
		</div> -->
		
		<div id="explore_map_desktop" class="col-sm-12 em_map_box explore-page nomobile760">
			
			
			<!-- <div class="em-region-area">
				<div class="regionText">
					<h3 class="blue_region">
						Mt. Rushmore / Black Hills
					</h3>
					<h3 class="brown_region">
						Northern Rapid City
					</h3>
					<h3 class="green_region">
						Badlands Gateway
					</h3>
					<h3 class="downtown_region">
						Downtown
					</h3>
				</div>
			</div> -->
			
			<div class="regions">
				<div class="blue_region">
					<img src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/blue_region.png" alt="Mt.Rushmore / Blackhills" />
				</div>
				<div class="green_region">
					<img src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/green_region.png" alt="Badlands Gateway" />
				</div>
				<div class="downtown_region">
					<img src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/downtown_region.png" alt="Rapid City Downtown" />
				</div>
			</div>
			
			
			<img class="img-responsive em-relative" src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/explore_map.jpg" alt="Explore Rapid City" />
			
			
			
			<div class="window_area">
			
			</div>
			
			<img class="img-responsive em-absolute" src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/trans.gif" alt="Rapid City" usemap="#explore_map" />
			<map name="explore_map" id="explore_map">
			  	<area shape="poly" coords="195,183,147,324,119,473,111,559,129,604,168,638,211,654,255,652,305,628,501,490,577,415,631,335,650,278,646,231,621,199,574,177,458,157,353,135,288,135,233,149" href="#" alt="blue region" />
				  <area shape="poly" coords="607,308,571,385,555,431,553,465,567,484,588,487,620,474,683,411,731,343,740,303,732,285,656,280,632,286" href="#" alt="downtown" />
				  <area shape="poly" coords="643,130,617,170,600,218,607,248,637,273,687,293,769,309,836,313,954,300,1034,279,1044,266,1028,252,984,240,938,231,859,216,800,176,730,130,683,117" href="#" alt="brown region" />
				  <area shape="poly" coords="597,481,595,559,606,609,639,632,699,641,793,634,910,609,989,572,1015,536,1006,493,931,410,837,300,817,297,739,339,690,373,634,414,613,443" href="#" alt="green region" />
			</map>
			
			
			
			
			<div class="em-sidebar explore-page">
				<div class="em-sidebar-background explore-page"></div>
				<div class="sidebar-text">
					<div class="top-region">
						<h2>See Big Things</h2>
						<p>Tour the Rapid City map by selecting a category below and see just how close you are to an amazing trip.</p>
						<div class="em-sidebar-dropdown-rep">
							<img src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/map_arrow2.gif" />
							<a class="dropdown" href="#"><span class="choice">Category</span></a>
						</div>
						<div class="em-sidebar-dropdown"></div>
					</div>
					
					<div class="em-sidebar-dynamic-detail">
						
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
</div>

