jQuery(function($) {

var hr_blue_region = "Mt. Rushmore / Black Hills";
var hr_green_region = "Badlands Gateway";
var hr_brown_region = "Northern Rapid City";
var hr_downtown_region = "Downtown";

	var em_home_map_box_width = 1280;
	var em_home_map_box_height = 680;
	var em_explore_map_box_width = 1140;
	var em_explore_map_box_height = 517;
	
var explore_dropdown_firstrun = true;

function convertFromLLHome(lat, lon){
	
	var width = em_home_map_box_width;
	var height = em_home_map_box_height;
	
	//top left 
	//44° 7.849', -103° 20.791'
	//44.1307203,-103.3469129
	//44.13126,-103.3474256
	
	//bottom right
	//44° 2.300', -103° 6.290'
	//44.0363632,-103.1007767
	//44.0351561,-103.0996663
	//44.0433238,-103.1185894
	
	//console.log("w: " + width);
	//console.log(" h: " + height);
	
	// Size of the map
	//int width = 538;
	//int height = 811;
	// X and Y boundaries
	//var westLong = -103.3469129;
	//var eastLong = -103.1052857;
	//var northLat = 44.1306903;
	//var southLat = 44.0375704; //44.2300;
	
	var westLong = -104.054560;
	var eastLong = -101.957358;
	var northLat = 44.367787;
	var southLat = 43.572394; //44.2300;

	//void drawPoint(float latitude, float longitude){

	//fill(#000000);

	var x = width * ((westLong-lon)/(westLong-eastLong));
	var y = (height * ((northLat-lat)/(northLat-southLat)));

	//console.log(x + ", " + y);
	//ellipseMode(RADIUS);
	//ellipse(x, y, 2, 2);    
	return [x, y];

}

function convertFromLLExplore(lat, lon){
	
	var width = em_explore_map_box_width;
	var height =  em_explore_map_box_height;
	
	//top left 
	//44° 7.849', -103° 20.791'
	//44.1307203,-103.3469129
	  //44.13126,-103.3474256
	
//bottom right
//44° 2.300', -103° 6.290'
//44.0363632,-103.1007767
//44.0351561,-103.0996663
	//44.0433238,-103.1185894
	
	//console.log("w: " + width);
	//console.log(" h: " + height);
	
	
	// Size of the map
	//int width = 538;
	//int height = 811;
	//44.1200597,-103.3304314,19z
	//44.0339575,-103.0835978
	// X and Y boundaries
	//var westLong = -103.3314314;
	//var eastLong = -103.0825978;
	//var northLat = 44.1210597;
	//var southLat = 44.0368575; //44.2300;

	var westLong = -104.054560;
	var eastLong = -101.597407;
	var northLat = 44.367787;
	var southLat = 43.572394; //44.2300;

	//void drawPoint(float latitude, float longitude){

	 //fill(#000000);

	 var x = width * ((westLong-lon)/(westLong-eastLong));
	 var y = (height * ((northLat-lat)/(northLat-southLat)));

	 //console.log(x + ", " + y);
	 //ellipseMode(RADIUS);
	 //ellipse(x, y, 2, 2);    
		return [x, y];
	
	
}






/*<img class="img-responsive" src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/homepage_map.jpg" alt="Rapid City" usemap="#explore_map" />
<map name="explore_map" id="explore_map">
  <area id="blue_region" shape="poly" coords="195,186,147,327,119,476,111,562,129,607,168,641,211,657,255,655,305,631,501,493,577,418,631,338,650,281,646,234,621,202,574,180,458,160,353,138,288,138,233,152" href="#" alt="blue region" />
  <area id="downtown_region" shape="poly" coords="607,314,571,391,555,437,553,471,567,490,588,493,620,480,683,417,731,349,740,309,732,291,656,286,632,292" href="#" alt="downtown" />
  <area id="brown_region" shape="poly" coords="643,134,617,174,600,222,607,252,637,277,687,297,769,313,836,317,954,304,1034,283,1044,270,1028,256,984,244,938,235,859,220,800,180,730,134,683,121" href="#" alt="brown region" />
  <area id="green_region" shape="poly" coords="597,486,595,564,606,614,639,637,699,646,793,639,910,614,989,577,1015,541,1006,498,931,415,837,305,817,302,739,344,690,378,634,419,613,448" href="#" alt="green region" />
</map>*/







$(document).ready(function($) {
	
	
	//track if in view
	var was_seen_tracked = false;
	var was_seen_tracked_mobile = false;
	if ($("#homepage_map_desktop").length > 0)
	{
		$(window).scroll(function(){
			
			//track
			if (!was_seen_tracked && $("#homepage_map_desktop").isOnScreen(0.01,0.01))
			{
				//console.log("tracking dl home view");
				//dataLayer.push({ 'eventCategory': 'Homepage Map', 'eventAction': 'Viewable', 'eventLabel': '/', 'eventValue': 'Map Visible', 'event': 'Viewed' });
				
				trackMapGTM("Interactive Map", "impression", "viewable", "", "viewed");
				was_seen_tracked = true;
			}
		});
		$("#homepage_map_desktop .em_homepage_action a.action_button").click(function(e){
			//trackMapGTMClickHack(e, this, "Interactive Map", "/", "explore-click", $(this).html(), "action");
			trackMapGTM("Interactive Map", "explore-click", $(this).html(), "", "action");
		});
	}
	if ($("#explore_map_desktop").length > 0)
	{
		$(window).scroll(function(){
			if (!was_seen_tracked && $("#explore_map_desktop").isOnScreen(0.01,0.01))
			{
				//console.log("tracking dl explore view desk");
				//dataLayer.push({ 'eventCategory': 'Explore Map', 'eventAction': 'Viewable', 'eventLabel': '/explore-rapid-city', 'eventValue': 'Map Visible', 'event': 'Viewed' });
				
				trackMapGTM("Interactive Map", "impression", "viewable", "", "viewed");
				was_seen_tracked = true;
			}
		});
	}
	if ($("#explore_map_mobile").length > 0)
	{
		$(window).scroll(function(){
			if (!was_seen_tracked_mobile && $("#explore_map_mobile div").isOnScreen(0.01,0.01))
			{
				//console.log("tracking dl explore view mobile");
				//dataLayer.push({ 'eventCategory': 'Mobile Explore Map', 'eventAction': 'Viewable', 'eventLabel': '/explore-rapid-city', 'eventValue': 'Mobile Map Visible', 'event': 'Viewed' });
				trackMapGTM("Interactive Map Mobile", "impression", "viewable", "", "viewed");
				was_seen_tracked_mobile = true;
			}
		});
		
	}
	$("#explore_map_mobile .listing a").click(function(){
	
		trackMapGTM("Interactive Map Mobile", "listing-click", $(this).html(), "", "listing-clicked");
	});
	
	
	$("a#refresh").click(function(e) {
		e.preventDefault();
		$(".em_map_box .em_dart").remove();
		populateEMHomepage();
	});
	
	
	//$(window).load(function() {
		//set dimensions of maparea
		//$(".em-absolute").css("height", $(".em-relative").height() + "px");
		//$(".em-absolute").css("width", $(".em-relative").width() + "px");
		//$('img[usemap]').rwdImageMaps();
		
		if ($(".em_map_box.home").length > 0)
		{
			populateEMHomepage();
			//scaleFont(".em_homepage_action");
		}
		if ($(".em_map_box.explore-page").length > 0)
			{
				
				populateEMExplore();
				//scaleAbsoluteCenterOffset($(".explore-page .regionText"), $(".em-sidebar-background.explore-page"));
				//scaleFont(".em_homepage_action");
		}

		//scale absolute centers
		scaleAbsoluteCenter();
		
		
	//});
	
	
	$(window).resize(function() {
		//set dimensions of maparea
		//$(".em-absolute").css("height", $(".em-relative").height() + "px");
		//$(".em-absolute").css("width", $(".em-relative").width() + "px");
		
		scaleAbsoluteCenter();
		//scaleAbsoluteCenterOffset($(".explore-page .regionText"), $(".em-sidebar-background.explore-page"));
	});
	
		$("#explore_map area").click(function(e) {
			e.preventDefault();
			
		});
	
	
	
	//ok, xml feed for homepage
	//$('img[usemap]').rwdImageMaps();
	
	$("#explore_map #blue_region").hover(function() {
	
	  //show
	  console.log("hov");
		//alert("blue");
		//$(".blue_region").show();
	}, function() {
		
		//hide
		console.log("hovend");
		//$(".blue_region").hide();
	});
	
	
	
});
	
	
	function populateEMHomepage()
	{
		$.ajax({
		        type: "GET",
			url: "/sites/all/themes/rapidcity/templates/rapid-city-map/listings.xml",
			dataType: "xml",
			cache: false,
			success: function(xml) {
				
				
			
				//onchange, rebuild darts from cached data (array)

				$(xml).find('listing').each(function(){
					
					var id = $(this).attr('id');
					var short_title = $(this).find('short_title').text();
					var title = $(this).find('title').text();
					var image = $(this).find('image').text();
					var description = $(this).find('description').text();
					var url = $(this).find('url').text();
					var longitude = $(this).find('long').text();
					var latitude = $(this).find('lat').text();
					var region = $(this).find('region').text();
					var position = $(this).find('position').text();
					
					//plot
					var coords = convertFromLLHome(latitude, longitude);
					//console.log("coords[0]:" + coords[0] + " boxw " + $(".em_map_box").width());
					var percentx = coords[0] / em_home_map_box_width * 100;
					var percenty = coords[1] / em_home_map_box_height * 100;
					
					if(latitude <= 44.07) { //if point is on southside of map ∫mh∫
						var win_pos = "win_pos2";
					} else {
						var win_pos = "win_pos1";
					}
					
					var content = "<div class=\"em_dart shortMode " + win_pos + "\" style=\"left: " + percentx + "%;" + " top: " + percenty + "%;\"><div class=\"short\">";
					
					/*if (position == "right-middle")
					{
					content += "<a class=\"straight\" href=\"#\"><img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\">"+ short_title + "</a>";
					//content += "<a href=\"#\"><img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\">"+ short_title + "</a>";
					}
					else
					if (position == "right-top")
					{
					content += "<a class=\"top\" href=\"#\"><img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\"></a><a class=\"top text\" href=\"#\">"+ short_title + "</a>";
					}
					else
					if (position == "right-bottom")
					{
						content += "<a class=\"bottom\" href=\"#\"><img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\"></a><a class=\"text bottom\" href=\"#\">"+ short_title + "</a>";
					}
					else
					{*/
						content += "<a class=\"x-mark\" href=\"#\"><img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\"></a>";
					//}
					
					content += "</div>";
					
					content += "<div class=\"long\"><a class=\"exit\" href=\"#\">X</a>"
					content += "<div class=\"title\">" + "<h3>" + title + "</h3>" + "</div>";
					content +="<div class=\"image\">" + "<img src=\"" + image + "\" alt=\"" + title +  "\">" + "</div><div class=\"text\">" + "<p>" + description + "</p>" + "<a class=\"learn_more\" href=\"" + url + "\"  onclick=\"" + "trackMapGTM('Interactive Map', 'listing-click', '" + title + "', '', 'action')" + "\">" + "Learn More" + "<img src=\"/sites/all/themes/rapidcity/templates/rapid-city-map/images/learn_more_arrow.gif\" />" +  "</a></div><div class=\"breaker\"></div></div>";
					content += "<div class=\"data\">" + "<span class=\"region\">" + region + "</span></div>";
					content +=  "</div>";
					$(".em_map_box.home").append(content);
					
					//console.log(xml);
					
					//track learnmore
					/*$(".em_map_box.home a.learn_more").click(function(){
							trackMapGTM("Interactive Map", "/", "listing-click", $(this).parent().parent().find(".title h3").html(), "action");
					});*/
					
				});
				
					//set hover events for darts, region
					$(".em_dart .short").hover(function(){
						$(".em_dart").css("z-index", "10");
						$(this).parent().css("z-index", "11");
						
						
						var region = $(this).parent().find(".data .region").text();
						//reveal region background
						
						//region
						if (!$(".regions ." + region).is(':visible'))
						{
							$(".regions div").attr("style", "");
							$(".regions ." + region).attr("style", "").fadeIn();
						}
						
						//show applicabe region text
						$(".regionText").css("background-color","lightgray"); //changes from default 'camoflauge' color ∫mh∫
						$(".regionText h3").hide(); 
						$(".regionText h3." + region).show(); 
						
						
						//console.log(region);
					},
					function(){
						//$(this).fadeIn();
						
						/*if(!$(this).next().is(':hover')) {
							var region = $(this).parent().find(".data .region").text();
							$(".regions ." + region).attr("style", "").fadeOut();
						}
						*/
						
						//show complete
						
					});
					
					
					//box show
					$(".em_dart .short").hoverIntent(function(){
						//trackMapGTM("Interactive Map", "/explore-rapid-city", "Sidebar Listing Click"
						trackMapGTM("Interactive Map", "dart-hover", $(this).parent().find(".long .title h3").html(), "", "dart-opened");
						showBox($(this));
							
							

						}, function(){
							
							//closeBox($(this));
							
							
						});
					
					
					//leave large window
					$(".em_map_box .window_area").hover(function(){},function(){
						
						closeFadeBox();
						
					})
			
			
			
			
					//short clicks to expand
					$(".em_dart .short a").click(function(e){
						e.preventDefault();
					});
					
					scaleFont(".em_map_box");
			}
		});
	}
	
	function showBox(thisElement)
	{
		var long_content = $(thisElement).parent().find(".long").html();
		var dart = $(thisElement).parent();
		
		//alert(long_content);
		
		//clear window
		$(".em_map_box .window_area").html(long_content);
		$(".em_map_box .window_area").attr("style", $(dart).attr("style"));
		if($(dart).hasClass("win_pos2")){
			$(".em_map_box .window_area").css("margin-top", ($(".window_area").height() + 40) * -1); //displaces the window area ∫mh∫
		} else {
			$(".em_map_box .window_area").css("margin-top", 0);
		}
		$(".em_map_box .window_area").css("z-index", "12");
		$(".em_map_box .window_area").stop().show();
		
		//change position if no room
		
		//exit clicks
		$(".em_map_box .window_area a.exit").click(function(e){
			e.preventDefault();
			closeBox();
		});
	}
	function closeBox()
	{
		//$(thisElement).parent().hide();
		//$(thisElement).parent().parent().find(".short").show();
		//$(this).parent().parent().removeClass("fullMode");
		//$(this).parent().parent().addClass("shortMode");
		$(".em_map_box .window_area").html("");
		$(".em_map_box .window_area").hide();
		$(".em-sidebar-dynamic-detail ol li a").removeClass("selected");
	}
	function closeFadeBox()
	{
		$(".em_map_box .window_area").stop().fadeOut(800, function(){ $(".em_map_box .window_area").html(""); });
		$(".em-sidebar-dynamic-detail ol li a").removeClass("selected");
		
	}
	
	function populateEMExplore()
	{
		$.ajax({
		        type: "GET",
			url: "/sites/all/themes/rapidcity/templates/rapid-city-map/listings_full.xml",
			dataType: "xml",
			success: function(xml) {

				//1. Build Menu
				//2. Add listings under this
				var menu_dropdown = "";
				var mobile_list = "";
				var countCategory = 0;
				
				//create array of arrays
				var myEMCategories = new Array();
				
				//alert($(xml).find('section'));
				$(xml).find('firstSection, section').each(function(){
					
					var section_title = $(this).children('title').text();
					var section_id = $(this).children('id').text();
					var section_list = "";
					
					$(this).find('category').each(function(){
						var category_title = $(this).children('title').text();
						var category_id_xml = $(this).children('id').text();
						var category_id = "em-cat-array-" + countCategory;
						var category_description = $(this).children('description').text();
						section_list += "<li><a href=\"#\" class=\"" + category_id_xml + "\" id=\"" + category_id + "\">" + category_title + "</a></li>";
						//mobile_list += "<li>" + category_title;
						//create new cat array, add to master
						var myEMCatListings = new Array();
						
						//fetch all listings, add to cat array
						//
						var countListings = 0;
						$(this).find('listing').each(function(){
							
							var short_title = $(this).find('short_title').text();
							var title = $(this).find('title').text();
							var image = $(this).find('image').text();
							var description = $(this).find('description').text();
							var url = $(this).find('url').text();
							var longitude = $(this).find('long').text();
							var latitude = $(this).find('lat').text();
							var region = $(this).find('region').text();
							//var position = $(this).find('position').text();
							
							//listing value array
							var listingArray = new Array();
							listingArray["short_title"] = short_title;
							listingArray["title"] = title;
							listingArray["image"] = image;
							listingArray["description"] = description;
							listingArray["url"] = url;
							listingArray["longitude"] = longitude;
							listingArray["latitude"] = latitude;
							listingArray["region"] = region;
							
							myEMCatListings[countListings] = listingArray;
							
							countListings++;
						});
						
						var myEMCatFields = new Array();
						myEMCatFields["title"] = category_title;
						myEMCatFields["description"] = category_description;
						myEMCatFields["listings"] = myEMCatListings;
						
						myEMCategories[countCategory] = myEMCatFields;
						
						countCategory++;
					});
					
			
					if($(this)[0].nodeName == "firstSection")
					{
						menu_dropdown += "<ul class=\"firstSection category_title\">" + section_list + "</ul>";
						$(".em-sidebar-dropdown-rep .dropdown .choice").html(section_title);
					}
					else
					{
						//build menu structure
						menu_dropdown += "<h3>" + section_title + "</h3>" + "<ul class=\"section\">" + section_list + "</ul>";
					}
				
						
						
					
					/*
					
					
					//console.log(xml);*/
					
				});
				$(".em-sidebar-dropdown").html(menu_dropdown);
				
				
				//mobile
				for(var i=0; i<myEMCategories.length; i++)
				{
					mobile_list += "<ul class=\"category\"><li>" + myEMCategories[i]["title"] + "</li><li><ul>";
					
					for(var j=0; j<myEMCategories[i]["listings"].length; j++)
					{
						  mobile_list += "<li class=\"listing\"><a href=\"" + myEMCategories[i]["listings"][j]["url"] + "\">" + myEMCategories[i]["listings"][j]["title"] + "</a><br />";
							var hr_region = "";
						
							if (myEMCategories[i]["listings"][j]["region"] == "blue_region") 
							{ hr_region = hr_blue_region; }
							if (myEMCategories[i]["listings"][j]["region"] == "green_region") 
							{ hr_region = hr_green_region; }
							if (myEMCategories[i]["listings"][j]["region"] == "brown_region") 
							{ hr_region = hr_brown_region; }
							if (myEMCategories[i]["listings"][j]["region"] == "downtown_region") 
							{ hr_region = hr_downtown_region; }
							mobile_list += "Region: " + hr_region + "</li>";
								
				  }
					mobile_list += "</ul></li></ul>";
					
				}
				
				$(".em-list").html(mobile_list);
				
				
				//plot attractions by default
				plotEMExplore(myEMCategories[1], 1);
				
				//click events for categories
				$(".em-sidebar-dropdown .firstSection a, .em-sidebar-dropdown .section a").click(function(e){
					e.preventDefault();
					//fetch id, fetch index, plot
					var id = $(this).attr("id");
					plotEMExplore(myEMCategories[id.substring(13)], id.substring(13));
					var cat_title = myEMCategories[id.substring(13)]["title"];
					var cat_description = myEMCategories[id.substring(13)]["description"];
					
					$(".em-sidebar-dynamic-detail").html("<h3>" + cat_title +  "</h3>" + "<p>" + cat_description + "</p>");
					$(".em-sidebar-dropdown").hide();
					$(".em-sidebar-dropdown-rep").show();
					$(".em-sidebar-dropdown-rep .dropdown .choice").html(cat_title);
					$(".em-sidebar-dynamic-detail").show();
					
					var listingsOutgo = "<ol>";
					//print list of listing points in dynamic section
					for (var i=0; i<myEMCategories[id.substring(13)]["listings"].length; i++)
					{
						listingsOutgo +=  "<li><a id=\"detail_cat_" + id.substring(13) + "_listing_" + i + "\" href=\"#\">" + myEMCategories[id.substring(13)]["listings"][i]["title"] + "</a></li>";
					}
					listingsOutgo += "</ol>";
					$(".em-sidebar-dynamic-detail").append(listingsOutgo);
					
					if (explore_dropdown_firstrun)
					{
						explore_dropdown_firstrun = false;
					}
					else
					{
						trackMapGTM("Interactive Map", "dropdown-interaction", cat_title, "", "dart-opened");
					}
					
					
					$(".em-sidebar-dynamic-detail ol li a").click(function(e){
						e.preventDefault();
						//locate listing and init click
						
						var id_name = $(this).attr("id");
						var cat = id_name.substring(11);
						cat = cat.substring(0, cat.indexOf("_"));
						//alert("cat: " + cat);
						var listing = id_name.substring(12);
						listing = listing.substring(listing.indexOf("_listing_") + 9);
						//$("#dart_cat_" + cat + "_listing_" + listing).trigger("mouseover");
						showBox($("#dart_cat_" + cat + "_listing_" + listing));
						setRegion($("#dart_cat_" + cat + "_listing_" + listing));
						
						$(".em-sidebar-dynamic-detail ol li a").removeClass("selected");
						$(this).addClass("selected");
						
						trackMapGTM("Interactive Map", "sidebar-listing-click", $("#dart_cat_" + cat + "_listing_" + listing).parent().find(".long .title h3").html(), "", "sidebar-listing-click");
						
						//alert("listing: " + listing);
					});
					
					
				});
				
				//click events for categories
				$(".em-sidebar-dropdown-rep .dropdown").click(function(e){
					e.preventDefault();
					closeBox();
					$(".em-sidebar-dropdown-rep").hide();
					$(".em-sidebar-dropdown").show();
					$(".em-sidebar-dynamic-detail").hide();
					
					
				});
				
				//init
				$(".em-sidebar-dropdown .firstSection a").trigger("click");
				
			}
		});
	}
	
	
	//run on load
	function scaleFont(element)
	{
		var parent_element = ".em_map_box";
		//var ratio = $(element).css("font-size").replace("px", "") / $(parent_element).width();
		ratio = 0.012280701754385965;
		
		var calcFont = ratio * $(parent_element).width();
		$(element).css("font-size", calcFont + "px");
		
		$(window).resize(function(){
			var calcFont = ratio * $(parent_element).width();
			$(element).css("font-size", calcFont + "px");
			
		});
		
	}
	
	
	function plotEMExplore(myEMListingsArray, cat_id)
	{
		$(".em_map_box.explore-page .em_dart").remove();
		
		
		
		var myEMListingsArrayList = myEMListingsArray["listings"];
		
		//console.log(myEMListingsArray);
		for(var i=0; i<myEMListingsArrayList.length; i++ )
		{
			//plot
			var coords = convertFromLLExplore(myEMListingsArrayList[i]["latitude"], myEMListingsArrayList[i]["longitude"]);
			//console.log("coords[0]:" + coords[0] + " boxw " + $(".em_map_box").width());
			var percentx = coords[0] / em_explore_map_box_width * 100;
			var percenty = coords[1] / em_explore_map_box_height * 100;

			var content = "<div class=\"em_dart shortMode nomobile\"" + " style=\"left: " + percentx + "%;" + " top: " + percenty + "%;\"><div id=\"dart_cat_" + cat_id + "_listing_" + i + "\" class=\"short\">";


				content += "<a class=\"x-mark\" href=\"#\"><img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\"></a>";
			

			content += "</div>";
			content += "<div class=\"long\"><a class=\"exit\" href=\"#\">X</a>"
			content += "<div class=\"title\">" + "<h3>" + myEMListingsArrayList[i]["title"] + "</h3>" + "</div>";
			content +="<div class=\"image\">" + "<img src=\"" + myEMListingsArrayList[i]["image"] + "\" alt=\"" + myEMListingsArrayList[i]["title"] +  "\">" + "</div><div class=\"text\">" + "<p>" + myEMListingsArrayList[i]["description"] + "</p>" + "<a class=\"learn_more\" href=\"" + myEMListingsArrayList[i]["url"] + "\" onclick=\"" + "trackMapGTM('Interactive Map', 'listing-click', '" + myEMListingsArrayList[i]["title"] + "', '', 'action')" + "\">" + "Learn More" + "<img src=\"/sites/all/themes/rapidcity/templates/rapid-city-map/images/learn_more_arrow.gif\" />" +  "</a></div><div class=\"breaker\"></div></div>";
			content += "<div class=\"data\">" + "<span class=\"region\">" + myEMListingsArrayList[i]["region"] + "</span></div>";
			content +=  "</div>";
			$(".em_map_box.explore-page").append(content);
			//track learnmore
			$(".em_map_box.explore-page.nomobile760 a.learn_more").click(function(){
					//trackMapGTM("Interactive Map", "/explore-rapid-city", "listing-click", $(this).parent().parent().find(".title h3").html(), "action");
			});
			
		}
		
		//events
		
		//set hover events for darts, region
		$(".em_dart .short").hover(function(){
			setRegion($(this));
			
			
			//console.log(region);
		},
		function(){
			//$(this).fadeIn();
			
			/*if(!$(this).next().is(':hover')) {
				var region = $(this).parent().find(".data .region").text();
				$(".regions ." + region).attr("style", "").fadeOut();
			}
			*/
			
			//show complete
			
		});
		
		
		
		
			//box show
			$(".em_dart .short").hoverIntent(function(){
				trackMapGTM("Interactive Map", "dart-hover", $(this).parent().find(".long .title h3").html(), "", "dart-opened");
				showBox($(this));
					
				}, function(){
					
					//closeBox($(this));
					
				});
			
			
			//leave large window
			$(".em_map_box .window_area").hover(function(){},function(){
				
				closeFadeBox();
				
			})
	
			//short clicks to expand
			$(".em_dart .short a").click(function(e){
				e.preventDefault();
			});
			
			scaleFont(".em_map_box");
		
		
	}
	
	function setRegion(this_element)
	{
		$(".em_dart").css("z-index", "10");
		$(this_element).parent().css("z-index", "11");
		
		
		var region = $(this_element).parent().find(".data .region").text();
		//reveal region background
		
		//region
		if (!$(".regions ." + region).is(':visible'))
		{
			$(".regions div").attr("style", "");
			$(".regions ." + region).attr("style", "").fadeIn();
		}
		
		//show applicabe region text
		$(".regionText h3").hide(); 
		$(".regionText h3." + region).show();
	}
	
	function scaleAbsoluteCenter()
	{
		
		$(".absolute_center").each(function()
		{
			//console.log("parent w: " + $(this).parent().width());
			//console.log("this w: " + $(this).width());
			$(this).parent().css("position", "relative");
			$(this).css("left", ($(this).parent().width() / 2 ) - ($(this).outerWidth() / 2)  + "px");
		});
		
		
	}
	function scaleAbsoluteCenterOffset(target, target_offset)
	{
		
		//$(target).each(function()
		//{
			//console.log("parent w: " + $(this).parent().width());
			//console.log("this w: " + $(this).width());
			$(target).parent().css("position", "relative");
			//console.log("parentw: " + $(target).parent().outerWidth());
			//console.log("targetw: " + $(target).outerWidth());
			
			var target_calc = ($(target).parent().outerWidth() / 2 ) - ($(target).outerWidth() / 2) - $(target_offset).outerWidth();
			
			$(target).css("left", target_calc  + "px");
		//});
		
		
	}
});

function trackMapGTMClickHack(e, thisone, eventcategory, eventaction, eventlabel, eventvalue, givenevent)
{
	e.preventDefault();
	trackMapGTM(eventcategory, eventaction, eventlabel, eventvalue, givenevent);
	window.location = $(thisone).attr("href");
}

function trackMapGTM(eventcategory, eventaction, eventlabel, eventvalue, givenevent)
{

	eventcategory = "\"" + eventcategory + "\"";
	eventaction = "\"" + eventaction + "\"";
	eventlabel = "\"" + eventlabel + "\"";
	eventvalue = "\"" + eventvalue + "\"";
	givenevent = "\"" + givenevent + "\"";

	//dataLayer.push({ 'eventCategory': 'Mobile Explore Map', 'eventAction': 'Viewable', 'eventLabel': '/explore-rapid-city', 'eventValue': 'Mobile Map Visible', 'event': 'Viewed' });
	//console.log("dataLayer.push({" + "eventCategory: " + eventcategory + ", eventAction: " + eventaction + ", eventLabel: " + eventlabel + ", eventValue: " + eventvalue + ", event: " + givenevent + " });");
	// event val takes the slack
	dataLayer.push({ 'eventCategory': eventcategory, 'eventAction': eventaction, 'eventLabel': eventlabel, 'eventValue': eventvalue, 'event': givenevent });
}