
<!--MetaData Inclusion --><?php render($page['content']['metatags']);  ?>


<div class="dropdown-bg"></div>

<!-- Start Booking Widget -->
<div class="dropdown-book">
	<div class="container">
		<div class="row">
			<script src="http://widgets.bookdirect.net/widgets/search_widget.js.php?id=758" type="text/javascript"></script>
		</div>
	</div>
</div>
<!-- End Booking Widget -->

<header id="navbar" role="banner" class="<?php //print $navbar_classes; ?>">
  <div class="navbar-container container">
	  
	  <div class="navbar-left">
	    <div class="navbar-header">
	      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>

		<div class="navbar-collapse collapse">
			<nav role="navigation">
  			  <?php //if (!empty($secondary_nav)): ?>
  			    <?php //print render($secondary_nav); ?>
  			  <?php //endif; ?>
			  <ul class="menu nav navbar-nav secondary">
				  <li class="first collapsed"><a href="/meeting-planners" title="">Meeting Planners</a></li>
				  <li class="collapsed"><a href="/press-media-inquiries">Press &amp; Media Inquiries</a></li>
				  <li class="collapsed"><a href="/sporting-events-planners" title="">Sporting Events Planners</a></li>
				  <li class="last collapsed"><a href="/travel-professionals" title="">Travel Professionals</a></li>
			  </ul>
			  <div class="head-social-icons">
  				<a class="head-social-icon head-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_head_facebook.png" alt="Facebook" /></a>
  				<a class="head-social-icon head-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_head_twitter.png" alt="Twitter" /></a>
  				<a class="head-social-icon head-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_head_pinterest.png" alt="Pinterest" /></a>
  				<a class="head-social-icon head-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_head_youtube.png" alt="YouTube"/></a>
  				<a class="head-social-icon head-instagram" href="http://www.instagram.com/VisitRapidCity" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_head_instagram.png" alt="Instagram" /></a>
			  </div>
			  <?php if (!empty($primary_nav)): ?>
			    <?php print render($primary_nav); ?>
			  <?php endif; ?>
			</nav>
		</div>
	</div>
	
	<div class="navbar-right">
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print t('Home'); ?></a>
		<div class="quicklinks">
			<div class="signin">
				<!-- <a href="/signin">Sign In/Register</a> -->
			</div>
			<div class="booknow">
				<a href="#">Book Now</a>
			</div>
		</div>
		<div class="nearme mobile-only">
			<a href="/near-me">What's Nearby?</a>
		</div>
		<?php if (!empty($page['navigation'])): ?>
			<?php print render($page['navigation']); ?>
		<?php endif; ?>
		<?php if (!empty($page['search'])): ?>
			<?php print render($page['search']); ?>
		<?php endif; ?>
	</div>
	  
  </div>
</header>

<div class="masthead">
	<div class="container jumbotron-container">
		<div class="row">
			<div class="col-sm-12">
				<?php if (!empty($page['header'])): ?>
					<?php print render($page['header']); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<!-- <div class="container" style="margin:20px auto;">
	<div class="row">
		<div class="col-sm-6">
			<p>Whether you share humble beginnings or feel destined to lead, the spirit of American determination rings out in Rapid City. You sense it in the South Dakota skies. It’s right there in the faces of all the presidents lining our streets. You witness it carved into stone gazing out over the Black Hills as you visit Mount Rushmore. It’s honored in the massive monument taking shape at the Crazy Horse Memorial. It lives on in the herds of buffalo roaming Custer State Park. The hopeful determination you feel when you’re in Rapid City urges you on, opening you up to new experiences and adventures, reminding you that each of us is made for more. Like those who reached this frontier before us, we’re all here to Do Big Things.</p>
		</div>
		<div class="col-sm-6">
			<p>Rapid City is one of the most popular travel destinations in all of North America, offering a vibrant contemporary city vibe to complement the deeply meaningful cultural heritage of the area. Along with monuments to great men, local traditions and folklore make for a rich experience when you visit. The <a href="/things-to-do/arts-culture">arts</a>, <a href="/things-to-do/parks-monuments">parks and monuments</a>, <a href="/things-to-do/outdoor-recreation">outdoor activities</a>, world-class <a href="/dining">restaurants</a> and quaint <a href="/things-to-do/shopping">shopping</a> set Rapid City apart as a hub for taking it all in. As your base camp while in the region, you’ll discover <a href="/things-to-do/downtown">Downtown Rapid City</a>, <a href="/things-to-do/mount-rushmore">Mount Rushmore</a>, <a href="/things-to-do/parks-monuments/custer-state-park">Custer State Park</a>, <a href="/things-to-do/parks-monuments/crazy-horse-memorial">Crazy Horse Memorial</a>, <a href="/things-to-do/parks-monuments/devils-tower">Devils Tower</a>, <a href="/things-to-do/parks-monuments/badlands-park">Badlands National Park</a> and many other attractions in the Black Hills.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<p align="center">You were born to be here in this moment. Head out and <strong>Do Big Things</strong>.</p>
		</div>
	</div>
</div> -->

<div class="callout">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="lato-bold">Monumental Trip Ideas</h1>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-xs-6 col-sm-3 bigthings">
				<a href="/dining"><img class="img-responsive" src="/sites/all/themes/rapidcity/images/eat_big.jpg" alt="Do Big Things - Eat" /></a>
			</div>
			<div class="col-xs-6 col-sm-3 bigthings">
				<a href="/things-to-do/shopping"><img class="img-responsive" src="/sites/all/themes/rapidcity/images/shop_big.jpg" alt="Do Big Things - Shop" /></a>
			</div>
			<div class="col-xs-6 col-sm-3 bigthings">
				<a href="/where-to-stay"><img class="img-responsive" src="/sites/all/themes/rapidcity/images/stay_big.jpg" alt="Do Big Things - Stay" /></a>
			</div>
			<div class="col-xs-6 col-sm-3 bigthings">
				<a href="/things-to-do"><img class="img-responsive" src="/sites/all/themes/rapidcity/images/do_big.jpg" alt="Do Big Things - Do" /></a>
			</div>
		</div>
	</div>
</div>

<div class="home-explore-map">
	<?php include 'rapid-city-map/explore_map.php'; ?>
</div> 

<!-- <div class="home-explore-map">
	<?php //include 'rapid-city-map/homepage_map.php'; ?>
</div>
 -->
<div class="like-a-local">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<img class="img-responsive ranger-title" src="/sites/all/themes/rapidcity/images/do_big_things.jpg" alt="Do Big Things" />
				<h2 class="lato-bold" style="margin:0;">Explore Big Possibilities</h2>
			</div>
		</div>

		<?php
  		print render(module_invoke('views', 'block_view', 'featured_content-block'));
		?>
	</div>
</div>

<div class="download-guide container">
	<div class="row">
		<div class="col-sm-12">
			<a class="lato-bold" href="/request-visitors-guide">Download a Visitors Guide</a>
		</div>
	</div>
</div>

	<div class="row">
		<div class="col-sm-12">
			<?php //print render($page['content']); ?>
		</div>
	</div>

<div class="callout">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="lato-bold">Where To Stay</h1>
				<p>Make your Black Hills vacation an experience in total comfort. Rapid City hotels offer a range of welcoming environments, from  contemporary design and amenities to historic charm. From downtown convenience to scenic views, with both familiar brands and unique options to choose from, you’ll find just the right Rapid City accommodations for your stay.</p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=21" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-bed-breakfast.jpg" alt="Bed and Breakfast" />
						<figcaption>
							Bed &amp;<br /> Breakfast
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=198" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-cabins.jpg" alt="Cabins" />
						<figcaption>
							Cabins
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=225" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-camp.jpg" alt="Campgrounds" />
						<figcaption>
							Campgrounds
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=337" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-hotel.jpg" alt="Hotels, Motels, Resorts" />
						<figcaption>
							Hotels,<br /> Motels, Resorts
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=311" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-homes.jpg" alt="Vacation Homes" />
						<figcaption>
							Vacation Homes
						</figcaption>
					</figure>
				</a>
			</div>
		</div>
	</div>
</div>


<div class="slab-red">
	<div class="">
		<?php if (!empty($page['bottom_first'])): ?>
			<?php print render($page['bottom_first']); ?>
		<?php endif; ?>
	</div>
</div>

<?php if (!empty($page['footer'])): ?>
	<?php print render($page['footer']); ?>
<?php endif; ?>

<div class="footer-fixed">
	<div class="foothill-left"></div>
	<div class="foothill-right"></div>
	<div class="footer-fixed-content">
		<?php print render($page['footer_fixed']); ?>
	</div>
</div>
