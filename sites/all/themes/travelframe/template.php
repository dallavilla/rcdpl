<?php


/**
 * Preprocess variables for page.tpl.php
 *
 * @see page.tpl.php
 */
function travelframe_preprocess_page(&$variables) {
  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['columns'] = 3;
  }
  elseif (!empty($variables['page']['sidebar_first'])) {
    $variables['columns'] = 2;
  }
  elseif (!empty($variables['page']['sidebar_second'])) {
    $variables['columns'] = 2;
  }
  else {
    $variables['columns'] = 1;
  }

}

/**
 * Bootstrap theme wrapper function for the primary menu links
 */
function travelframe_menu_tree__primary(&$variables) {
}

/**
 * Bootstrap theme wrapper function for the secondary menu links
 */
function travelframe_menu_tree__secondary(&$variables) {
}

/**
 * Override Bootstrap base theme's implementation.
 */
function travelframe_menu_local_action($variables) {
  return theme_menu_local_action($variables);
}
function travelframe_menu_link(array $variables) {
  return theme_menu_link($variables);
}

/**
	* Prints breadcrumbs based on url separators and includes current page
	*/
function get_breadcrumb(){
  global $base_url;
  $breadcrumb = array();
  $path = $_SERVER['REQUEST_URI'];
  $firstpath = explode('?',$path);
  $paths = explode('/',$firstpath[0]);
  $actualpath = '';
  $last = end($paths);
  foreach($paths as $test){
    $actualpath .= $test. '/';
    if($test == $last) {
      $breadcrumb[] = t(ucwords(str_replace("-"," ",$test)));
    }
    else {
      $breadcrumb[] = l(ucwords(str_replace("-"," ",$test)), rtrim($base_url.$actualpath, "/"));
    }
  }
  $breadcrumb[0] = l('Home','<front>');
  return $breadcrumb;
}

