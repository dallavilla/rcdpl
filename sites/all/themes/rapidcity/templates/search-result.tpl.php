
<div class="search-results">

	    <h2><a href="<?php print $url; ?>"><?php print $title; ?></a></h2>
	
	    <?php if ($snippet) : ?>
	      <p class="search-snippet"><?php print $snippet; ?><br />
		  <a href="<?php print $url; ?>"><?php print $url; ?></a></p>
	    <?php endif; ?>
		
</div>