
jQuery(function($) {
	
	// When the 'from date' is changed on the events search fields, set
	// the 'start date' equal to the 'end date'.
	$('#edit-idss-vatt-event-start-date-value-min input[id*="datepicker"]').change(function() {
		$('#edit-idss-vatt-event-start-date-value-max input[id*="datepicker"]').val($(this).val());
		$('#edit-idss-vatt-event-end-date-value-min input[id*="datepicker"]').val($(this).val());
		$('#edit-idss-vatt-event-end-date-value-max input[id*="datepicker"]').val($(this).val());
		$('#edit-between-date-filter-start-value input[id*="datepicker"]').val($(this).val());
		$('#edit-between-date-filter-end-value input[id*="datepicker"]').val($(this).val());
	});
	$('#edit-idss-vatt-event-start-date-value-max input[id*="datepicker"]').change(function() {
		$('#edit-idss-vatt-event-end-date-value-max input[id*="datepicker"]').val($(this).val());
		$('#edit-between-date-filter-end-value input[id*="datepicker"]').val($(this).val());
	});
	
	$('#idssform').submit(function() {
		$('#idssform #submit').hide();
		$('#idssform .submitting').show();
	});
	  
	
	//$('.dropdown-bg').stop(true, true).slideUp();
	//setTimeout( function(){ $('.dropdown-bg').css( "opacity", "1" ); },500);
	
	$('#navbar .dropdown').hover(function() {
		$('.dropdown-bg').css('height', $(this).find('.dropdown-menu').height() + 10);
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown(100);
	}, function() {
		$('.dropdown-bg').css('height', 0);
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(100);
	});
	

	$('#navbar .dropdown > a').click(function(){
		location.href = this.href;
	});
	
	$('.thumb').magnificPopup({ 
  		type: 'image'
	// other options
	});

	$('.popup-youtube').magnificPopup({ 
  		type: 'iframe'
	// other options
	});

	
	$( ".booknow a" ).click(function() {
		$(".dropdown-book").removeClass("bookvis");
		$(".dropdown-book").slideToggle( "fast" );
		setTimeout(function(){ $(".dropdown-book").addClass("bookvis"); }, 500); 
		e.preventDefault();
	});
	
	$('.bigthings img').hover(function() {
	    $(this).stop(true, true).animate({ borderWidth: 4 }, "fast" );
	}, function () {
	    $(this).stop(true, true).animate({ borderWidth: 8 }, "fast");
	});
	
	$('.local-block').hover(function() {
		$(this).find('.local-hover').css( "opacity", "1" );
	}, function () {
		$(this).find('.local-hover').css( "opacity", "0" );
	});
	
	$('.slide-gallery-photo').hover(function() {
		$(this).find('.slide-gallery-desc').css( "opacity", "1" );
	}, function() {
		$(this).find('.slide-gallery-desc').css( "opacity", "0" );
	});
	
	$('.view-photos-page').on({
	    mouseenter: function () {
	        $(this).find('.slide-gallery-desc').css( "opacity", "1" );
	    },
	    mouseleave: function () {
	        $(this).find('.slide-gallery-desc').css( "opacity", "0" );
	    }
	}, '.slide-gallery-photo');
	
	
	// Validate Forms
	$("#idssform").validate();
	
	$('input[name=guideFormat]:radio').on('change', function() {
		if(this.value == '2'){
			$('.snailmail').removeClass('hide');
			$('#street').attr('required','');
		}else{
			$('.snailmail').addClass('hide');
		}
	});
	
	
	
	/* Homepage Jumbotron Scale on Scroll */
	 // $(function() {
	 //          var top = $('.masthead').offset().top;
	 //          $(window).scroll(function(evt) {
	 //              var y = $(this).scrollTop();
	 //              console.log(y);
	 //              if (y > top) {
	 //                  $('.slide .views-field-field-homepage-slide-image img').stop().animate({'width': '100%'});
	 //             }
	 //          });
	 //      });
	 // $('.carousel-control').click(function(event){
	 //          event.preventDefault();
	 //      });
	 
	 /* Clears text field of default text */
     $('input.clearme').on('focus', function() {
         if (!$(this).data('defaultText')) $(this).data('defaultText', $(this).val());
         if ($(this).val()==$(this).data('defaultText')) $(this).val('');
     });
     $('input.clearme').on('blur', function() {
         if ($(this).val()=='') $(this).val($(this).data('defaultText')); 
     });
	
	
	/* Responsive jQuery jCarousel */
	$(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var width = jcarousel.innerWidth();

                //if (width >= 768) {
                    width = width / 3;
				//} else {
                    //width = width / 2;
				//}

                jcarousel.jcarousel('items').css('width', width + 'px');
				$('.jcarousel').css('margin-left', (($(window).width() + 150) / 2) * -1);
				$('.jcarousel-control-prev').css('top', ($(window).width() / 6) - 80);
				$('.jcarousel-control-next').css('top', ($(window).width() / 6) - 80);
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
	
	
	// NEAR ME GEOLOCATION
	var geoTimeout = 8000;
	var latitude = "";
	var longitude = "";
	
	$('.nearme a').click(function (e) {
        e.preventDefault();
        var count = 0;

        if (navigator.geolocation) {
            watchID = navigator.geolocation.watchPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;

                count = count + 1;

                if (count == 1) {
                    setTimeout(function () { window.location.href = "/near-me/" + latitude + "," + longitude + "_2000"; navigator.geolocation.clearWatch(watchID); }, 1000);
                }

                if (count > 2) {
                    //redirect
                    window.location.href = "/near-me/" + latitude + "," + longitude + "_2000";
                    navigator.geolocation.clearWatch(watchID);
                }

            }, geo_error, { enableHighAccuracy: true, timeout: geoTimeout, maximumAge: 150 });

        } else {
            error('Geolocation is not supported.');
        }
    });
	
	function geo_error(err) {
	    if (err.code == 1) {
	        alert('Geolocation services are disabled. Please enable location services.')
	    } else if (err.code == 2) {
			alert('Your location is unavailable. You may need to enable location services on your phone.');
	    } else if (err.code == 3) {
	        alert('The request to get your location timed out.');
	    } else {
	        alert('An unknown error occurred while requesting your location.');
	    }
	}

$(".listing-image-thumbnails .thumb-image a").click(function(e){
	e.preventDefault();
	var this_image_url = $(this).find("img").attr("data-url");
	$(".main-listing-image img").attr("src", this_image_url);
});			



$(function() {
  var loc = window.location.href; // returns the full URL
 	 if(/big-appetites/.test(loc)) {
    $('#block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group, #block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group > .form-control, .slide-dropdown-container ').addClass('big-appetites');
  }
  
  else if (/big-secrets/.test(loc)) {
    $('#block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group, #block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group > .form-control, .slide-dropdown-container ').addClass('big-secrets');
  }
  
    else if (/big-memories/.test(loc)) {
    $('#block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group, #block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group > .form-control, .slide-dropdown-container ').addClass('big-memories');
  }
  
    else if (/big-happenings/.test(loc)) {
    $('#block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group, #block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group > .form-control, .dropdown-menu, .slide-dropdown-container ').addClass('big-happenings');
  }
  
   else if (/big-adventures/.test(loc)) {
    $('#block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group, #block-views-a63456fc5587db7059db85066772748c > div > div.view-header > div > div.slide-dropdown-container > div .form-group > .form-control, .slide-dropdown-container ').addClass('big-adventures');
  }
  
   else if (/archives/.test(loc)) {
     $('.container-inline-date .form-item .form-item, .container-inline-date .form-item select, #edit-field-blog-category-tid-wrapper > div > div, #edit-field-field-author-id-target-id-wrapper > div > div,  ').addClass('archive-form-item');
  }
  
  
   	if (/big-happenings/.test(loc)) {
    $('.blog-category').addClass('happenings-category');
  }
      
  else if (/big-secrets/.test(loc)) {
    $('.blog-category').addClass('secrets-category');
  }
  
    else if (/big-memories/.test(loc)) {
    $('.blog-category').addClass('memories-category');
  }

    else if (/big-appetites/.test(loc)) {
    $('.blog-category').addClass('appetites-category');
  }

    else if (/big-adventures/.test(loc)) {
    $('.blog-category').addClass('adventures-category');
  }
 
    if (/big-happenings/.test(loc)) {
    $('.masthead-color').addClass('big-happenings');
  }
      
   if (/big-secrets/.test(loc)) {
    $('.masthead-color').addClass('big-secrets');
  }
  
    if (/big-memories/.test(loc)) {
    $('.masthead-color').addClass('big-memories');
  }

    if (/big-appetites/.test(loc)) {
    $('.masthead-color').addClass('big-appetites');
  }

    if (/big-adventures/.test(loc)) {
    $('.masthead-color').addClass('big-adventures');
  }



    if (/#2112/.test(loc)) {
    $('#nid-2112 > div > div.recent-author-post > div.view.view-blog-roll.view-id-blog_roll.view-display-id-block_6').css("display", "none"); 
     }
  
   if (/#2111/.test(loc)) {
    $('#nid-2111 > div > div.recent-author-post > div.view.view-blog-roll.view-id-blog_roll.view-display-id-block_4').css("display", "none"); 
     }

});


 

	$(document).ready(function(){
	    $("#flip").click(function(){
	        $("#panel").slideToggle("slow");
	    });
	});
	
	
	$("#edit-combine").on("click", function() {
	    if ($(this).val() == "SEARCH")
	        $(this).val("")
	});

	 
});

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});




$('#edit-combine').keypress(function(e){
        if(e.which == 13){
        $("#edit-combine").click();
    }    });
    
    

  
    