<?php if($node->field_photo): ?>
	<div class="slide-gallery-photo">
		
	  <?php
	  //print theme('image_style', array('path' => $node->field_photo['und'][0]['uri'], 'style_name' => 'photo_gallery_image'));
	  ?>
	  <img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
	  
	  <div class="slide-gallery-desc">
	  
		  <?php if($node->title): ?>
		  	  <h1><?php print($node->title); ?></h1>
		  <?php endif; ?>

		  <?php if($node->field_subtitle): ?>
		    <?php print($node->field_subtitle['und'][0]['value']); ?>
		  <?php endif; ?>

		  <?php if($node->field_read_more_link): ?>
		     ... <a href="<?php print($node->field_read_more_link['und'][0]['url']); ?>">Read More</a>
		  <?php endif; ?>
		  
		  <div class="slide-gallery-share">
			<!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php print url($node_url, array('absolute' => TRUE)); ?>">
			<a class="addthis_button_facebook"></a>
			<a class="addthis_button_twitter"></a>
			<a class="addthis_button_google_plusone_share"></a>
			<a class="addthis_button_pinterest_share"></a>
			<a class="addthis_button_compact"></a>
			</div>
			<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
			<!-- AddThis Button END -->
		</div>
		
	  </div>
	  
	</div>
<?php endif; ?>

<?php if($node->field_video): ?>
	<div class="slide-gallery-video video-container">
		<iframe src="//www.youtube.com/embed/<?php print($node->field_video['und'][0]['value']); ?>?controls=0&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
	</div>
<?php endif; ?>