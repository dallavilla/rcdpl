<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<?php if ($teaser): ?>
<!-- teaser template HTML -->
	
	<div class="listing-teaser row">
	
		<div class="listing-teaser-image col-sm-3">
			<a href="<?php print url($node_url, array('absolute' => TRUE)); ?>">
				<?php if($node->field_photo  && !empty($node->field_photo['und'][0]['uri'])): ?>
					<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
				<?php else: ?>
					<img src="/sites/all/themes/rapidcity/images/nodineimage.jpg" class="img-responsive" />
				<?php endif; ?>
			</a>
		</div>
	
		<div class="listing-teaser-content col-sm-5">

			<?php print render($title_prefix); ?>
		    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
			<?php print render($title_suffix); ?>
		
			<?php if($node->body): ?>
				<?php if($node->body['und'][0]['value']): ?>
				  <?php //print $node->body['und'][0]['value']; ?>
				  <?php print render(field_view_value('node', $node, 'body', $body[0],'teaser'))?> <a class="readmore" href="<?php print url($node_url, array('absolute' => TRUE)); ?>">Read More</a>
				<?php endif; ?>
			<?php endif; ?>
			
		
			<div class="listing-teaser-extras">
				
				<?php if($node->idss_venue_website_url): ?>
					<?php if($node->idss_venue_website_url['und'][0]['value']): ?>
						<div class="listing-teaser-links">
							<?php
								$urlStr = $node->idss_venue_website_url['und'][0]['value'];
								$urlparsed = parse_url($urlStr);
								if (empty($urlparsed['scheme'])) {
								    $urlStr = 'http://' . ltrim($urlStr, '/');
								}
							?>
							<a href="<?php print($urlStr); ?>" target="_blank">Visit Website</a>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php print url($node_url, array('absolute' => TRUE)); ?>">
					<span style="float:left;">Share This: &nbsp;</span>
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_google_plusone_share"></a>
				<a class="addthis_button_compact"></a>
				</div>
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
				<!-- AddThis Button END -->
			
			</div>
		
		</div>
	
		<div class="listing-teaser-map col-sm-4">
			<?php //print views_embed_view('map','gmap_block'); ?>
		
			<?php //if($node->idss_vatt_latitude['und'][0]['value']): ?>
				<?php //print views_embed_view('map','gmap_block'); ?>
			<?php //endif; ?>
		</div>
	
	</div>
	
<?php else: ?>
<!-- regular node view template HTML -->

	<div class="listing-page">
		
		<?php print render($title_prefix); ?>
	    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
		<?php print render($title_suffix); ?>
		
		<?php if($node->field_photo  && !empty($node->field_photo['und'][0]['uri'])): ?>
			<div class="listing-image">
				<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_address['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Address:</strong><br /> <?php print($node->idss_venue_address['und'][0]['value']); ?>
			  
				<?php if($node->idss_venue_address_suite && !empty($node->idss_venue_address_suite['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_suite['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_address_3  && !empty($node->idss_venue_address_3['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_3['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_address_4  && !empty($node->idss_venue_address_4['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_4['und'][0]['value']); ?>
				<?php endif; ?>
			  
				<?php if($node->idss_venue_city['und'][0]['value']): ?>
				  <br /><?php print($node->idss_venue_city['und'][0]['value']); ?>,
				<?php endif; ?>
				
				<?php if($node->idss_venue_state['und'][0]['value']): ?>
				  <?php print($node->idss_venue_state['und'][0]['value']); ?>
				  <? else: ?>
				  SD
				<?php endif; ?>
				
				<?php if($node->idss_venue_postal_code['und'][0]['value']): ?>
				  <br /><?php print($node->idss_venue_postal_code['und'][0]['value']); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		
		
		<?php if($node->idss_venue_phone1['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Phone:</strong> <?php print($node->idss_venue_phone1['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		<?php if($node->idss_venue_phone2['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Secondary Phone:</strong> <?php print($node->idss_venue_phone2['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		
		
		<?php if($node->idss_venue_email['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Email:</strong> <a href="mailto:<?php print($node->idss_venue_email['und'][0]['value']); ?>"><?php print($node->idss_venue_email['und'][0]['value']); ?></a>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_website_url['und'][0]['value']): ?>
			<div class="field-section">
				<?php
					$urlStr = $node->idss_venue_website_url['und'][0]['value'];
					$urlparsed = parse_url($urlStr);
					if (empty($urlparsed['scheme'])) {
					    $urlStr = 'http://' . ltrim($urlStr, '/');
					}
				?>
			  <strong>Website:</strong> <a href="<?php print($urlStr); ?>" target="_blank"><?php print($node->idss_venue_website_url['und'][0]['value']); ?></a>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_facebook_url || $node->idss_venue_twitter_url || $node->idss_venue_youtube_url || $node->idss_venue_linkedin_url): ?>
			<div class="field-section social_section">
			  
			  <?php if($node->idss_venue_facebook_url['und'][0]['value']): ?>
				  <?php if (strpos($node->idss_venue_facebook_url['und'][0]['value'],'facebook.com') !== false) { ?>
				      <a href="http://<?php print($node->idss_venue_facebook_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_facebook.png" /></a>
				  <?php } ?>
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_twitter_url['und'][0]['value']): ?>
				  <a href="http://<?php print($node->idss_venue_twitter_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_twitter.png" /></a> 
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_youtube_url['und'][0]['value']): ?>
				   <a href="http://<?php print($node->idss_venue_youtube_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_youtube.png" /></a> 
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_linkedin_url['und'][0]['value']): ?>
				  <a href="http://<?php print($node->idss_venue_linkedin_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_linkedin.png" /></a> 
			  <?php endif; ?>
			  
			</div>
		<?php endif; ?>
		
		<div class="dynamic-fields">
			<ul>
		<?php if($node->idss_vatt_alcohol_served['und'][0]['value']): ?>
			<li>Alcohol Served</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_breakfast['und'][0]['value']): ?>
			<li>Breakfast</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_brunch['und'][0]['value']): ?>
			<li>Brunch</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_catering['und'][0]['value']): ?>
			<li>Catering</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_delivery['und'][0]['value']): ?>
			<li>Delivery</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_dinner['und'][0]['value']): ?>
			<li>Dinner</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_free_wifi['und'][0]['value']): ?>
			<li>Free Wifi</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_handicapped_access['und'][0]['value']): ?>
			<li>Handicapped Access</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_happy_hour['und'][0]['value']): ?>
			<li>Happy Hour</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_hours_of_operation): ?>
			<li>Hours of Operation: <?php print($node->idss_vatt_hours_of_operation['und'][0]['value']); ?></li>
		<?php endif; ?>
		
		<?php if($node->idss_vatt_live_entertainment['und'][0]['value']): ?>
			<li>Live Entertainment</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_lunch['und'][0]['value']): ?>
			<li>Lunch</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_open_seasonally['und'][0]['value']): ?>
			<li>Open Seasonally</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_outdoor_dining['und'][0]['value']): ?>
			<li>Outdoor Dining</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_price['und'][0]['value']): ?>
			
			<?php
				$price = $node->idss_vatt_price['und'][0]['value'];
				if($price == 'Free___'){$price = 'Free-$$';}
				if($price == '__Under_12'){$price = '$-Under$12';}
				if($price == '____12__20'){$price = '$$-$12-$20';}
				if($price == '_____Up to _20'){$price = '$-$$-Up to $20';}
				if($price == '_____21__30'){$price = '$$$-$21-$30';}
				if($price == '______Up to _30'){$price = '$-$$$-Up to $30';}
				if($price == '_____Over _30'){$price = '$$$$-Over $30';}
				if($price == '_______Up to _30_'){$price = '$-$$$$-Up to $30+';}
			?>
			
			<li>Price: <?php print($price); ?></li>
		<?php endif; ?>
		<?php if($node->idss_vatt_private_groups['und'][0]['value']): ?>
			<li>Private Groups</li>
		<?php endif; ?>
		<?php if($node->idss_vatt_reservations['und'][0]['value']): ?>
			<li>Reservations</li>
		<?php endif; ?>
			</ul>
		</div>
		
		
		
		<?php 

			//loop through dynamic fields
			foreach($node as $key => $value)
			{
				if (strrpos($key, "idss_vatt") != false)
				{
					//display
					print($value['und'][0]['value'] . "<br />");
				}
			}
		
		
		?>
		
		
		<?php if($node->body['und'][0]['value']): ?>
			<div class="desc-section">
				<?php print($node->body['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_vatt_latitude['und'][0]['value']): ?>
			<div class="listing-map">
				<?php print views_embed_view('map','gmap_block'); ?>
			</div>
		<?php endif; ?>
		
	</div>
	
  
<?php endif; ?>

</div>

