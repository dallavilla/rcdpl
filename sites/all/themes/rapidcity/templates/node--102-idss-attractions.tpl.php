<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<?php if ($teaser): ?>
<!-- teaser template HTML -->
	
	<div class="listing-teaser row">
	
		<div class="listing-teaser-image col-sm-3">
			<a href="<?php print url($node_url, array('absolute' => TRUE)); ?>">
				<?php if($node->field_photo  && !empty($node->field_photo['und'][0]['uri'])): ?>
					<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
				<?php else: ?>
					<img src="/sites/all/themes/rapidcity/images/noimage.jpg" class="img-responsive" />
				<?php endif; ?>
			</a>
		</div>
	
		<div class="listing-teaser-content col-sm-5">

			<?php print render($title_prefix); ?>
		    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
			<?php print render($title_suffix); ?>
		
			<?php if($node->body): ?>
				<?php if($node->body['und'][0]['value']): ?>
				  <?php //print $node->body['und'][0]['value']; ?>
				  <?php print render(field_view_value('node', $node, 'body', $body[0],'teaser'))?> <a class="readmore" href="<?php print url($node_url, array('absolute' => TRUE)); ?>">Read More</a>
				<?php endif; ?>
			<?php endif; ?>
			
		
			<div class="listing-teaser-extras">
		
				<?php if($node->idss_venue_website_url['und'][0]['value']): ?>
					<?php
						$urlStr = $node->idss_venue_website_url['und'][0]['value'];
						$urlparsed = parse_url($urlStr);
						if (empty($urlparsed['scheme'])) {
						    $urlStr = 'http://' . ltrim($urlStr, '/');
						}
					?>
					<div class="listing-teaser-links">
						<a href="<?php print($urlStr); ?>" target="_blank">Visit Website</a>
					</div>
				<?php endif; ?>
				
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php print url($node_url, array('absolute' => TRUE)); ?>">
					<span style="float:left;">Share This: &nbsp;</span>
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_google_plusone_share"></a>
				<a class="addthis_button_compact"></a>
				</div>
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
				<!-- AddThis Button END -->
			
			</div>
		
		</div>
	
		<div class="listing-teaser-map col-sm-4">
			
			<?php if($node->idss_vatt_latitude): ?>
				<?php if($node->idss_vatt_latitude['und'][0]['value']): ?>
			
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
				<script>
					var map;
					function initialize() {
					  var myLatlng = new google.maps.LatLng(<?php print($node->idss_vatt_latitude['und'][0]['value']); ?>,<?php print($node->idss_vatt_longitude['und'][0]['value']); ?>);
					  var mapOptions = {
					    zoom: 8,
					    center: new google.maps.LatLng(<?php print($node->idss_vatt_latitude['und'][0]['value']); ?>, <?php print($node->idss_vatt_longitude['und'][0]['value']); ?>)
					  };
					  map = new google.maps.Map(document.getElementById('map-<?php print($node->idss_venue_id['und'][0]['value']); ?>'),
					      mapOptions);
					  var marker = new google.maps.Marker({
					        position: myLatlng,
					        map: map
					    });
					}
					google.maps.event.addDomListener(window, 'load', initialize);
				</script>
			
					<div id="map-<?php print($node->idss_venue_id['und'][0]['value']); ?>" style="width:100%;height:150px;" class="map-teaser"></div>
				
				<?php endif; ?>
			<?php endif; ?>
			
		</div>
	
	</div>
	
<?php else: ?>
<!-- regular node view template HTML -->

	<div class="listing-page">
		
		<?php print render($title_prefix); ?>
	    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
		<?php print render($title_suffix); ?>
		
		<?php if($node->field_photo  && !empty($node->field_photo['und'][0]['uri'])): ?>
			
			<div>
				<?php
				
				/*
				<div class="listing-image">
					<!-- <img class="img-responsive" src="<?php //print($node->idss_vatt_picture_1['und'][0]['value']); ?>" /> -->
					<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
				</div>
				*/
				?>
				<div class="main-listing-image">
					<!-- <img class="img-responsive" src="<?php //print($node->idss_vatt_picture_1['und'][0]['value']); ?>" /> -->
					<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
				</div>
				
					<?php 
					$count_images = 0;
					$output_pics = array();
					foreach ($node->field_photo['und'] as $photo)
					{
						if (isset($photo) && isset($photo['uri']) && !empty($photo['uri']) && $photo['uri'] != "public://venue-images/public")
						{
					//print $photo['uri'];
							//thumbnails
							//$output_pics[$count_images] = "<div class=\"thumb-image\">";// . print_r($photo, false);
							$output_pics[$count_images] =	"<a href=\"#\"><img class=\"img-responsive\" src=\"" . image_style_url("listing_thumbnail", $photo['uri']) . "\"  data-url=\"" . file_create_url($photo['uri']) . "\"/></a>";
							//$output_pics[$count_images] .= "</div>";
				
							$count_images++;
						}
					}
					?>
					<div class="listing-image-thumbnails <?php print "cols-" . $count_images?>">
					<?php
					
					if (count($output_pics) > 1)
					{
						foreach ($output_pics as $output_pic)
						{
							print "<div class=\"thumb-image col-num-" . count($output_pics) . "\">";
							print $output_pic;
							print "</div>";
						}
					}
					
					//print $output_pics;
					?>
					<div class="breaker"></div>
				</div>
			</div>
			
			
		<?php endif; ?>
		
		<div class="badges">
			<?php if($node->idss_venue_address['und'][0]['value'] == ''): ?>
			<?php endif; ?>
		</div>
		
		<?php if($node->idss_venue_address['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Address:</strong><br /> <?php print($node->idss_venue_address['und'][0]['value']); ?>
			  
				<?php if($node->idss_venue_address_suite && !empty($node->idss_venue_address_suite['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_suite['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_address_3  && !empty($node->idss_venue_address_3['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_3['und'][0]['value']); ?>
				<?php endif; ?>
				
				<?php if($node->idss_venue_address_4  && !empty($node->idss_venue_address_4['und'][0]['value'])): ?>
				  <br /><?php print($node->idss_venue_address_4['und'][0]['value']); ?>
				<?php endif; ?>
			  
				<?php if($node->idss_venue_city['und'][0]['value']): ?>
				  <br /><?php print($node->idss_venue_city['und'][0]['value']); ?>,
				<?php endif; ?>
				
				<?php if($node->idss_venue_state['und'][0]['value']): ?>
				  <?php print($node->idss_venue_state['und'][0]['value']); ?>
				  <? else: ?>
				  SD
				<?php endif; ?>
				
				<?php if($node->idss_venue_postal_code['und'][0]['value']): ?>
				  <br /><?php print($node->idss_venue_postal_code['und'][0]['value']); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		
		
		<?php if($node->idss_venue_phone1['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Phone:</strong> <?php print($node->idss_venue_phone1['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		
		
		<?php if($node->idss_venue_email['und'][0]['value']): ?>
			<div class="field-section">
			  <strong>Email:</strong> <a href="mailto:<?php print($node->idss_venue_email['und'][0]['value']); ?>"><?php print($node->idss_venue_email['und'][0]['value']); ?></a>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_website_url['und'][0]['value']): ?>
			<div class="field-section">
				<?php
					$urlStr = $node->idss_venue_website_url['und'][0]['value'];
					$urlparsed = parse_url($urlStr);
					if (empty($urlparsed['scheme'])) {
					    $urlStr = 'http://' . ltrim($urlStr, '/');
					}
				?>
			  <strong>Website:</strong> <a href="<?php print($urlStr); ?>" target="_blank"><?php print($node->idss_venue_website_url['und'][0]['value']); ?></a>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_venue_facebook_url || $node->idss_venue_twitter_url || $node->idss_venue_youtube_url || $node->idss_venue_linkedin_url): ?>
			<div class="field-section social_section">
			  
			  <?php if($node->idss_venue_facebook_url['und'][0]['value']): ?>
				  <?php if (strpos($node->idss_venue_facebook_url['und'][0]['value'],'facebook.com') !== false) { ?>
				      <a href="http://<?php print($node->idss_venue_facebook_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_facebook.png" /></a>
				  <?php } ?>
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_twitter_url['und'][0]['value']): ?>
				  <a href="http://<?php print($node->idss_venue_twitter_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_twitter.png" /></a> 
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_youtube_url['und'][0]['value']): ?>
				   <a href="http://<?php print($node->idss_venue_youtube_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_youtube.png" /></a> 
			  <?php endif; ?>
			  
			  <?php if($node->idss_venue_linkedin_url['und'][0]['value']): ?>
				  <a href="http://<?php print($node->idss_venue_linkedin_url['und'][0]['value']); ?>" target="_blank"><img src="/sites/all/themes/rapidcity/images/icon_linkedin.png" /></a> 
			  <?php endif; ?>
			  
			</div>
		<?php endif; ?>
		
		<div class="dynamic-fields">
			<ul>
				<?php if($node->idss_vatt_price): ?>
					
					<?php
						$price = $node->idss_vatt_price['und'][0]['value'];
						if($price == 'Free___'){$price = 'Free-$$';}
						if($price == '__Under_12'){$price = '$-Under$12';}
						if($price == '____12__20'){$price = '$$-$12-$20';}
						if($price == '_____Up to _20'){$price = '$-$$-Up to $20';}
						if($price == '_____21__30'){$price = '$$$-$21-$30';}
						if($price == '______Up to _30'){$price = '$-$$$-Up to $30';}
						if($price == '_____Over _30'){$price = '$$$$-Over $30';}
						if($price == '_______Up to _30_'){$price = '$-$$$$-Up to $30+';}
					?>
					
					<li>Price: <?php print($price); ?></li>
				<?php endif; ?>
				<?php if($node->idss_vatt_hours_of_operation): ?>
					<li>Hours of Operation: <?php print($node->idss_vatt_hours_of_operation['und'][0]['value']); ?></li>
				<?php endif; ?>
				<?php if($node->idss_vatt_days_closed): ?>
					<li>Days Closed: <?php print($node->idss_vatt_days_closed['und'][0]['value']); ?></li>
				<?php endif; ?>
				<?php if($node->idss_vatt_family_friendly['und'][0]['value']): ?>
					<li>Family Friendly</li>
				<?php endif; ?>
				<?php if($node->idss_vatt_food_service['und'][0]['value']): ?>
					<li>Food Service</li>
				<?php endif; ?>
				<?php if($node->idss_vatt_gift_shop['und'][0]['value']): ?>
					<li>Gift Shop</li>
				<?php endif; ?>
				<?php if($node->idss_vatt_handicapped_access['und'][0]['value']): ?>
					<li>Handicapped Access</li>
				<?php endif; ?>
				<?php if($node->idss_vatt_open_seasonally['und'][0]['value']): ?>
					<li>Open Seasonally</li>
				<?php endif; ?>
				<?php if($node->idss_vatt_pet_friendly['und'][0]['value']): ?>
					<li>Pet Friendly</li>
				<?php endif; ?>
			</ul>
		</div>
		
		
		<?php if($node->body['und'][0]['value']): ?>
			<div class="desc-section">
				<?php print($node->body['und'][0]['value']); ?>
			</div>
		<?php endif; ?>
		
		<?php if($node->idss_vatt_latitude['und'][0]['value']): ?>
			<div class="listing-map">
				<?php print views_embed_view('map','gmap_block'); ?>
			</div>
		<?php endif; ?>
		
	</div>
	
  
<?php endif; ?>

</div>