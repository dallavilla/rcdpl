<div class="dropdown-bg"></div>

<!-- Start Booking Widget -->
<div class="dropdown-book">
	<div class="container">
		<div class="row">
			<script src="http://widgets.bookdirect.net/widgets/search_widget.js.php?id=758" type="text/javascript"></script>
		</div>
	</div>
</div>
<!-- End Booking Widget -->

<header id="navbar" role="banner" class="<?php //print $navbar_classes; ?>">
  <div class="navbar-container container">
	  
	  <div class="navbar-left">
	    <div class="navbar-header">
	      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>

		<div class="navbar-collapse collapse">
			<nav role="navigation">
  			  <?php //if (!empty($secondary_nav)): ?>
  			    <?php //print render($secondary_nav); ?>
  			  <?php //endif; ?>
			  <ul class="menu nav navbar-nav secondary">
				  <li class="first collapsed"><a href="/meeting-planners" title="">Meeting Planners</a></li>
				  <li class="collapsed"><a href="/press-media-inquiries">Press &amp; Media Inquiries</a></li>
				  <li class="collapsed"><a href="/sporting-events-planners" title="">Sporting Events Planners</a></li>
				  <li class="last collapsed"><a href="/travel-professionals" title="">Travel Professionals</a></li>
			  </ul>
			  <div class="head-social-icons">
  				<a class="head-social-icon head-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_head_facebook.png" alt="Facebook" /></a>
  				<a class="head-social-icon head-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_head_twitter.png" alt="Twitter" /></a>
  				<a class="head-social-icon head-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_head_pinterest.png" alt="Pinterest" /></a>
  				<a class="head-social-icon head-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_head_youtube.png" alt="YouTube"/></a>
  				<a class="head-social-icon head-instagram" href="http://www.instagram.com/VisitRapidCity" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_head_instagram.png" alt="Instagram" /></a>
			  </div>
			  <?php if (!empty($primary_nav)): ?>
			    <?php print render($primary_nav); ?>
			  <?php endif; ?>
			</nav>
		</div>
	</div>
	
	<div class="navbar-right">
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print t('Home'); ?></a>
		<div class="quicklinks">
			<div class="signin">
				<!-- <a href="/signin">Sign In/Register</a> -->
			</div>
			<div class="booknow">
				<a href="#">Book Now</a>
			</div>
		</div>
		<div class="nearme mobile-only">
			<a href="/near-me">What's Nearby?</a>
		</div>
		<?php if (!empty($page['navigation'])): ?>
			<?php print render($page['navigation']); ?>
		<?php endif; ?>
		<?php if (!empty($page['search'])): ?>
			<?php print render($page['search']); ?>
		<?php endif; ?>
	</div>
	  
  </div>
</header>

<div class="masthead">
	<div class="container">
		<div class="row">
		    <a id="main-content"></a>
			
			<?php if (!empty($page['masthead'])): ?>
			<div class="masthead-extra">	
				<?php print render($page['masthead']); ?>
			</div>
			<div class="masthead-strip">	
			<?php else: ?>
			<div class="masthead-simple">	
			<?php endif; ?>
			
				<?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
		        <?php print render($title_prefix); ?>
		        <?php if (!empty($title)): ?>
		          <h1 class="page-header"><?php print $title; ?></h1>
		        <?php endif; ?>
		        <?php print render($title_suffix); ?>
				
			</div>
		</div>
	</div>
</div>

<div class="main-container container">
	
  <div class="row">

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="sidebar-second col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>

		<div class="explore-map">
			<?php include 'rapid-city-map/explore_map.php'; ?>
		</div>


    </section>

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="sidebar-first col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

  </div>
</div>

<?php if (!empty($page['footer'])): ?>
	<?php print render($page['footer']); ?>
<?php endif; ?>

<div class="footer-fixed">
	<div class="foothill-left"></div>
	<div class="foothill-right"></div>
	<div class="footer-fixed-content">
		<?php print render($page['footer_fixed']); ?>
	</div>
</div>
