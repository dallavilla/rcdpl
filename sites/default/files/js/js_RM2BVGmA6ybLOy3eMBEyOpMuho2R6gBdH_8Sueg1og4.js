(function ($) {

/**
 * Attaches sticky table headers.
 */
Drupal.behaviors.tableHeader = {
  attach: function (context, settings) {
    if (!$.support.positionFixed) {
      return;
    }

    $('table.sticky-enabled', context).once('tableheader', function () {
      $(this).data("drupal-tableheader", new Drupal.tableHeader(this));
    });
  }
};

/**
 * Constructor for the tableHeader object. Provides sticky table headers.
 *
 * @param table
 *   DOM object for the table to add a sticky header to.
 */
Drupal.tableHeader = function (table) {
  var self = this;

  this.originalTable = $(table);
  this.originalHeader = $(table).children('thead');
  this.originalHeaderCells = this.originalHeader.find('> tr > th');
  this.displayWeight = null;

  // React to columns change to avoid making checks in the scroll callback.
  this.originalTable.bind('columnschange', function (e, display) {
    // This will force header size to be calculated on scroll.
    self.widthCalculated = (self.displayWeight !== null && self.displayWeight === display);
    self.displayWeight = display;
  });

  // Clone the table header so it inherits original jQuery properties. Hide
  // the table to avoid a flash of the header clone upon page load.
  this.stickyTable = $('<table class="sticky-header"/>')
    .insertBefore(this.originalTable)
    .css({ position: 'fixed', top: '0px' });
  this.stickyHeader = this.originalHeader.clone(true)
    .hide()
    .appendTo(this.stickyTable);
  this.stickyHeaderCells = this.stickyHeader.find('> tr > th');

  this.originalTable.addClass('sticky-table');
  $(window)
    .bind('scroll.drupal-tableheader', $.proxy(this, 'eventhandlerRecalculateStickyHeader'))
    .bind('resize.drupal-tableheader', { calculateWidth: true }, $.proxy(this, 'eventhandlerRecalculateStickyHeader'))
    // Make sure the anchor being scrolled into view is not hidden beneath the
    // sticky table header. Adjust the scrollTop if it does.
    .bind('drupalDisplaceAnchor.drupal-tableheader', function () {
      window.scrollBy(0, -self.stickyTable.outerHeight());
    })
    // Make sure the element being focused is not hidden beneath the sticky
    // table header. Adjust the scrollTop if it does.
    .bind('drupalDisplaceFocus.drupal-tableheader', function (event) {
      if (self.stickyVisible && event.clientY < (self.stickyOffsetTop + self.stickyTable.outerHeight()) && event.$target.closest('sticky-header').length === 0) {
        window.scrollBy(0, -self.stickyTable.outerHeight());
      }
    })
    .triggerHandler('resize.drupal-tableheader');

  // We hid the header to avoid it showing up erroneously on page load;
  // we need to unhide it now so that it will show up when expected.
  this.stickyHeader.show();
};

/**
 * Event handler: recalculates position of the sticky table header.
 *
 * @param event
 *   Event being triggered.
 */
Drupal.tableHeader.prototype.eventhandlerRecalculateStickyHeader = function (event) {
  var self = this;
  var calculateWidth = event.data && event.data.calculateWidth;

  // Reset top position of sticky table headers to the current top offset.
  this.stickyOffsetTop = Drupal.settings.tableHeaderOffset ? eval(Drupal.settings.tableHeaderOffset + '()') : 0;
  this.stickyTable.css('top', this.stickyOffsetTop + 'px');

  // Save positioning data.
  var viewHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
  if (calculateWidth || this.viewHeight !== viewHeight) {
    this.viewHeight = viewHeight;
    this.vPosition = this.originalTable.offset().top - 4 - this.stickyOffsetTop;
    this.hPosition = this.originalTable.offset().left;
    this.vLength = this.originalTable[0].clientHeight - 100;
    calculateWidth = true;
  }

  // Track horizontal positioning relative to the viewport and set visibility.
  var hScroll = document.documentElement.scrollLeft || document.body.scrollLeft;
  var vOffset = (document.documentElement.scrollTop || document.body.scrollTop) - this.vPosition;
  this.stickyVisible = vOffset > 0 && vOffset < this.vLength;
  this.stickyTable.css({ left: (-hScroll + this.hPosition) + 'px', visibility: this.stickyVisible ? 'visible' : 'hidden' });

  // Only perform expensive calculations if the sticky header is actually
  // visible or when forced.
  if (this.stickyVisible && (calculateWidth || !this.widthCalculated)) {
    this.widthCalculated = true;
    var $that = null;
    var $stickyCell = null;
    var display = null;
    var cellWidth = null;
    // Resize header and its cell widths.
    // Only apply width to visible table cells. This prevents the header from
    // displaying incorrectly when the sticky header is no longer visible.
    for (var i = 0, il = this.originalHeaderCells.length; i < il; i += 1) {
      $that = $(this.originalHeaderCells[i]);
      $stickyCell = this.stickyHeaderCells.eq($that.index());
      display = $that.css('display');
      if (display !== 'none') {
        cellWidth = $that.css('width');
        // Exception for IE7.
        if (cellWidth === 'auto') {
          cellWidth = $that[0].clientWidth + 'px';
        }
        $stickyCell.css({'width': cellWidth, 'display': display});
      }
      else {
        $stickyCell.css('display', 'none');
      }
    }
    this.stickyTable.css('width', this.originalTable.outerWidth());
  }
};

})(jQuery);
;
(function($) {
  var ThemeKey = ThemeKey || {};

  ThemeKey.oldParentId;

  ThemeKey.adjustChildCounter = function (parentId, adjust) {
    var childCounter = $('#themekey-num-childs-' + parentId);
    childCounter.val(parseInt(childCounter.val()) + adjust);
    var propertiesRow = $('#themekey-properties-row-' + parentId);
    if (1 == adjust) {
      $('.themekey-property-theme', propertiesRow).css('display', 'none');
      $('.themekey-property-append-path', propertiesRow).css('display', 'none');
      $('.themekey-property-append-path + label', propertiesRow).css('display', 'none');
      $('.themekey-rule-delete-link', propertiesRow).css('display', 'none');
    }
    else if (childCounter.val() < 1) {
      $('.themekey-property-theme', propertiesRow).css('display', 'block');
      $('.themekey-property-append-path', propertiesRow).css('display', 'inline');
      $('.themekey-property-append-path + label', propertiesRow).css('display', 'inline');
      $('.themekey-rule-delete-link', propertiesRow).css('display', 'block');
    }
  };

  ThemeKey.disableChilds = function (parentId) {
    var childRows = $("option[selected][value='" + parentId + "']", $('.themekey-property-parent')).parents("tr[id^='themekey-properties-row-']");
    childRows.each(function () {
      var childRow = $(this);
      var enabledElement = $('.themekey-property-enabled', childRow);
      enabledElement.css('display', 'none');
      if (enabledElement.attr('checked')) {
        enabledElement.removeAttr('checked');
        childRow.addClass('themekey-fade-out');
        ThemeKey.adjustChildCounter(parentId, -1);
        ThemeKey.disableChilds($('.themekey-property-id', childRow).val());
      }
    });
  };

  ThemeKey.allowEnablingDirectChilds = function (parentId) {
    var childRows = $("option[selected][value='" + parentId + "']", $('.themekey-property-parent')).parents("tr[id^='themekey-properties-row-']");
    childRows.each(function () {
      $('.themekey-property-enabled', $(this)).css('display', 'block');
    });
  };

  if (Drupal.tableDrag) {
    Drupal.tableDrag.prototype.onDrag = function() {
      ThemeKey.oldParentId = $('.themekey-property-parent', $(this.rowObject.element)).val();
      return null;
    };

    Drupal.tableDrag.prototype.onDrop = function() {
      if (this.changed) {
        var rowElement = $(this.rowObject.element);
        var newParentId = $('.themekey-property-parent', rowElement).val();
        var enabledElement = $('.themekey-property-enabled', rowElement);

        if (enabledElement.attr('checked')) {
          if (0 < ThemeKey.oldParentId) {
            ThemeKey.adjustChildCounter(ThemeKey.oldParentId, -1);
          }

          if (0 < newParentId) {
            var parentEnabledElement = $('.themekey-property-enabled', $('#themekey-properties-row-' + newParentId));
            if (parentEnabledElement.attr('checked')) {
              ThemeKey.adjustChildCounter(newParentId, 1);
            }
            else {
              enabledElement.removeAttr('checked');
              enabledElement.css('display', 'none');
              rowElement.addClass('themekey-fade-out');
              // hide and disable children
              var id = enabledElement.attr('name').replace('old_items[', '').replace('][enabled]', '');
              ThemeKey.disableChilds(id);
            }
          }
        }
        else {
          if (0 < newParentId) {
            var parentEnabledElement = $('.themekey-property-enabled', $('#themekey-properties-row-' + newParentId));
            if (parentEnabledElement.attr('checked')) {
              enabledElement.css('display', 'block');
            }
            else {
              enabledElement.css('display', 'none');
            }
          }
        }

        if (0 < newParentId) {
          if (0 >= ThemeKey.oldParentId) {
            rowElement.removeClass('themekey-top-level');
          }
        }
        else {
          enabledElement.css('display', 'block');
          rowElement.addClass('themekey-top-level');
        }
      }
      return null;
    };
  }

  Drupal.behaviors.ThemeKey = {
    attach: function(context) {
      $('.themekey-property-property').change(
        function() {
          var wildcardElement = $('#' + $(this).attr('id').replace('property', 'wildcard'));
          if ('drupal:path:wildcard' == $(this).val()) {
            wildcardElement.css('display', 'block');
          }
          else {
            wildcardElement.css('display', 'none');
          }

          var propertyName = $(this).val().replace(':', '-').replace(':', '-').replace('_', '-').replace('_', '-');

          var pageCacheIconElement = $('#' + $(this).attr('id').replace('property', 'page-cache-icon'));
          pageCacheIconElement.empty();
          pageCacheIconElement.append($('#themekey-page-cache-' + propertyName).html());

          var valueHelpElement = $('#' + $(this).attr('id').replace('property', 'value-help'));
          var helpText = $('#themekey-value-help-' + propertyName).html();
          valueHelpElement.attr('title', helpText);
          while (helpText.match("'")) {
            helpText = helpText.replace("'", '"');
          }
          valueHelpElement.attr('onClick', "alert('" + helpText + "')");
        }
      );

      $('.themekey-property-enabled').change(
        function() {
          var id = $(this).attr('name').replace('old_items[', '').replace('][enabled]', '');
          var rowElement = $('#themekey-properties-row-' + id);
          var parentId = $('.themekey-property-parent', rowElement).val();
          if ($(this).attr('checked')) {
            rowElement.removeClass('themekey-fade-out');
            ThemeKey.adjustChildCounter(parentId, 1);
            ThemeKey.allowEnablingDirectChilds(id);
          }
          else {
            rowElement.addClass('themekey-fade-out');
            ThemeKey.adjustChildCounter(parentId, -1);
            // hide and disable children
            ThemeKey.disableChilds(id);
          }
        }
      );
    }
  };
})(jQuery);;
