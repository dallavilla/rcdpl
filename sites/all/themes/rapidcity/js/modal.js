(function ($) {
$(function(e) {                       //run when the DOM is ready
  $("a.popup-with-form").click(function() {  //use a class, since your ID gets mangled
    $('.visitor-form-popup').toggleClass("visitor-form-active");      //add the class to the popup element
      $('.modal-background').toggleClass("modal-background-active");      //add the class to the popup element
      $('.modal-background').click(function() {
            $('.visitor-form-popup').removeClass("visitor-form-active"); 
          $('.modal-background').removeClass("modal-background-active");
           });
  });
});
})(jQuery);
