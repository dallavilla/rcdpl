<?php

// distributionListIDs from Rapid City IDSS
if(isset($_POST['guide'])) { $guide = $_POST['guide']; } else { $guide = ''; }
if($guide == 'Visitors Guide'){$guide = '844';} // Website Newsletter
if($guide == 'Meeting Professionals Guide'){$guide = '796';} // Website Newsletter - Meetings
if($guide == 'Sports and Events Planners Guide'){$guide = '797';} // Website Newsletter - Sports
if($guide == 'Travel Professionals Guide'){$guide = '798';} // Website Newsletter - Travel

if(isset($_POST['guideFormat'])) { $guideFormat = $_POST['guideFormat']; } else { $guideFormat = ''; }
   
if(isset($_POST['salutation'])) { $salutation = $_POST['salutation']; } else { $salutation = ''; }
if(isset($_POST['firstName'])) { $firstName = $_POST['firstName']; } else { $firstName = ''; }
if(isset($_POST['middleName'])) { $middleName = $_POST['middleName']; } else { $middleName = ''; }
if(isset($_POST['lastName'])) { $lastName = $_POST['lastName']; } else { $lastName = ''; }
if(isset($_POST['title'])) { $title = $_POST['title']; } else { $title = ''; }
if(isset($_POST['companyName'])) { $companyName = $_POST['companyName']; } else { $companyName = ''; }
if(isset($_POST['phone'])) { $phone = $_POST['phone']; } else { $phone = ''; }
if(isset($_POST['homePhone'])) { $homePhone = $_POST['homePhone']; } else { $homePhone = ''; }
//if(isset($_POST['faxPhone'])) { $faxPhone = $_POST['faxPhone']; } else { $faxPhone = ''; }
if(isset($_POST['emailFrom'])) { $emailFrom = $_POST['emailFrom']; } else { $emailFrom = ''; }
if(isset($_POST['otherEmail'])) { $otherEmail = $_POST['otherEmail']; } else { $otherEmail = ''; }
if(isset($_POST['street'])) { $street = $_POST['street']; } else { $street = ''; }
if(isset($_POST['suite'])) { $suite = $_POST['suite']; } else { $suite = ''; }
if(isset($_POST['city'])) { $city = $_POST['city']; } else { $city = ''; }
if(isset($_POST['countryAbbr'])) { $countryAbbr = $_POST['countryAbbr']; } else { $countryAbbr = ''; }
if( $countryAbbr == '' ){ $countryAbbr='US'; }
if(isset($_POST['stateAbbr'])) { $stateAbbr = $_POST['stateAbbr']; } else { $stateAbbr = ''; }
if($stateAbbr == ''){ $stateAbbr = 'Unknown'; }
if($guideFormat == '2'){
	if( $stateAbbr == 'Other-International' ){ $stateAbbr = $_POST['countryAbbr']; }
}
if(isset($_POST['postalCode'])) { $postalCode = $_POST['postalCode']; } else { $postalCode = ''; }
if(isset($_POST['comments'])) { $comments = $_POST['comments']; } else { $comments = ''; }
if(isset($_POST['optin'])) { $optin = $_POST['optin']; } else { $optin = 'off'; } //optin values: on | off


header('Location:/guide-thank-you?guide='.$guide.'&tp='.$guideFormat.'&fn='.$firstName.'&ln='.$lastName.'&st='.$street.'&ct='.$city.'&sa='.$stateAbbr.'&pc='.$postalCode.'&em='.$emailFrom);

$_SESSION['guidepass'] = $_POST['guide'];


$clientWS = new SoapClient('http://ws.idssasp.com/Prospects.asmx?wsdl');
$namespaceWS = 'http://ws.idssasp.com/Prospects.asmx';

$dmsClientU = 'rapidcity';
$dmsClientP = '783889d7-a690-42f1-b54c-1bad2fe0e852';

$headerBodyWS = array('UserName' => $dmsClientU, 'Password' => $dmsClientP);
$headerWS = new SOAPHeader($namespaceWS, 'AuthorizeHeader', $headerBodyWS, false);
$clientWS->__setSoapHeaders(array($headerWS));

$parameters = array('salutation' => $salutation, 'firstName' => $firstName, 'middleName' => $middleName, 'lastName' => $lastName, 'title' => $title, 'companyName' => $companyName, 'phone' => $phone, 'homePhone' => $homePhone, 'mobilePhone' => '', 'fax' => '', 'email' => $emailFrom, 'otherEmail' => $otherEmail, 'street' => $street, 'suite' => $suite, 'city' => $city, 'countryAbbr' => $countryAbbr, 'stateAbbr' => $stateAbbr, 'postalCode' => $postalCode, 'sourceID' => 1796, 'comments' => $comments, 'validateCountryState' => 'false' );

$results = $clientWS->CreateProspect($parameters);

$newID = get_object_vars($results);
$newNum = $newID[CreateProspectResult];

//print_r($newNum);
//print_r('<br/><br/>');

if ($optin == 'on'){
	$parametersd = array('prospectID' => $newNum, 'distributionListID' => $guide, );
	$resultsd = $clientWS->AddProspectToDistributionList($parametersd);
}

//print_r( $resultsd );
//print_r('<br/><br/>');

$today = date('Y-m-d');

if($guideFormat == '1'){ //if digital download
	if($guide == '844'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Visitor Guide _ Downloaded', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, 'sentViaCode' => 'Digital Download', 'sentDate' => $today, 'sentBy' => 'Jessica Ross', );
	}
	if($guide == '796'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Meeting Planners Guide _ Downloaded', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, 'sentViaCode' => 'Digital Download', 'sentDate' => $today, 'sentBy' => 'Jessica Ross', );
	}
	if($guide == '797'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Sports Professionals Guide _ Downloaded', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, 'sentViaCode' => 'Digital Download', 'sentDate' => $today, 'sentBy' => 'Jessica Ross', );
	}
	if($guide == '798'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Travel Professional Guide _ Download', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, 'sentViaCode' => 'Digital Download', 'sentDate' => $today, 'sentBy' => 'Jessica Ross', );
	}
	$resultsk = $clientWS->CreateProspectRequestWithSentVia($parametersk);
}
if($guideFormat == '2'){ //if snail mail
	if($guide == '844'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Visitor Guide _ Mailed', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, );
	}
	if($guide == '796'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Meeting Planner Guide', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, );
	}
	if($guide == '797'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Sports Professional Guide', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, );
	}
	if($guide == '798'){
		$parametersk = array('prospectID' => $newNum, 'itemCode' => 'Travel Professionals Guide', 'quantity' => 1, 'requestDate' => $today, 'sourceID' => 1796, );
	}
	$resultsk = $clientWS->CreateProspectRequestWithSource($parametersk);
}



if(isset($_POST['travelInterestsFamily'])) {
	$travelInterestsFamily = $_POST['travelInterestsFamily'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $travelInterestsFamily, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['travelInterestsGrown'])) {
	$ttravelInterestsGrown = $_POST['travelInterestsGrown'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $ttravelInterestsGrown, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['travelInterestsParks'])) {
	$travelInterestsParks = $_POST['travelInterestsParks'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $travelInterestsParks, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['travelInterestsHistory'])) {
	$travelInterestsHistory = $_POST['travelInterestsHistory'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $travelInterestsHistory, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['travelInterestsOutdoor'])) {
	$travelInterestsOutdoor = $_POST['travelInterestsOutdoor'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $travelInterestsOutdoor, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['travelInterestsWinter'])) {
	$travelInterestsWinter = $_POST['travelInterestsWinter'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $travelInterestsWinter, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['travelInterestsEvents'])) {
	$travelInterestsEvents = $_POST['travelInterestsEvents'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $travelInterestsEvents, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}

if(isset($_POST['findSite'])) {
	$findSite = $_POST['findSite'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $findSite, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}
if(isset($_POST['planToVisit'])) {
	$planToVisit = $_POST['planToVisit'];
	$parametersg = array('prospectID' => $newNum, 'connectionItemID' => $planToVisit, );
	$resultsg = $clientWS->CreateProspectConnectionItem($parametersg);
}


//print_r( $resultsg );

exit();


?>