
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="en" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/themes/rapidcity/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="Travel Frame RSS" href="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/rss.xml" />
  <title>Travel Frame</title>
  <style>@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/modules/system/system.base.css?n0j72j");</style>
<style>@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/calendar/css/calendar_multiday.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/date/date_api/date.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/date/date_repeat_field/date_repeat_field.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/modules/field/theme/field.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/logintoboggan/logintoboggan.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/views/css/views.css?n0j72j");</style>
<style>@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/ckeditor/ckeditor.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/ctools/css/ctools.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/jcarousel/skins/default/jcarousel-default.css?n0j72j");</style>
<link type="text/css" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" media="all" />
<style>@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/themes/bootstrap/css/overrides.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/themes/rapidcity/css/style.css?n0j72j");
@import url("http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/themes/rapidcity/css/navigation.css?n0j72j");</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.7/jquery.js'>\x3C/script>")</script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/misc/jquery.once.js?v=1.2"></script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/misc/drupal.js?n0j72j"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/admin_menu/admin_devel/admin_devel.js?n0j72j"></script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/views_bootstrap/js/views-bootstrap-carousel.js?n0j72j"></script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/jcarousel/js/jquery.jcarousel.min.js?n0j72j"></script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/modules/jcarousel/js/jcarousel.js?n0j72j"></script>
<script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/themes/rapidcity/js/explore_map_homepage.js?n0j72j"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"rapidcity","theme_token":"sqGeAB6kHzsQQENJs6unnRfRD9GSGxrl6yrKlktoKHo","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"https:\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.7.1\/jquery.js":1,"0":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"\/\/netdna.bootstrapcdn.com\/bootstrap\/3.0.2\/js\/bootstrap.min.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/views_bootstrap\/js\/views-bootstrap-carousel.js":1,"sites\/all\/modules\/jcarousel\/js\/jquery.jcarousel.min.js":1,"sites\/all\/modules\/jcarousel\/js\/jcarousel.js":1,"sites\/all\/themes\/rapidcity\/js\/scripts.js":1,"sites\/all\/themes\/rapidcity\/js\/explore_map_homepage.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/jcarousel\/skins\/default\/jcarousel-default.css":1,"\/\/netdna.bootstrapcdn.com\/bootstrap\/3.0.2\/css\/bootstrap.min.css":1,"sites\/all\/themes\/bootstrap\/css\/overrides.css":1,"sites\/all\/themes\/rapidcity\/css\/style.css":1,"sites\/all\/themes\/rapidcity\/css\/navigation.css":1}},"jcarousel":{"ajaxPath":"\/jcarousel\/ajax\/views","carousels":{"jcarousel-dom-1":{"view_options":{"view_args":"","view_path":"node","view_base_path":null,"view_display_id":"homepage_carousel","view_name":"homepage_slides","jcarousel_dom_id":1},"skin":"default","visible":3,"autoPause":1,"start":1,"scroll":3,"selector":".jcarousel-dom-1"}}},"viewsBootstrap":{"carousel":{"1":{"id":1,"name":"homepage_slides","attributes":{"interval":5000,"pause":false}}}},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-node" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="dropdown-bg"></div>

<!-- Start Booking Widget -->
<div class="dropdown-book">
	<div class="container">
		<div class="row">
			<span class="book-title">Book A Room</span> <input class="date-pick" id="start-date" name="start-date" readonly="readonly" type="text" value="Check-In" /><img class="ui-datepicker-trigger" src="/sites/all/themes/rapidcity/images/icon_cal.jpg" /> <input class="date-pick" id="end-date" name="end-date" readonly="readonly" type="text" value="Check-Out" /><img class="ui-datepicker-trigger" src="/sites/all/themes/rapidcity/images/icon_cal.jpg" /> <select class="select-pick"><option selected="selected" value="0">All Lodgings</option><option value="1">One</option><option value="2">Two</option><option value="3">Three</option></select> <input class="form-submit" name="Submit" src="/sites/all/themes/rapidcity/images/btn_search.jpg" type="image" /></div>
	</div>
</div>
<!-- End Booking Widget -->

<header id="navbar" role="banner" class="">
  <div class="navbar-container container">
	  
	  <div class="navbar-left">
	    <div class="navbar-header">
	      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>

		<div class="navbar-collapse collapse">
			<nav role="navigation">
  			    			    <ul class="menu nav navbar-nav secondary"><li class="first leaf"><a href="http://www.visitrapidcity.com/meetingprofessionals/" title="">Meeting Planners</a></li>
<li class="leaf"><a href="http://www.visitrapidcity.com/media/" title="">Press &amp; Meeting Inquiries</a></li>
<li class="leaf"><a href="http://www.visitrapidcity.com/sportseventsplanners/" title="">Sporting Events Planners</a></li>
<li class="last leaf"><a href="http://www.visitrapidcity.com/travelprofessionals/" title="">Travel Professionals</a></li>
</ul>  			  			  <div class="head-social-icons">
  				<a class="head-social-icon head-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_head_facebook.png" /></a>
  				<a class="head-social-icon head-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_head_twitter.png" /></a>
  				<a class="head-social-icon head-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_head_pinterest.png" /></a>
  				<a class="head-social-icon head-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_head_youtube.png" /></a>
  				<a class="head-social-icon head-instagram" href="#" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_head_instagram.png" /></a>
			  </div>
			  			    <ul class="menu nav navbar-nav"><li class="first expanded dropdown"><a href="/things-to-do" title="" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Things To Do <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/things-to-do/arts-culture" title="">Arts &amp; Culture</a></li>
<li class="leaf"><a href="/things-to-do/attractions" title="">Attractions</a></li>
<li class="leaf"><a href="/things-to-do/downtown" title="">Downtown</a></li>
<li class="leaf"><a href="/things-to-do/outdoor" title="">Outdoor</a></li>
<li class="leaf"><a href="/things-to-do/parks-monuments" title="">Parks &amp; Monuments</a></li>
<li class="leaf"><a href="/things-to-do/rushmore" title="">Rushmore</a></li>
<li class="leaf"><a href="/things-to-do/scenic-drives" title="">Scenic Drives</a></li>
<li class="leaf"><a href="/things-to-do/shopping" title="">Shopping</a></li>
<li class="leaf"><a href="/things-to-do/sports" title="">Sports</a></li>
<li class="last leaf"><a href="/things-to-do/top-10-lists" title="">Top 10 Lists</a></li>
</ul></li>
<li class="leaf"><a href="/stay">Stay</a></li>
<li class="leaf"><a href="/dining">Dine</a></li>
<li class="leaf"><a href="/events" title="">Events</a></li>
<li class="leaf"><a href="/interactive-map" title="">Photos</a></li>
<li class="leaf"><a href="/blog" title="">Blog</a></li>
<li class="last leaf"><a href="/plan-your-trip">Plan Your Trip</a></li>
</ul>			  			</nav>
		</div>
	</div>
	
	<div class="navbar-right">
        <a class="logo navbar-btn pull-left" href="/" title="Home">
          <img src="/sites/all/themes/rapidcity/images/rapidcity_logo.png" alt="Home" />
        </a>
		<div class="quicklinks">
			<div class="signin">
				<a href="/signin">Sign In/Register</a>
			</div>
			<div class="booknow">
				<a href="#">Book Now</a>
			</div>
		</div>
							  <div class="region region-search">
    <section id="block-search-form" class="block block-search clearfix">

  <form class="form-search content-search" action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div>
      <h2 class="element-invisible">Search form</h2>
    <div class="input-group"><input title="Enter the terms you wish to search for." placeholder="Search" class="form-control form-text" type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" /><span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="icon glyphicon glyphicon-search" aria-hidden="true"></i></button></span></div><button class="element-invisible btn btn-primary form-submit" id="edit-submit" name="op" value="Search" type="submit">Search</button>
<input type="hidden" name="form_build_id" value="form-Kv6Ug-uMBEiXfo5M7kYGmgoZNFVQ9g4NgontGERHw_Y" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>
</section> <!-- /.block -->
  </div>
			</div>
	  
  </div>
</header>

<div class="masthead">
					  <div class="region region-header">
    <section id="block-views-9e165da51dfbcfbb98b3f513caeb67f1" class="block block-views clearfix">

  <div class="view view-homepage-slides view-id-homepage_slides view-display-id-homepage_jumbotron view-dom-id-e36bf75d9d6e70a88105e60344355b7b">
        
  
  
      <div class="view-content">
      <div id="views-bootstrap-carousel-1" class="views-bootstrap-carousel-plugin-style carousel slide" >
  
  <!-- Carousel items -->
  <div class="carousel-inner">
          <div class="item slide-wrap active">
          
  <div class="views-field views-field-field-photo">        <div class="field-content"><img typeof="foaf:Image" src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/default/files/styles/home_slide/public/slides/lincoln.jpg?itok=GAwbbKQy" width="1280" height="864" alt="" /></div>  </div>		<!-- <div class="views-field views-field-field-photo slide-photo">
			<div class="field-content">
							</div>
		</div>
		<div class="slide-copy">
			<div class="slide-title"></div>  
			<div class="slide-subtitle"> <div class="slide-readmore"></div></div>
		</div> -->
      </div>
      </div>

      <!-- Carousel navigation -->
    <a class="carousel-control left" href="#views-bootstrap-carousel-1" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#views-bootstrap-carousel-1" data-slide="next">&rsaquo;</a>
  </div>
    </div>
  
  
  
  
  
  
</div>
</section> <!-- /.block -->
  </div>
		</div>

<div class="callout">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="lato-bold">Monumental Trip Ideas</h1>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-xs-6 col-sm-3 bigthings">
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/eat_big.jpg" />
			</div>
			<div class="col-xs-6 col-sm-3 bigthings">
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/shop_big.jpg" />
			</div>
			<div class="col-xs-6 col-sm-3 bigthings">
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/stay_big.jpg" />
			</div>
			<div class="col-xs-6 col-sm-3 bigthings">
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/do_big.jpg" />
			</div>
		</div>
	</div>
</div>

<div class="home-explore-map">
	
	
<?php include 'homepage_map.php'; ?>
	
	
</div>

<div class="like-a-local">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<img class="img-responsive ranger-title" src="/sites/all/themes/rapidcity/images/do_big_things.jpg" alt="Do Big Things" />
				<h2 class="lato-bold">Like A Local</h2>
				<p>Residents and fans share their top ten things to do in Rapid City.</p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-xs-6 col-sm-3 local-block">
				<div class="local-info rcblue"><h3 class="open-bold">Person&#x27;s Name</h2><span>Professional Title</span></div>
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/profilepic1.jpg" />
			</div>
			<div class="col-xs-6 col-sm-3 local-block">
				<div class="local-info rcpurple"><h3 class="open-bold">Person&#x27;s Name</h2><span>Professional Title</span></div>
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/profilepic2.jpg" />
			</div>
			<div class="col-xs-6 col-sm-3 local-block">
				<div class="local-info rcgreen"><h3 class="open-bold">Person&#x27;s Name</h2><span>Professional Title</span></div>
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/profilepic3.jpg" />
			</div>
			<div class="col-xs-6 col-sm-3 local-block">
				<div class="local-info rcyellow"><h3 class="open-bold">Person&#x27;s Name</h2><span>Professional Title</span></div>
				<img class="img-responsive" src="/sites/all/themes/rapidcity/images/profilepic4.jpg" />
			</div>
		</div>
	</div>
</div>

<div class="download-guide container">
	<div class="row">
		<div class="col-sm-12">
			<a class="lato-bold" href="#">Download a Visitor&#x27;s Guide</a>
		</div>
	</div>
</div>

<div class="home-photos">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				
				<div class="jcarousel-wrapper">
	                <div class="jcarousel">
	                    <ul>
	                        <li><img src="/sites/default/files/slides/018.jpg" alt="Image 1"></li>
	                        <li><img src="/sites/default/files/slides/020.jpg" alt="Image 2"></li>
	                        <li><img src="/sites/default/files/slides/086.jpg" alt="Image 3"></li>
	                        <li><img src="/sites/default/files/slides/093.jpg" alt="Image 4"></li>
	                        <li><img src="/sites/default/files/slides/108.jpg" alt="Image 5"></li>
	                    </ul>
	                </div>

	                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	                <a href="#" class="jcarousel-control-next">&rsaquo;</a>

	                <p class="jcarousel-pagination"></p>
	            </div>
			</div>
		</div>
	</div>
</div>

	<footer class="footer">
	<div class="container">

		<div class="foot-social-icons">
				<a class="foot-social-icon foot-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_foot_facebook.png" /></a>
				<a class="foot-social-icon foot-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_foot_twitter.png" /></a>
				<a class="foot-social-icon foot-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_foot_pinterest.png" /></a>
				<a class="foot-social-icon foot-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_foot_youtube.png" /></a>
				<a class="foot-social-icon foot-instagram" href="#" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_foot_instagram.png" /></a>
		</div>

		<p>Rapid City Convention &amp; Visitors Bureau<br />
		<a href="http://www.VisitRapidCity.com">www.VisitRapidCity.com</a></p>

		<p>444 Mt. Rushmore Road N. &bull; Rapid City, SD 57701<br />
		1-800-487-3223 &bull; 605-718-8484 &bull; Fax: 605-348-9217</p>

		<p><a href="/things-to-do">Things To Do</a>  |  <a href="/stay">Stay</a>  |  <a href="/dine">Dine</a>  |  <a href="/events">Events</a>  |  <a href="/photos">Photos</a>  |  <a href="/blog">Blog</a>  |  <a href="/plan-your-trip">Plan Your Trip</a>  |  <a href="/about-rapid-city">About Rapid City</a><br />
		<a href="/privacy-policy">Privacy Policy</a>  |  <a href="/sitemap">Sitemap</a>  |  <a href="/sign-up">Sign up for email</a>  |  <a href="/meeting-planners">Meeting Planners</a>  |  <a href="/travel-professionals">Travel Professionals</a>  |  <a href="/sporting-events-planners">Sporting Events Planners</a><br />
		<a href="/media">Media</a>  |  <a href="/press-media-inquiries">Press &amp; Media Inquiries</a><br />
		<a href="http://www.RCGov.org" target="_blank">RCGov.org</a>  |  <a href="http://www.RapidCityChamber.com" target="_blank">RapidCityChamber.com</a></p>

		<div class="languages">					
			<a href="/languages/Español/" title="Español"><img src="/sites/all/themes/rapidcity/images/flag-spanish-small.png"></a>
			<a href="/languages/FrançaiseCanada/" title="Française Canada"><img src="/sites/all/themes/rapidcity/images/flag-french-canada-small.png"></a>
			<a href="/languages/Deutsch/" title="Deutsch"><img src="/sites/all/themes/rapidcity/images/flag-german-small.png"></a>
			<a href="/languages/Italiano/" title="Italiano"><img src="/sites/all/themes/rapidcity/images/flag-italian-small.png"></a>
			<a href="/languages/FrançaiseFrance/" title="Française France"><img src="/sites/all/themes/rapidcity/images/flag-french-france-small.png"></a>
			<a href="/languages/Chinese" title="漢語"><img src="/sites/all/themes/rapidcity/images/flag-chinese-small.png"></a>
			<a href="/languages/Japanese/" title="標準語"><img src="/sites/all/themes/rapidcity/images/flag-japanese-small.png"></a>
		</div>

		<div class="foot-logos">
			<a class="foot-logo" href="http://www.discoveramerica.com/usa/states/south-dakota.aspx" target="_blank" title="USA DiscoverAmerica.com"><img src="/sites/all/themes/rapidcity/images/usa_logo.png" /></a>
			<a class="foot-logo" href="http://www.tripadvisor.com/Tourism-g54774-Rapid_City_South_Dakota-Vacations.html" target="_blank" title="Trip Advisor"><img src="/sites/all/themes/rapidcity/images/tripadvisor.png" /></a>
		</div>
		
	</div>
</footer>
<div class="footer-fixed">
	<div class="foothill-left"></div>
	<div class="foothill-right"></div>
	<div class="footer-fixed-content">
		  <div class="region region-footer-fixed">
    <section id="block-block-3" class="block block-block clearfix">

  <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style footer-like">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
<!-- AddThis Button END -->
</section> <!-- /.block -->
<section id="block-block-4" class="block block-block clearfix">

  <div class="fixed-footer-link">
	<a href="#">Newsletter Signup</a></div>
<div class="fixed-footer-link">
	<a href="#">Visitor's Guide</a></div>

</section> <!-- /.block -->
  </div>
	</div>
</div>
  <script src="http://ec2-54-201-98-147.us-west-2.compute.amazonaws.com/sites/all/themes/bootstrap/js/bootstrap.js?n0j72j"></script>
</body>
</html>
