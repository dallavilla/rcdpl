<header id="blog-navbar" role="banner" class="<?php //print $navbar_classes; ?>">
  <div class="navbar-container container">
	  
	
	<div class="blog-navbar-left">
       <div class="logo-container">
       <a href="/blog" title="<?php print t('Home'); ?> " class="navbar-btn pull-left" ><img src="/sites/all/themes/rapidcity/images/rapidcity_logo_red.png" id="blog-logo"></a>
       </div>
		<div class="blog-searchbar">
			<?php
			$block = module_invoke('views', 'block_view', '-exp-blog_search-page_1');
			print render($block['content']);
			?>
		</div>
		<div class="archive-button"><a href="/blog/archives" class="archive-link">VIEW ARCHIVES</a>
		</div>
   </div>
		
 	<div class="blog-navbar-right">
		  <div class="head-social-icons"> 
		   		<a class="home-link" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>  "  target="_blank" ><?php print t('VISITRAPIDCITY.COM '); ?></a>
  				<a class="head-social-icon head-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_head_facebook.png" alt="Facebook" /></a>
  				<a class="head-social-icon head-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_head_twitter.png" alt="Twitter" /></a>
  				<a class="head-social-icon head-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_head_pinterest.png" alt="Pinterest" /></a>
  				<a class="head-social-icon head-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_head_youtube.png" alt="YouTube"/></a>
  				<a class="head-social-icon head-instagram" href="http://www.instagram.com/VisitRapidCity" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_head_instagram.png" alt="Instagram" /></a>
		  </div>
	</div>
  </div>
</header>

<div class="clear-all">
</div>
  <a href="#0" class="cd-top">Top</a>

<?php if (!empty($page['header'])): ?>
	<?php print render($page['header']); ?>
<?php endif; ?>


<div class="blog-main-container container">
  <div class="row">
<?php print render($page['content']); ?>
  </div>
</div>



<?php if ($page['footerblog']): ?>
<div class="footerblog">
<?php print render($page['footerblog']); ?>
</div>
 <!-- /.footer icons -->
<?php endif; ?>




<div class="footer-fixed">
	<div class="foothill-left"></div>
	<div class="foothill-right"></div>
	<div class="footer-fixed-content">

		<?php print render($page['footer_fixed']); ?>
	</div>
</div>
