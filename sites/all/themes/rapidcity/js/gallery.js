
jQuery(function($) {

$(document).ready(function() {
	$("a.thumb").bind("click", showImage);
});

function showImage() {
	$("a.thumb").unbind("click", showImage);
	
	var doc = $(document);
	var mask = $(document.createElement('div'))
		.addClass('mask')
		.addClass('mini-gallery-mask-bg')
		.width(doc.width())
		.height(doc.height());
	
	$('body').append(mask);
	
	mask.fadeTo("fast", .5);
	
	var img = $(document.createElement('img'))
		.bind('load', imageLoaded)
		.attr('src', $(this).attr('href'));
	
	return false;
}

function imageLoaded() {
	var c = $(document.createElement('div'));
	var doc = $(document);
	var mask = $('.mask');

	c.addClass('gal-container')
		.addClass('mini-gallery-container')
		.append(this);
	
	var close = $(document.createElement('a'))
		.attr('href', '#close')
		.text('close')
		.bind("click", closeClick);
	
	c.append(close);
	$('body').append(c);
	
	var left = (doc.width() - c.width()) / 2; 
	
	var topPadding = 30;
	var bottomPadding = 30;
	var height = c.height();
	var top = doc.scrollTop() + topPadding;
	var bodyHeight = doc.height();
	var requiredHeight = top + height + bottomPadding; 
	if(bodyHeight < requiredHeight) {
		mask.height(requiredHeight);
		mask.css({ position: "absolute" });
	}
	
	c.css({
		position: "absolute",
		left: Math.round(left) + "px",
		top: Math.round(top) + "px"
	});
}

var _fadeTimeout = 0;
function closeClick() {
	$(this).unbind('click');
	$(this).bind('click', function() { return false; })
	
	$('.mini-gallery-container').fadeOut();
	$('.mini-gallery-mask-bg').fadeOut("slow", resetGallery);
	_fadeTimeout = setTimeout(resetGallery, 3000);
	return false;
}

function resetGallery() {
	clearTimeout(_fadeTimeout);
	$('.mini-gallery-container').remove();
	$('.mini-gallery-mask-bg').remove();
	
	$("a.thumb").unbind("click");
	$("a.thumb").bind("click", showImage);
}
}); 