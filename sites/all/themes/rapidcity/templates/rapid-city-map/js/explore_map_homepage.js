

function convertFromLL(lat, lon){
	
	var width = $(".em_map_box").width();
	var height = $(".em_map_box").height();
	
	//top left 
	//44° 7.849', -103° 20.791'
	//44.1307203,-103.3469129
	  //44.13126,-103.3474256
	
//bottom right
//44° 2.300', -103° 6.290'
//44.0363632,-103.1007767
//44.0351561,-103.0996663
	//44.0433238,-103.1185894
	
	//console.log("w: " + width);
	//console.log(" h: " + height);
	
	
	// Size of the map
	//int width = 538;
	//int height = 811;
	// X and Y boundaries
	
	/* old values */
	//var westLong = -103.3469129;
	//var eastLong = -103.1052857;
	//var northLat = 44.1306903;
	//var southLat = 44.0375704; //44.2300;
	
	var westLong = -104.054560;
	var eastLong = -101.957358;
	var northLat = 44.367787;
	var southLat = 43.572394; //44.2300;

	//void drawPoint(float latitude, float longitude){

	 //fill(#000000);

	 var x = width * ((westLong-lon)/(westLong-eastLong));
	 var y = (height * ((northLat-lat)/(northLat-southLat)));

	 //console.log(x + ", " + y);
	 //ellipseMode(RADIUS);
	 //ellipse(x, y, 2, 2);    
		return [x, y];
	
	
}







/*<img class="img-responsive" src="/sites/all/themes/rapidcity/templates/rapid-city-map/images/homepage_map.jpg" alt="Rapid City" usemap="#explore_map" />
<map name="explore_map" id="explore_map">
  <area id="blue_region" shape="poly" coords="195,186,147,327,119,476,111,562,129,607,168,641,211,657,255,655,305,631,501,493,577,418,631,338,650,281,646,234,621,202,574,180,458,160,353,138,288,138,233,152" href="#" alt="blue region" />
  <area id="downtown_region" shape="poly" coords="607,314,571,391,555,437,553,471,567,490,588,493,620,480,683,417,731,349,740,309,732,291,656,286,632,292" href="#" alt="downtown" />
  <area id="brown_region" shape="poly" coords="643,134,617,174,600,222,607,252,637,277,687,297,769,313,836,317,954,304,1034,283,1044,270,1028,256,984,244,938,235,859,220,800,180,730,134,683,121" href="#" alt="brown region" />
  <area id="green_region" shape="poly" coords="597,486,595,564,606,614,639,637,699,646,793,639,910,614,989,577,1015,541,1006,498,931,415,837,305,817,302,739,344,690,378,634,419,613,448" href="#" alt="green region" />
</map>*/







$(document).ready(function() {
	
	
	$(window).load(function() {
		//set dimensions of maparea
		$(".em-absolute").css("height", $(".em-relative").height() + "px");
		$(".em-absolute").css("width", $(".em-relative").width() + "px");
		//$('img[usemap]').rwdImageMaps();
		
		populateEMHomepage();
		//scaleFont(".em_homepage_action");
		scaleFont(".em_map_box");
		
	});
	
	
	$(window).resize(function() {
		//set dimensions of maparea
		$(".em-absolute").css("height", $(".em-relative").height() + "px");
		$(".em-absolute").css("width", $(".em-relative").width() + "px");
		
		
	});
	
		$("#explore_map area").click(function(e) {
			e.preventDefault();
			
		});
	
	
	
	//ok, xml feed for homepage
	
	var checkMapIsLoaded = false;
	
	
      
      //$('.em_homepage_action a').bigtext({
          //minfontsize: 30
      //});
	
	
	
	/*if ()
	setInterval(function(){
		
		if($(".em-relative").width() != 0)
		{
			
		}
		
	}, 800);*/
	
	
	
	//alert("b");
	
	
	//$('img[usemap]').rwdImageMaps();
	
	$("#explore_map #blue_region").hover(function() {
	
	  //show
	  console.log("hov");
		//alert("blue");
		$(".blue_region").show();
	}, function() {
		
		//hide
		console.log("hovend");
		//$(".blue_region").hide();
	});
	
	
	
	
	
	
	
	
});
	
	
	function populateEMHomepage()
	{
		$.ajax({
		        type: "GET",
			url: "/sites/all/themes/rapidcity/templates/rapid-city-map/listings.xml",
			dataType: "xml",
			success: function(xml) {

				$(xml).find('listing').each(function(){
					
					var id = $(this).attr('id');
					var short_title = $(this).find('short_title').text();
					var title = $(this).find('title').text();
					var image = $(this).find('image').text();
					var description = $(this).find('description').text();
					var url = $(this).find('url').text();
					var longitude = $(this).find('long').text();
					var latitude = $(this).find('lat').text();
					var region = $(this).find('region').text();
					
					//plot
					var coords = convertFromLL(latitude, longitude);
					//console.log("coords[0]:" + coords[0] + " boxw " + $(".em_map_box").width());
					var percentx = coords[0] / $(".em_map_box").width() * 100;
					var percenty = coords[1] / $(".em_map_box").height() * 100;
					
					
					var content_dart = "<div class=\"em_dart\"" + " style=\"left: " + percentx + "%;" + " top: " + percenty + "%;\">" + "<img src=\"" + "/sites/all/themes/rapidcity/templates/rapid-city-map/images/em_plot.png" + "\">"+ title + "</div>";
					
					
					$(".em_map_box").append(content_dart);
					
					
					//set hover events for darts
					$(".em_dart").hover(function(){

						//reveal region background
						$(".regions ." + region).fadeIn();
						//$(this).fadeOut();
					},
					function(){
						//$(this).fadeIn();
						$(".regions ." + region).fadeOut();
					});
					
					//console.log(xml);
				});
				

				
				

			}
		});
	}
	
	
	//run on load
	function scaleFont(element)
	{
		var parent_element = ".em_map_box";
		var ratio = $(element).css("font-size").replace("px", "") / $(parent_element).width();
		
		$(window).resize(function(){
			var calcFont = ratio * $(parent_element).width();
			$(element).css("font-size", calcFont + "px");
			
		});
		
	}
	
	function setRatioFont(element, parent_element)
	{
		return $(element).css("font-size").replace("px", "") / $(parent_element).width();
	}
	
	
