<div class="dropdown-bg"></div>

<!-- Start Booking Widget -->
<div class="dropdown-book">
	<div class="container">
		<div class="row">
			<script src="http://widgets.bookdirect.net/widgets/search_widget.js.php?id=758" type="text/javascript"></script>
		</div>
	</div>
</div>
<!-- End Booking Widget -->

<header id="navbar" role="banner" class="<?php //print $navbar_classes; ?>">
  <div class="navbar-container container">
	  
	  <div class="navbar-left">
	    <div class="navbar-header">
	      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>

		<div class="navbar-collapse collapse">
			<nav role="navigation">
  			  <?php //if (!empty($secondary_nav)): ?>
  			    <?php //print render($secondary_nav); ?>
  			  <?php //endif; ?>
			  <ul class="menu nav navbar-nav secondary">
				  <li class="first collapsed"><a href="/meeting-planners" title="">Meeting Planners</a></li>
				  <li class="collapsed"><a href="/press-media-inquiries">Press &amp; Media Inquiries</a></li>
				  <li class="collapsed"><a href="/sporting-events-planners" title="">Sporting Events Planners</a></li>
				  <li class="last collapsed"><a href="/travel-professionals" title="">Travel Professionals</a></li>
			  </ul>
			  <div class="head-social-icons">
  				<a class="head-social-icon head-facebook" href="https://www.facebook.com/RapidCitySouthDakota" target="_blank" title="Facebook"><img src="/sites/all/themes/rapidcity/images/icon_head_facebook.png" alt="Facebook" /></a>
  				<a class="head-social-icon head-twitter" href="https://twitter.com/VisitRapidCity" target="_blank" title="Twitter"><img src="/sites/all/themes/rapidcity/images/icon_head_twitter.png" alt="Twitter" /></a>
  				<a class="head-social-icon head-pinterest" href="http://www.pinterest.com/visitrapidcity/" target="_blank" title="Pinterest"><img src="/sites/all/themes/rapidcity/images/icon_head_pinterest.png" alt="Pinterest" /></a>
  				<a class="head-social-icon head-youtube" href="http://www.youtube.com/VisitRapidCity" target="_blank" title="YouTube"><img src="/sites/all/themes/rapidcity/images/icon_head_youtube.png" alt="YouTube"/></a>
  				<a class="head-social-icon head-instagram" href="http://www.instagram.com/VisitRapidCity" target="_blank" title="Instagram"><img src="/sites/all/themes/rapidcity/images/icon_head_instagram.png" alt="Instagram" /></a>
			  </div>
			  <?php if (!empty($primary_nav)): ?>
			    <?php print render($primary_nav); ?>
			  <?php endif; ?>
			</nav>
		</div>
	</div>
	
	<div class="navbar-right">
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print t('Home'); ?></a>
		<div class="quicklinks">
			<div class="signin">
				<!-- <a href="/signin">Sign In/Register</a> -->
			</div>
			<div class="booknow">
				<a href="#">Book Now</a>
			</div>
		</div>
		<div class="nearme mobile-only">
			<a href="/near-me">What's Nearby?</a>
		</div>
		<?php if (!empty($page['navigation'])): ?>
			<?php print render($page['navigation']); ?>
		<?php endif; ?>
		<?php if (!empty($page['search'])): ?>
			<?php print render($page['search']); ?>
		<?php endif; ?>
	</div>
	  
  </div>
</header>

<div class="masthead">
	<div class="container">
		<div class="row">
		    <a id="main-content"></a>
			<div class="masthead-wrapper">
				<div class="masthead-image">
					<?php print render($page['masthead_image']); ?>
				</div>
				<div class="masthead-strip">
					<div class="masthead-info">
						<?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
				        <?php print render($title_prefix); ?>
				        <?php if (!empty($title)): ?>
				          <h1 class="page-header"><?php print $title; ?></h1>
				        <?php endif; ?>
				        <?php print render($title_suffix); ?>
					</div>
					<div class="masthead-form">
						<?php print render($page['masthead_form']); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-container container">
	
  <div class="row">
  	<div class="col-md-6">
    <section>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>
    </div>
    <div class="col-md-6 rushmore-side-col">
    	<figure class="rushmore-link-figure">
    		<a href="/things-to-do">
    			<img src="/sites/default/files/images/rushmore-main-cta.jpg" alt="" class="img-responsive">
    					<figcaption>
    						<h2>MOUNT RUSHMORE <br />NATIONAL MEMORIAL</h2>
    						Explore Nearby Attractions
    					</figcaption>
    		</a>
    	</figure>
    	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		      <img src="/sites/default/files/images/rushmore-fall.jpg" class="img-responsive">
		      <div class="carousel-caption">
		        
		      </div>
		    </div>
		    <div class="item">
		      <img src="/sites/default/files/images/rushmore-aerial1.jpg" class="img-responsive">
		      <div class="carousel-caption">
		        
		      </div>
		    </div>
		    <div class="item">
		      <img src="/sites/default/files/images/rushmore-aerial2.jpg" class="img-responsive">
		      <div class="carousel-caption">
		        
		      </div>
		    </div>
		    <div class="item">
		      <img src="/sites/default/files/images/rushmore-trail.jpg" class="img-responsive">
		      <div class="carousel-caption">
		        
		      </div>
		    </div>
		    <div class="item">
		      <img src="/sites/default/files/images/rushmore-goat.jpg" class="img-responsive">
		      <div class="carousel-caption">
		        
		      </div>
		    </div>
		    
		  </div>
		</div>

		<h3>Rapid City is the gateway to Mount Rushmore National Memorial and is the perfect place to start your journey.</h3>
		<div class="row rushmore-links">
			<div class="col-xs-4">
				<a href="/dining"><img src="/sites/default/files/images/rushmore-eat.jpg" class="img-responsive" alt="Eat"/><span>EAT</span></a>
			</div>
			<div class="col-xs-4">
				<a href="/where-to-stay"><img src="/sites/default/files/images/rushmore-stay.jpg" class="img-responsive" alt="Stay"/><span>STAY</span></a>
			</div>
			<div class="col-xs-4">
				<a href="/things-to-do"><img src="/sites/default/files/images/rushmore-do.jpg" class="img-responsive" alt="Do"/><span>DO</span></a>
			</div>
		</div>

		<figure class="rushmore-about-figure">
    		<a href="/mount-rushmore">
    			<img src="/sites/default/files/images/rushmore-presidential-trail.jpg" alt="ABOUT MOUNT RUSHMORE" class="img-responsive">
    					<figcaption>
    						<h2>ABOUT MOUNT RUSHMORE</h2>
    					</figcaption>
    		</a>
    	</figure>

    	<figure class="rushmore-about-figure">
    		<a href="/things-to-do/mount-rushmore/history-mount-rushmore">
    			<img src="/sites/default/files/images/rushmore-lighting-ceremony.jpg" alt="HISTORY OF MOUNT RUSHMORE" class="img-responsive">
    					<figcaption>
    						<h2>HISTORY OF MOUNT RUSHMORE</h2>
    					</figcaption>
    		</a>
    	</figure>

    	<figure class="rushmore-about-figure">
    		<a href="/things-to-do/mount-rushmore/faces-mount-rushmore">
    			<img src="/sites/default/files/images/rushmore-visitor-center.jpg" alt="THE FACES OF MOUNT RUSHMORE" class="img-responsive">
    					<figcaption>
    						<h2>THE FACES OF MOUNT RUSHMORE</h2>
    					</figcaption>
    		</a>
    	</figure>

    </div>




  </div>
</div>
<!-- <div class="container">
<div class="row">
	<div class="col-md-12">
		<div class="explore-map">
			<?php //include 'rapid-city-map/explore_map.php'; ?>
		</div> 
		<?php //if (!empty($page['bottom_first'])): ?>
			<?php //print render($page['bottom_first']); ?>
		<?php //endif; ?>
	</div>
</div>
</div> -->
<?php if (!empty($page['footer'])): ?>
	<?php print render($page['footer']); ?>
<?php endif; ?>

<div class="footer-fixed">
	<div class="foothill-left"></div>
	<div class="foothill-right"></div>
	<div class="footer-fixed-content">
		<?php print render($page['footer_fixed']); ?>
	</div>
</div>
