﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// config.extraPlugins = 'doksoft_bootstrap_include, doksoft_bootstrap_advanced_blocks, doksoft_bootstrap_block_conf, doksoft_bootstrap_templates, doksoft_bootstrap_table, doksoft_bootstrap_button';
};
