<section id="<?php print $block_html_id; ?>" class="container <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php //print $content ?>
  
  <?php
  		$child_block = block_load('menu_block', '4');
  		$child_block_render = render(_block_get_renderable_array(_block_render_blocks(array($child_block))));
  		$sibling_block = block_load('menu_block', '5');
  		$sibling_block_render = render(_block_get_renderable_array(_block_render_blocks(array($sibling_block))));
  		if (!empty($child_block_render)) {
  			print $child_block_render;
  		}
  		if (empty($child_block_render)) {
  			print $sibling_block_render;
  		}
  ?>

</section> <!-- /.block -->
