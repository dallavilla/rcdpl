<section id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <div class="jcarousel-wrapper">
      <div class="jcarousel">
		  <?php if ($rows): ?>
				  <?php print $rows; ?>
		  <?php elseif ($empty): ?>
				  <?php print $empty; ?>
		  <?php endif; ?>
      </div>

      <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
      <a href="#" class="jcarousel-control-next">&rsaquo;</a>

      <!-- <p class="jcarousel-pagination"></p> -->
  </div>

</section> <!-- /.block -->
