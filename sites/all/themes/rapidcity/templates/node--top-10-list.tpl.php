<?php
$field1=array();
$field2=array();
$field3=array();
$field4=array();
$field5=array();
$full_count = 0;
$item_count = 0;
if(!empty($content['field_top_10_item']['#items']))
	foreach($content['field_top_10_item']['#items'] as $item){
		// because field items are numbered consecutively, the correct index needs to be retrieved here
		$index=$item['value'];
		$field1[]=$content['field_top_10_item'][$full_count]['entity']['field_collection_item'][$index]['field_title'][0]['#markup'];
		$field2[]=$content['field_top_10_item'][$full_count]['entity']['field_collection_item'][$index]['field_top_10_body'][0]['#markup'];
		$field3[]=$content['field_top_10_item'][$full_count]['entity']['field_collection_item'][$index]['field_top_10_link'][0]['#element']['url'];
		$field4[]=$content['field_top_10_item'][$full_count]['entity']['field_collection_item'][$index]['field_top_10_image'][0]['#item']['uri'];
		$field5[]=$content['field_top_10_item'][$full_count]['entity']['field_collection_item'][$index]['field_top_10_link'][0]['#element']['title'];
		$full_count++;
	}
?>

<!-- <h1><?php print $title; ?></h1> -->

<div class="ten-slideshow">
	<div><?php print render($content['body']); ?></div>
	<div>
			<?php while ($item_count < $full_count){ ?>
            <div class="ten-single-item">

            	<h2><?php print $item_count + 1; ?> - <?php print $field1[$item_count]; ?></h2>
        		<?php if (!empty($field4[$item_count])) : ?>
				<img src="<?php print image_style_url('top_ten_image', $field4[$item_count]) ?>" class="img-responsive"/>
				<?php endif; ?>
                <span class="ten-copy"><?php print $field2[$item_count]; ?></span>
                <?php if (!empty($field3[$item_count])) : ?>
                <span class="ten-link"><a href="<?php print $field3[$item_count]; ?>"><?php print $field5[$item_count]; ?></a></span>
                <?php endif; ?>
   			</div>
			<?php $item_count++; ?>
			<?php } ?>
    </div>
	
	<div class="ten-slides-nav"></div>

</div>


