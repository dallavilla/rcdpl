<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<?php if ($teaser): ?>
<!-- teaser template HTML -->

	<div class="listing-teaser row">

		<div class="listing-teaser-content col-sm-12">


			<?php print render($title_prefix); ?>
		    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
			<?php print render($title_suffix); ?>

			<?php if($node->field_photo  && !empty($node->field_photo['und'][0]['uri'])): ?>
				<div class="col-md-3">
						<div class="listing-image">
							<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
						</div>
				</div>
				<div class="col-md-9">
			<?php else: ?>
				<div class="col-md-12">
			<?php endif; ?>

			<h3>
				<?php
					$shorthand = array('label'=>'hidden', 'settings' => array('format_type' => 'shorthand'));
					$justtime = array('label'=>'hidden', 'settings' => array('format_type' => 'just_time'));
					$justdate = array('label'=>'hidden', 'settings' => array('format_type' => 'just_date'));
					$startdate = render(field_view_field('node', $node, 'idss_vatt_event_start_date', $shorthand));
					$enddate = render(field_view_field('node', $node, 'idss_vatt_event_end_date', $shorthand));




					if(substr($startdate, 108, 5) == '23:59') {
						//print("ALL DAY");
						print render(field_view_field('node', $node, 'idss_vatt_event_start_date', $justdate ));
					} else {
						print $startdate;
					}

					if(($node->idss_vatt_event_start_date['und'][0]['value'] == $node->idss_vatt_event_end_date['und'][0]['value'])){
						//print("SAME");
					} elseif($node->idss_vatt_event_end_date['und'][0]['value'] == '') {
						//print("NO END DATE");
					} elseif(substr($enddate, 108, 5) == '23:59') {
							if (substr($startdate, 0, 107) != substr($enddate, 0, 107)) // not same date? show end date, else nothing
							{
								print(" - ");
								print render(field_view_field('node', $node, 'idss_vatt_event_end_date', $justdate ));
							}
					} elseif(substr($startdate, 0, 107) == substr($enddate, 0, 107)) {
						print(" - ");
						print render(field_view_field('node', $node, 'idss_vatt_event_end_date', $justtime));
					} else {
						print(" - ");
						print $enddate;
					}
					//print "<span class=\"hidden\">" . $node->idss_vatt_event_start_date['und'][0]['value'] . "</span>";
					//print "<span class=\"hidden\">" . $node->idss_vatt_event_end_date['und'][0]['value'] . "</span>";
				?>
			</h3>

			<?php if($node->body): ?>
				<?php if($node->body['und'][0]['value']): ?>
				  <?php //print $node->body['und'][0]['value']; ?>
				  <?php print render(field_view_value('node', $node, 'body', $body[0],'teaser'))?> <a class="readmore" href="<?php print url($node_url, array('absolute' => TRUE)); ?>">Read More</a>
				<?php endif; ?>
			<?php endif; ?>


			<div class="listing-teaser-extras">
				<?php if($node->idss_vatt_event_website['und'][0]['value']): ?>
					<?php
						$urlStr = $node->idss_vatt_event_website['und'][0]['value'];
						$urlparsed = parse_url($urlStr);
						if (empty($urlparsed['scheme'])) {
						    $urlStr = 'http://' . ltrim($urlStr, '/');
						}
					?>
					<div class="listing-teaser-links">
						<a href="<?php print($urlStr); ?>" target="_blank">Visit Website</a>
					</div>
				<?php endif; ?>

				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php print url($node_url, array('absolute' => TRUE)); ?>">
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_google_plusone_share"></a>
				<a class="addthis_button_compact"></a>
				</div>
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
				<!-- AddThis Button END -->
				</div>
			</div>

		</div>
	</div>
	<div class="print-listing">
		<h2><?php print $title; ?></h2>
		<strong>Date:</strong> <?php print $startdate ?> - <?php print $enddate ?><br />
		<strong>Phone:</strong> <?php print($node->idss_vatt_event_contact_phone['und'][0]['value']); ?><br />
		<strong>Location:</strong> <?php print($node->idss_vatt_event_facility_locati['und'][0]['value']); ?><br />
		<?php print $node->body['und'][0]['value']; ?>
	</div>

<?php else: ?>
<!-- regular node view template HTML -->

	<div class="listing-page">

		<?php print render($title_prefix); ?>
	    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
		<?php print render($title_suffix); ?>

		<h3>
			<?php
				$shorthand = array('label'=>'hidden', 'settings' => array('format_type' => 'shorthand'));
				$justtime = array('label'=>'hidden', 'settings' => array('format_type' => 'just_time'));
				$justdate = array('label'=>'hidden', 'settings' => array('format_type' => 'just_date'));
				$startdate = render(field_view_field('node', $node, 'idss_vatt_event_start_date', $shorthand));
				$enddate = render(field_view_field('node', $node, 'idss_vatt_event_end_date', $shorthand));

				if(substr($startdate, 108, 5) == '23:59') {
					//print("ALL DAY");
					print render(field_view_field('node', $node, 'idss_vatt_event_start_date', $justdate));
					//print "<span class=\"hidden\">" . $node->idss_vatt_event_start_date['und'][0]['value'] . "</span>";
					//print "<span class=\"hidden\">" . date('d-m-Y', substr($startdate, 97, 10))    . "</span>";

				} else {

					if (substr($startdate, 108, 5) == '00:00')
					{
						print render(field_view_field('node', $node, 'idss_vatt_event_start_date', $justdate ));
					}
					else
					{
						print $startdate;
					}
				}
				//print("<span class=\"hidden\">" . $node->idss_vatt_event_start_date['und'][0]['value'] . "</span>");
				//print("<span class=\"hidden\">" . $node->idss_vatt_event_end_date['und'][0]['value'] . "</span>");

				if(($node->idss_vatt_event_start_date['und'][0]['value'] == $node->idss_vatt_event_end_date['und'][0]['value'])){
					//print("<span class=\"hidden\">SAME</span>");
				} elseif($node->idss_vatt_event_end_date['und'][0]['value'] == '') {
					//print("<span class=\"hidden\">NO END DATE</span>");
				} elseif(substr($enddate, 108, 5) == '23:59') { // end time 11:59
					if (substr($startdate, 0, 107) != substr($enddate, 0, 107)) // not same date? show end date, else nothing
					{
						print(" - ");
						print render(field_view_field('node', $node, 'idss_vatt_event_end_date', $justdate ));
					}
				} elseif(substr($startdate, 0, 107) == substr($enddate, 0, 107)) {
					print(" - ");
					print render(field_view_field('node', $node, 'idss_vatt_event_end_date', $justtime));
				} else {
					print(" - ");
					print $enddate;
				}
			?>
		</h3>

		<?php if($node->field_photo  && !empty($node->field_photo['und'][0]['uri'])): ?>
			<div class="listing-image">
				<img class="img-responsive" src="<?php print file_create_url($node->field_photo['und'][0]['uri']); ?>" />
			</div>
		<?php endif; ?>

		<?php if($node->idss_vatt_event_website['und'][0]['value']): ?>
			<div class="field-section">
				<?php
					$urlStr = $node->idss_vatt_event_website['und'][0]['value'];
					$urlparsed = parse_url($urlStr);
					if (empty($urlparsed['scheme'])) {
					    $urlStr = 'http://' . ltrim($urlStr, '/');
					}
				?>
			  <strong>Website:</strong> <a href="<?php print($urlStr); ?>" target="_blank"><?php print($node->idss_vatt_event_website['und'][0]['value']); ?></a>
			</div>
		<?php endif; ?>

		<?php if($node->idss_vatt_event_facility_locati && !empty($node->idss_vatt_event_facility_locati['und'][0]['value'])): ?>
			<div class="field-section">
			  <strong>Location:</strong> <?php print($node->idss_vatt_event_facility_locati['und'][0]['value']); ?>
			</div>
		<?php endif; ?>


		<?php if($node->body['und'][0]['value']): ?>
			<div class="desc-section">
				<?php print($node->body['und'][0]['value']); ?>
			</div>
		<?php endif; ?>

		<div class="dynamic-fields">
			<ul>
				<?php if($node->idss_vatt_ticketed_event && ($node->idss_vatt_ticketed_event['und'][0]['value'] == 1)): ?>
					<li>Ticketed Event</li>
				<?php endif; ?>
				<?php if($node->idss_vatt_ticket_sales_begin && !empty($node->idss_vatt_ticket_sales_begin['und'][0]['value'])): ?>
					<li>Ticket Sales Begin: <?php print($node->idss_vatt_ticket_sales_begin['und'][0]['value']); ?></li>
				<?php endif; ?>
				<?php if($node->idss_vatt_ticket_information && !empty($node->idss_vatt_ticket_information['und'][0]['value'])): ?>
					<li>Ticket Information: <?php print($node->idss_vatt_ticket_information['und'][0]['value']); ?></li>
				<?php endif; ?>
			</ul>
		</div>


		<?php if($node->idss_vatt_event_contact_name || $node->idss_vatt_event_contact_phone || $node->idss_vatt_event_contact_email || $node->idss_vatt_event_website): ?>
			<div class="field-section">
				<strong>Event Contact:</strong>
				<?php if($node->idss_vatt_event_contact_name && !empty($node->idss_vatt_event_contact_name['und'][0]['value'])): ?>
					<br /> <?php print($node->idss_vatt_event_contact_name['und'][0]['value']); ?>
				<?php endif; ?>

				<?php if($node->idss_vatt_event_contact_phone && !empty($node->idss_vatt_event_contact_phone['und'][0]['value'])): ?>
					<br /><?php print($node->idss_vatt_event_contact_phone['und'][0]['value']); ?>
				<?php endif; ?>

				<?php if($node->idss_vatt_event_contact_email  && !empty($node->idss_vatt_event_contact_email['und'][0]['value'])): ?>
					<br /><?php print($node->idss_vatt_event_contact_email['und'][0]['value']); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

	</div>


<?php endif; ?>

</div>
