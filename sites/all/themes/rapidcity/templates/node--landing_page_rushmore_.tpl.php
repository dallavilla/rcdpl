<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">

<?php
$slider_field1=array();
$slider_field2=array();
$slider_field3=array();
$slider_field4=array();
$slider_field5=array();
$full_count = 0;
$item_count = 0;
if(!empty($content['field_rushmore_slider']['#items']))
	foreach($content['field_rushmore_slider']['#items'] as $item){
		// because field items are numbered consecutively, the correct index needs to be retrieved here
		$index=$item['value'];
		$slider_field1[]=$content['field_rushmore_slider'][$full_count]['entity']['field_collection_item'][$index]['field_rushmore_slider_image'][0]['#item']['uri'];
		$full_count++;
	}
?>

<div class="jumbotron-container">
<div id="carousel-example-generic" class="carousel slide container" data-ride="carousel">
		
		

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		  	<?php
		  	$indicators = "";



		  	while ($item_count < $full_count){ ?>
			    <div class="item <?php if ($item_count == 1 ) print 'active'; ?>">
			    	<?php if (!empty($slider_field1[$item_count])) : ?>
						<img src="<?php print image_style_url('top_ten_image', $slider_field1[$item_count]) ?>" class="img-responsive"/>
					<?php endif; ?>
			      <div class="carousel-caption">
			      </div>
			    </div>
			<?php $indicators .= "<li data-target=\"#carousel-example-generic\" data-slide-to=\"" . $item_count . "\" class=\"\"></li>"; ?>

			<?php $item_count++; ?>
			<?php } ?>

		  </div>
		<!-- Indicators -->
		  <ol class="carousel-indicators">
				<?php print($indicators); ?>
		  </ol>
		</div></div>



	<div class="row full-slab rushmore-intro-copy">
		<div class="col-md-12 container">

			<?php print render($content['body']); ?>
		</div>
	</div>
<?php if ($node->nid == 2027): ?> 

	<div class="row rushmore-map-container">
		<div class="col-md-9">
			<iframe src="https://www.google.com/maps/d/u/0/embed?mid=zSJ08GnXk9js.kMVEnhEaLCfQ" width="100%" height="480"></iframe>
		</div>
		<div class="col-md-3 rushmore-map-copy">
			<p>Five national <a href="things-to-do/parks-monuments">parks and monuments</a> are within an hour’s drive of Rapid City, along with popular attractions and spectacular scenery, making it the ideal hub for your Black Hills vacation experience.</p>
		</div>
	</div>
<?php endif; ?> 

<?php
$callouts_field1=array();
$callouts_field2=array();
$callouts_field3=array();
$callouts_field4=array();
$callouts_field5=array();
$full_count = 0;
$item_count = 0;
if(!empty($content['field_rushmore_callouts']['#items']))
	foreach($content['field_rushmore_callouts']['#items'] as $item){
		// because field items are numbered consecutively, the correct index needs to be retrieved here
		$index=$item['value'];
		$callouts_field1[]=$content['field_rushmore_callouts'][$full_count]['entity']['field_collection_item'][$index]['field_rushmore_callouts_title'][0]['#markup'];
		$callouts_field2[]=$content['field_rushmore_callouts'][$full_count]['entity']['field_collection_item'][$index]['field_rushmore_callouts_content'][0]['#markup'];
		$callouts_field3[]=$content['field_rushmore_callouts'][$full_count]['entity']['field_collection_item'][$index]['field_rushmore_callouts_url'][0]['#element']['url'];
		$callouts_field4[]=$content['field_rushmore_callouts'][$full_count]['entity']['field_collection_item'][$index]['field_rushmore_callouts_image'][0]['#item']['uri'];
		$callouts_field5[]=$content['field_rushmore_callouts'][$full_count]['entity']['field_collection_item'][$index]['field_rushmore_callouts_url'][0]['#element']['title'];
		$full_count++;
	}
?>



<div class="rushmore-callouts">
			<?php while ($item_count < $full_count){ ?>
            <figure class="rushmore-items">
				<?php if (!empty($callouts_field4[$item_count])) : ?>
					<img src="<?php print image_style_url('top_ten_image', $callouts_field4[$item_count]) ?>" class="img-responsive"/>
				<?php endif; ?>
				<figcaption>
					<div>
		            	<h2><?php print $callouts_field1[$item_count]; ?></h2>
		                <span class="ten-copy"><?php print $callouts_field2[$item_count]; ?></span>
		                <?php if (!empty($callouts_field3[$item_count])) : ?>
		                <span class="ten-link"><a href="<?php print $callouts_field3[$item_count]; ?>"><?php print $callouts_field5[$item_count]; ?></a></span>
		                <?php endif; ?>
	            	</div>
            	</figcaption>
   			</figure>
			<?php $item_count++; ?>
			<?php } ?>

</div>
<div class="rushmore-stay slab-tan">
	<div class="rushmore-stay-h1">
		<h1 class="container">Where to Stay</h1>
	</div>
	<div class="container">
	<div class="region region-bottom-first">
    <section id="block-block-17" class="block block-block contextual-links-region clearfix">

<div class="callout">

		<div class="row">
			<div class="col-sm-12">
				<p>Make your Black Hills vacation an experience in total comfort. Rapid City hotels offer a range of welcoming environments, from  contemporary design and amenities to historic charm. From downtown convenience to scenic views, with both familiar brands and unique options to choose from, you’ll find just the right Rapid City accommodations for your stay.</p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=21" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-bed-breakfast.jpg" alt="Bed and Breakfast" />
						<figcaption>
							Bed &amp;<br /> Breakfast
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=198" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-cabins.jpg" alt="Cabins" />
						<figcaption>
							Cabins
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=225" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-camp.jpg" alt="Campgrounds" />
						<figcaption>
							Campgrounds
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=337" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-hotel.jpg" alt="Hotels, Motels, Resorts" />
						<figcaption>
							Hotels,<br /> Motels, Resorts
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-5 where-to-stay">
				<a href="http://visitrapidcity.bookdirect.net/?lodgingID=311" target="_blank">
					<figure>
						<img class="img-responsive" src="/sites/default/files/images/stay-homes.jpg" alt="Vacation Homes" />
						<figcaption>
							Vacation Homes
						</figcaption>
					</figure>
				</a>
			</div>
		</div>

</div>

<!-- 
  <div class="view view-listing-categories view-id-listing_categories view-display-id-attractions_categories view-dom-id-9e413892ada167db4eb98465b10d8795">
	<div class="view-content">
		<div class="views-view-grid cols-5 top-buffer">
			<div class="row-1 row-first">
				<div class="col-1 col-first cat-row">
					<div class="views-field views-field-name">
						<span class="field-content"><a href="/things-to-do/mount-rushmore/about-mount-rushmore">Hotels, Motels, Resorts</a></span></div>
					<div class="views-field views-field-field-cat-image">
						<div class="field-content">
							<a href="/things-to-do/mount-rushmore/about-mount-rushmore"><img alt="About Mount Rushmore" height="540" src="/sites/default/files/images/stay-hotel.jpg" typeof="foaf:Image" width="540"></a></div>
					</div>
				</div>
				<div class="col-2 cat-row">
					<div class="views-field views-field-name">
						<span class="field-content"><a href="/things-to-do/mount-rushmore/history-mount-rushmore">History of<br>
						Mount Rushmore</a></span></div>
					<div class="views-field views-field-field-cat-image">
						<div class="field-content">
							<a href="/things-to-do/mount-rushmore/history-mount-rushmore"><img alt="History of Mount Rushmore" height="540" src="/sites/default/files/styles/category_square/public/SquareListings_MountRushmore_History.jpg" typeof="foaf:Image" width="540"></a></div>
					</div>
				</div>
				<div class="col-3 cat-row">
					<div class="views-field views-field-name">
						<span class="field-content"><a href="/things-to-do/mount-rushmore/facts-figures">Mount Rushmore Facts and Figures</a></span></div>
					<div class="views-field views-field-field-cat-image">
						<div class="field-content">
							<a href="/things-to-do/mount-rushmore/facts-figures"><img alt="Facts and Figures" height="540" src="/sites/default/files/styles/category_square/public/SquareListings_ParksMonumentsMemorials.jpg" typeof="foaf:Image" width="540"></a></div>
					</div>
				</div>
				<div class="col-4 cat-row">
					<div class="views-field views-field-name">
						<span class="field-content"><a href="/things-to-do/mount-rushmore/chuckwagons-western-entertainment">The Faces of Mount Rushmore</a></span></div>
					<div class="views-field views-field-field-cat-image">
						<div class="field-content">
							<a href="/things-to-do/mount-rushmore/faces-mount-rushmore"><img alt="Chuckwagons &amp; Western Entertainment" height="540" src="/sites/default/files/styles/category_square/public/SquareListings_MountRushmore_Faces.jpg" typeof="foaf:Image" width="540"></a></div>
					</div>
				</div>
				<div class="col-5 col-last cat-row">
					<div class="views-field views-field-name">
						<span class="field-content"><a href="things-to-do/attractions/parks-monuments-memorials">Other Parks and Monuments</a></span></div>
					<div class="views-field views-field-field-cat-image">
						<div class="field-content">
							<a href="/things-to-do/attractions/parks-monuments-memorials"><img alt="Other Parks and Monuments" height="540" src="/sites/default/files/styles/category_square/public/SquareListings_MountRushmore_OtherParks.jpg" typeof="foaf:Image" width="540"></a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->
</div>


<div class="full-slab">
	<div class="tag-your-adventures container">
		<a href="http://instagram.com/visitrapidcity" target="_blank"><img class="img-responsive" src="/sites/default/files/images/tag_your_adventures.png" /></a></div>
</div>

</section> <!-- /.block -->
  </div>
</div>


  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>

    </footer>
  <?php endif; ?>



</article> <!-- /.node -->