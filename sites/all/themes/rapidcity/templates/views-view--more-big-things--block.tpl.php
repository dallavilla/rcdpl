
<div class="full-slab slab-purple <?php print $classes; ?>">
	
	<div class="more-big-things container">
	  
	<h1><img src="/sites/default/files/images/more_big_things.png" class="img-responsive" /></h1>
	<h2 class="text-purple">Explore More Attractions and Dining</h2>

	  <?php if ($attachment_before): ?>
	    <div class="attachment attachment-before">
	      <?php print $attachment_before; ?>
	    </div>
	  <?php endif; ?>

	  <?php if ($rows): ?>
	    <div class="view-content">
	      <?php //print $rows; ?>
	    </div>
	  <?php endif; ?>

	  <?php if ($attachment_after): ?>
	    <div class="attachment attachment-after">
	      <?php print $attachment_after; ?>
	    </div>
	  <?php endif; ?>

	  <?php if ($more): ?>
	    <?php print $more; ?>
	  <?php endif; ?>
	  
	  <div class="view-more-things">
		  <div class="view-more-btn"><a href="/things-to-do"><img src="/sites/default/files/images/view_more_green.png" /></a></div>
		  <div class="view-more-btn"><a href="/dining"><img src="/sites/default/files/images/view_more_green.png" /></a></div>
	  </div>
  
	</div>

</div><?php /* class view */ ?>