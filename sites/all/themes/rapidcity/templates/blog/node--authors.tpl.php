
<?php if (!empty($page['header'])): ?>
	<?php print render($page['header']); ?>
<?php endif; ?>

<div class="clear-all">
</div>
<div class="container col-md-12">	

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>


  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && $title): ?>
  
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  </header>

  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
   
  ?>
 
  <div class="col-md-3">
  <?php print render($content['field_author_image']); ?>
  </div>
  
  <div class="col-md-9">
    <?php print render($content['field_author_name']); ?>
  <?php print render($content['field_author_title']); ?>
  <?php print render($content['body']); ?>
  

  </div>
 <?php print render($block['content']); ?>



</article> <!-- /.node -->


</div>

