<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>


  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && $title): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted): ?>
      <span class="submitted">
        <?php print $user_picture; ?>
        <?php print $submitted; ?>
      </span>
    <?php endif; ?>
  </header>

  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    print render($content);
  ?>
	
	<div style="clear:both;"></div>
	
	<?php
		// Recent content feed
		if (drupal_get_path_alias(current_path()) == 'events') {
			print views_embed_view('recent_listings','upcoming_events');
		} else if (drupal_get_path_alias(current_path()) == 'things-to-do') {
			print views_embed_view('recent_listings','thingstodo_recent');
		} else if (drupal_get_path_alias(current_path()) == 'dining') {
			print views_embed_view('recent_listings','dining_recent');
		}
	?>
	
	<?php
		// Add Listing/Event Category Squares
		if (drupal_get_path_alias(current_path()) == 'events') {
			print views_embed_view('listing_categories','event_categories');
		} else if (drupal_get_path_alias(current_path()) == 'things-to-do') {
			print views_embed_view('listing_categories','thingstodo_categories');
		} else if (drupal_get_path_alias(current_path()) == 'dining') {
			print views_embed_view('listing_categories','dining_categories');
		}
	?>

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article> <!-- /.node -->
