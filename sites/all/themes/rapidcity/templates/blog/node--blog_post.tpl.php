<?php
$array_all_blog_posts = array();


  //Fetch all locals here, so we don't do it every record -- single array of all local listings, with remote ids
  $post = 'blog_post';
  $current_nid = $node->nid;
  $tid = $node->field_blog_category['und'][0]['tid'];
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $post)
    ->fieldCondition('field_blog_category', 'tid', $tid)
    ->propertyCondition('status', NODE_PUBLISHED);
  $result = $query->execute();

  if (isset($result['node']))
  {
      $nids = array_keys($result['node']);
      $blog_post = node_load_multiple($nids);
      $output = node_view_multiple($blog_post);

      //add to array
      array_push($array_all_blog_posts, $blog_post);
  }
?>
<div class="container col-md-12">
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>

<header>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e91d0918b17a42"></script>
<!-- AddThis Button END -->


  <?php print render($content['field_masthead']); ?>
  <?php print render($content['field_blog_category']); ?>
  <div class="postdate-container">
	 <div class="authordate">  <?php print date( "F j, Y ",$node->created)?> - </div>  <?php print render($content['field_field_author_id']); ?>

  </div>

  		<h2><?php print $title; ?></h2>
</header>


  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['comments']);
    print render($content);
  ?>

<div class="addthis-container">
	  <!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
	<a class="addthis_button_print"></a>
	<a class="addthis_button_facebook"></a>
	<a class="addthis_button_twitter"></a>
	<a class="addthis_button_google_plusone_share"></a>
	<a class="addthis_button_pinterest_share"></a>
	<a class="addthis_button_compact"></a>
	</div>

</div>

<?php
$count = 0;
foreach($blog_post as $products){
 if ($products->nid == $current_nid){
  if ($count != 0){
    $previous_post = $count - 1;
    $next_post = $count + 1;
    $previous_node = array_slice($blog_post, $previous_post, 1);
    $next_node = array_slice($blog_post, $next_post, 1);
    $previous_nid = $previous_node[0]->nid;
    $next_nid = $next_node[0]->nid;
    ?>
    
 <div class="next-prev-container">   
<div class="previous-post">
  <?php if(!empty($previous_nid)) : ?>
    <a href="<?php print url("node/$previous_nid");?>"><img src="/sites/all/themes/rapidcity/images/blog/leftarrow.jpg" class="prevarrow" alt="leftarrow" width="" height="" /><?php print $previous_node[0]->title; ?></a>
  <?php endif; ?>
</div>
<div class="next-post">
  <?php if(!empty($next_nid)) : ?>
    <a href="<?php print url("node/$next_nid");?>"><?php print $next_node[0]->title; ?><img src="/sites/all/themes/rapidcity/images/blog/rightarrow.jpg" class="nextarrow" alt="rightarrow" width="" height="" /></a>
  <?php endif; ?>
</div>
 </div>
  <?php

  }
  break;
 }
 $count++;
 } ?>




</div>


<style>
			   
  #nid-2112 > div > div.recent-author-post > div.view.view-blog-roll.view-id-blog_roll.view-display-id-block_6{
	display: none;	
}

    #nid-2111 > div > div.recent-author-post > div.view.view-blog-roll.view-id-blog_roll.view-display-id-block_4{
	display: none;	
}
</style>

<div class="post-wrapper-container">
<div id="latest-post-wrapper">
 <h2>Meet the Author</h2>

	 <div class="author-feed col-md-12">

	  <?php
		$block = module_invoke('views', 'block_view', 'author-blog_author');
		print render($block['content']);
	 	?>
	 </div>
	<h2>latest blog posts</h2>

	<div class="latest-blog-category col-md-12">
	 <?php
		$block = module_invoke('views', 'block_view', 'latest_blog_post-block');
		print render($block['content']);
		?>
	</div>
</div>

</div>

<div class="comment-wrapper col-md-12">
  <?php print render($content['comments']); ?>
</div>
</article> <!-- /.node -->





<!-- Magnificant Popup-->
<script>
jQuery(document).ready(function ($) {
  $(".view-author .title > a").magnificPopup({
    type:"inline",
    removalDelay: 300,
    mainClass: 'mfp-fade',
    midClick: true,
    showCloseBtn: true
  });
});
</script>

<!-- SLICK SLIDER -->
 <link type="text/css" rel="stylesheet" href="/sites/all/themes/rapidcity/js/slick/slick/slick.css">
 <link type="text/css" rel="stylesheet" href="/sites/all/themes/rapidcity/js/slick/slick/slick-theme.css">


 <script src="/sites/all/themes/rapidcity/js/slick/slick/slick.js"></script>
 <script src="/sites/all/themes/rapidcity/js/slick/slick/slick.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
 $('.field-name-field-image-gallery > div').slick({
  infinite: true,
  speed: 500,
  focusOnSelect: true,
  fade: true,
  autoplay: true,
  autoplaySpeed: 5000,
  cssEase: 'linear'
  });
});
</script>
