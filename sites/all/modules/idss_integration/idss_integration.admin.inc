<?php

/**
 * @file
 * Original author: Aaron Sawyer
 * 
 */

/**
 * Menu callback; presents the sitemap settings page.
 */
function idss_integration_admin_settings_form() {
  
  // Field to set url
  $form['idss_integration_webservice_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Venue Webservice Url'),
    '#default_value' => variable_get('idss_integration_webservice_url', 'http://ws.idssasp.com/venue.asmx?wsdl'),
    '#description' => t('The IDSS SOAP Venue URL that will be pulled from, ex: http://ws.idssasp.com/venue.asmx?wsdl'),
  );
  $form['idss_integration_webservice_namespace'] = array(
    '#type' => 'textfield',
    '#title' => t('Venue Webservice Namespace Url'),
    '#default_value' => variable_get('idss_integration_webservice_namespace', 'http://ws.idssasp.com/venue.asmx'),
    '#description' => t('The IDSS SOAP Venue Namespace that will be used (ex: http://ws.idssasp.com/venue.asmx )'),
  );
  // Field to set url
  $form['idss_integration_webservice_members_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Members Webservice Url'),
    '#default_value' => variable_get('idss_integration_webservice_members_url', 'http://ws.idssasp.com/Members.asmx?wsdl'),
    '#description' => t('The IDSS SOAP Members (Venue owners) URL that will be pulled from, ex: http://ws.idssasp.com/Members.asmx?wsdl'),
  );
  $form['idss_integration_webservice_members_namespace'] = array(
    '#type' => 'textfield',
    '#title' => t('Members Webservice Namespace Url'),
    '#default_value' => variable_get('idss_integration_webservice_members_namespace', 'http://ws.idssasp.com/Members.asmx'),
    '#description' => t('The IDSS SOAP Members Namespace that will be used (ex: http://ws.idssasp.com/Members.asmx )'),
  );
  
  // Field to set username
  $form['idss_integration_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('idss_integration_username', ''),
    '#description' => t('The IDSS SOAP Username'),
  );
  // Field to set password
  $form['idss_integration_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('idss_integration_password', ''),
    '#description' => t('The IDSS SOAP Password'),
  );
  // Field to set site map message.
  //$site_map_message = variable_get('idss_integration_message', array('value' => '', 'format' => NULL));
  /*$form['idss_integration_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show front page'),
    '#default_value' => variable_get('site_map_show_front', 1),
    '#description' => t('When enabled, this option will include the front page in the site map.'),
  );*/

/*
$form['idss_integration_event_field_section'] = array(
    '#type' => 'fieldset',
    '#title' => t('Events Settings'),
  );
*/

$form['#suffix'] = "";
$vocabulary = taxonomy_get_vocabularies();
  $list_vocab_array = array(); /* Change to array('0' => '--none--'); if you want a none option*/
  foreach ($vocabulary as $item) {
  $key = $item->vid;
  $value = $item->name;
  $list_vocab_array[$key] = $value;
}

$form['idss_integration_venue_field_section']['idss_integration_venue_category_node'] = array(
    '#weight' => '1',
	  '#key_type' => 'associative',
	  '#multiple_toggle' => '1', 
	  '#type' => 'select',
	  '#options' => $list_vocab_array,
    '#title' => t('Select the local category node under which to sync with the remote venue categories:'),
    '#default_value' => variable_get('idss_integration_venue_category_node', 0),
    '#description' => t('Module will sync this base taxonomy with the remote IDSS'),
  );

/* datatype of event */
/*
$content_types = node_type_get_names();
  $list_content_types_array = array(); // Change to array('0' => '--none--'); if you want a none option
  foreach ($content_types as $item) {
  $key = str_replace(' ', '_', strtolower($item));
  $value =   $item;  
  $list_content_types_array[$key] = $value;
}

$form['idss_integration_event_field_section']['idss_integration_event_data_type'] = array(
    '#weight' => '1',
	  '#key_type' => 'associative',
	  '#multiple_toggle' => '1',
	  '#type' => 'select',
	  '#options' => $list_content_types_array,
    '#title' => t('Select the local event datatype to sync with remote IDSS events:'),
    '#default_value' => variable_get('idss_integration_event_data_type', 0),
    '#description' => t('Module will sync this base taxonomy with the remote IDSS'),
  );
*/
// Field to set event def id
  $form['idss_integration_event_def_exception'] = array(
    '#type' => 'textfield',
    '#title' => t('IDSS Venue Definition ID for Events'),
    '#default_value' => variable_get('idss_integration_event_def_exception', ''),
    '#description' => t('The IDSS Def ID for events. Events are completely different on IDSS, so we must treat them differently. No category mapping, no owner field grabbing.'),
  );

  $form['idss_integration_picture_field_names'] = array(
    '#type' => 'textfield',
    '#title' => t('IDSS Remote Venue field names for picture fields. Comma separated.'),
    '#default_value' => variable_get('idss_integration_picture_field_names', 'Picture 1,Picture 2,Picture 3,Picture 4,Picture 5'),
    '#description' => t('IDSS Venue field names for picture fields. Comma separated. These will be snatched from IDSS and placed on the server temporarily, then uploaded into drupal if they changed since last time.'),
  );

 $form['idss_integration_picture_desc_field_names'] = array(
    '#type' => 'textfield',
    '#title' => t('IDSS Venue Remote field names for picture description fields. Comma separated.'),
    '#default_value' => variable_get('idss_integration_picture_desc_field_names', 'Picture 1 Description,Picture 2 Description,Picture 3 Description,Picture 4 Description,Picture 5 Description'),
    '#description' => t('IDSS Venue field names for picture field alt tags. Comma separated. These will be matched with picture fields. Order must be the same as the picture fields!'),
  );
 $form['idss_integration_local_picture_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('IDSS Venue Local machine field name for picture field.'),
    '#default_value' => variable_get('idss_integration_local_picture_field_name', 'field_temp_drupal_photo'),
    '#description' => t('IDSS Venue Local machine field name for Drupal picture field.'),
  );



//exclude ids from taxonomies
$form['idss_integration_exclude_categories'] = array(
   '#type' => 'textfield',
   '#title' => t('IDSS Venue category ids to exclude from syncs. Comma separated.'),
   '#default_value' => variable_get('idss_integration_exclude_categories', '3775,3776,3777,3816,3818,3785,3783,3817,3784,3779,3780,3781,3782'),
   '#description' => t('IDSS category ids to exclude from syncs. Comma separated'),
 );
//exclude dt from syncs
$form['idss_integration_exclude_definitions'] = array(
   '#type' => 'textfield',
   '#title' => t('IDSS Venue Definition ids to exclude from syncs. Comma separated.'),
   '#default_value' => variable_get('idss_integration_exclude_definitions', '103,104'),
   '#description' => t('IDSS Venue Definition ids to exclude from syncs. Comma separated'),
 );

$form['idss_integration_automated_section'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automation'),
  );


$form['idss_integration_automated_section']['idss_integration_automated'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automated Vendor Listing Synchronization (downstream)?'),
    '#default_value' => variable_get('idss_integration_automated', 1),
    '#description' => t('When enabled, this option will enable automated Vendor listing syncs to this Drupal installation.'),
  );

//hour of day
$form['idss_integration_automated_section']['idss_integration_automated_hour'] = array(
       '#type' => 'select',
       '#title' => t('Hour of Day (uses crontab hour / min format)'),
       '#options' => array(
		  '*' => t('Every Hour'),
          0 => t('12:00am'),
          1 => t('1:00am'),
		  2 => t('2:00am'),
          3 => t('3:00am'),
		  4 => t('4:00am'),
		  5 => t('5:00am'),
		  6 => t('6:00am'),
          7 => t('7:00am'),
		  8 => t('8:00am'),
		  9 => t('9:00am'),
		  10 => t('10:00am'),
		  11 => t('11:00am'),
		  12 => t('12:00pm'),
		  13 => t('1:00pm'),
		  14 => t('2:00pm'),
		  15 => t('3:00pm'),
       ),
       '#default_value' =>  variable_get('idss_integration_automated_hour', 2),
       '#description' => t('Set this to an hour value to automatically sync IDSS at the hour specified'),
   );

//on minute
$form['idss_integration_automated_section']['idss_integration_automated_minute'] = array(
       '#type' => 'select',
       '#title' => t('Minute'),
       '#options' => array(
		  '*/2' => t('Every 2 Minutes (for Devel only)'),
		  '*/45' => t('Every 45 Minutes'),
          0 => t('[hour]:00 minutes'),
          15 => t('[hour]:15 minutes'),
		  30 => t('[hour]:30 minutes'),
          45 => t('[hour]:45 minutes'),
       ),
       '#default_value' =>  variable_get('idss_integration_automated_minute', 0),
       '#description' => t('Set this to a minute value to automatically sync IDSS at the minute value specified. Uses crontab format - ex. to set every x minutes, hour must be set to Every Hour'),
   );
/*
$form['#suffix'] = '<div class="yall"><h3>Event Data Type Structure</h3>';
$form['#suffix'] .= '<h3>Event Taxonomy Category Structure:</h3><p>create an integer field named "<strong>IDSS_ID</strong>"</p></div><br />';
$form['#suffix'] .= '<h3>Event Data Type Structure:</h3><p>create a new content datatype as such:';
$form['#suffix'] .= '<table><tr><td>' . '[IDSS_EventID]' . '</td><td>' . 'integer' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[IDSS_EventTypeID] (superfluous)' . '</td><td>' . 'text' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[IDSS_EventTypeName]' . '</td><td>' . 'text' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[Event Name] (machine name "title")' . '</td><td>' . 'text' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[Description] (machine name "body")' . '</td><td>' . 'long text and summary' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[Website]' . '</td><td>' . 'text' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[StatusName]?' . '</td><td>' . 'text' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[StartDate]' . '</td><td>' . 'date' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[EndDate]' . '</td><td>' . 'date' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[ArrivalDate]?' . '</td><td>' . 'date' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[DepartuerDate]?' . '</td><td>' . 'date' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[SetupDate]?' . '</td><td>' . 'date' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[TeardownDate]?' . '</td><td>' . 'date' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[Location]' . '</td><td>' . 'location' . '</td></tr>';
$form['#suffix'] .= '<tr><td>' . '[Category]' . '</td><td>' . 'term reference tree' . '</td></tr>';


$form['#suffix'] .= '</table>';
*/
/*
$form['#suffix'] .= '<div class="yall"><h3>Venue Listing Data Type Structure</h3>';
$form['#suffix'] .= '<h3>Venue Taxonomy Category Structure:</h3><ul><li> create an integer field named "<strong>IDSS_ID</strong>"</li><li>create an integer field named "<strong>IDSS_Parent_ID</strong>"</li></ul></div>';

*/










$event_cat_term_tree = taxonomy_get_tree(variable_get('idss_integration_event_category_node', 0)); //
//HAVE LOCAL CAT TREE, CAN COMPARE

if (count($event_cat_term_tree) > 0)
{
	//update / delete logic
}
else
{
	//add
}

//foreach ($tree as $term) {
 //$outgo .= $term->tid;
//}
//$form['#suffix'] .= print_r($tree, true);


$form['downloadSync'] = array(
  '#type' => 'submit',
  '#value' => 'Kickoff IDSS Download',
  '#submit' => array('manualFileDownloadSync')
);

  $form['manualSync'] = array(
    '#type' => 'submit',
    '#value' => 'Kickoff Sync',
    '#submit' => array('manualSync')
  );


  $form['imageSync'] = array(
    '#type' => 'submit',
    '#value' => 'Image Sync',
    '#submit' => array('imageSync')
  );


$form['#suffix'] .= "<div class=\"column-main\"><fieldset class=\"form-wrapper fieldset titled\">
      <legend><span class=\"fieldset-title fieldset-legend\">Logs</span></legend><div class=\"fieldset-content fieldset-wrapper clearfix\"><div class=\"\"><ul><li>Check the Drupal log for detailed events</li><li><a href=\"/sites/all/modules/idss_integration/log/idss_unified_log.txt\" target=\"blank\">Unified Log</a></li><li><a href=\"/sites/all/modules/idss_integration/log/idss_ran_cron.txt\" target=\"blank\">Automated Cron Log</li></ul></div></div></fieldset></div>";




/*
  $form['site_map_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site map content'),
  );
  $form['site_map_content']['site_map_show_front'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show front page'),
    '#default_value' => variable_get('site_map_show_front', 1),
    '#description' => t('When enabled, this option will include the front page in the site map.'),
  );
  if (module_exists('blog')) {
    $form['site_map_content']['site_map_show_blogs'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show active blog authors'),
      '#default_value' => variable_get('site_map_show_blogs', 1),
      '#description' => t('When enabled, this option will show the 10 most active blog authors.'),
    );
  }
  if (module_exists('book')) {
    $book_options = array();
    foreach (book_get_books() as $book) {
      $book_options[$book['mlid']] = $book['title'];
    }
    $form['site_map_content']['site_map_show_books'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Books to include in the site map'),
      '#default_value' => variable_get('site_map_show_books', array()),
      '#options' => $book_options,
      '#multiple' => TRUE,
    );
    $form['site_map_content']['site_map_books_expanded'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show books expanded'),
      '#default_value' => variable_get('site_map_books_expanded', 1),
      '#description' => t('When enabled, this option will show all children pages for each book.'),
    );
  }

  $menu_options = array();
  $menu_options = menu_get_menus();

  $form['site_map_content']['site_map_show_menus'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Menus to include in the site map'),
    '#default_value' => variable_get('site_map_show_menus', array()),
    '#options' => $menu_options,
    '#multiple' => TRUE,
  );
  if (module_exists('faq')) {
    $form['site_map_content']['site_map_show_faq'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show FAQ content'),
      '#default_value' => variable_get('site_map_show_faq', 0),
      '#description' => t('When enabled, this option will include the content from the FAQ module in the site map.'),
    );
  }
  $vocab_options = array();
  if (module_exists('taxonomy')) {
    foreach (taxonomy_get_vocabularies() as $vocabulary) {
      $vocab_options[$vocabulary->vid] = $vocabulary->name;
    }
  }
  $form['site_map_content']['site_map_show_vocabularies'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Categories to include in the site map'),
    '#default_value' => variable_get('site_map_show_vocabularies', array()),
    '#options' => $vocab_options,
    '#multiple' => TRUE,
  );

  $form['site_map_taxonomy_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Categories settings'),
  );
  $form['site_map_taxonomy_options']['site_map_show_count'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show node counts by categories'),
    '#default_value' => variable_get('site_map_show_count', 1),
    '#description' => t('When enabled, this option will show the number of nodes in each taxonomy term.'),
  );
  $form['site_map_taxonomy_options']['site_map_categories_depth'] = array(
    '#type' => 'textfield',
    '#title' => t('Categories depth'),
    '#default_value' => variable_get('site_map_categories_depth', 'all'),
    '#size' => 3,
    '#maxlength' => 10,
    '#description' => t('Specify how many subcategories should be included on the categorie page. Enter "all" to include all subcategories, "0" to include no subcategories, or "-1" not to append the depth at all.'),
  );
  $form['site_map_taxonomy_options']['site_map_term_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Category count threshold'),
    '#default_value' => variable_get('site_map_term_threshold', 0),
    '#size' => 3,
    '#description' => t('Only show categories whose node counts are greater than this threshold. Set to -1 to disable.'),
  );
  $form['site_map_taxonomy_options']['site_map_forum_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Forum count threshold'),
    '#default_value' => variable_get('site_map_forum_threshold', -1),
    '#size' => 3,
    '#description' => t('Only show forums whose node counts are greater than this threshold. Set to -1 to disable.'),
  );

  $form['site_map_rss_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('RSS settings'),
  );
  $form['site_map_rss_options']['site_map_rss_front'] = array(
    '#type' => 'textfield',
    '#title' => t('RSS feed for front page'),
    '#default_value' => variable_get('site_map_rss_front', 'rss.xml'),
    '#description' => t('The RSS feed for the front page, default is rss.xml.'),
  );
  $form['site_map_rss_options']['site_map_show_rss_links'] = array(
    '#type' => 'select',
    '#title' => t('Include RSS links'),
    '#default_value' => variable_get('site_map_show_rss_links', 1),
    '#options' => array(
      0 => t('None'),
      1 => t('Include on the right side'),
      2 => t('Include on the left side')
    ),
    '#description' => t('When enabled, this option will show links to the RSS feeds for each category and blog.'),
  );
  $form['site_map_rss_options']['site_map_rss_depth'] = array(
    '#type' => 'textfield',
    '#title' => t('RSS feed depth'),
    '#default_value' => variable_get('site_map_rss_depth', 'all'),
    '#size' => 3,
    '#maxlength' => 10,
    '#description' => t('Specify how many subcategories should be included in the RSS feed. Enter "all" to include all subcategories or "0" to include no subcategories.'),
  );

  $form['site_map_css_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS settings'),
  );
  $form['site_map_css_options']['site_map_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not include site map CSS file'),
    '#default_value' => variable_get('site_map_css', 0),
    '#description' => t("If you don't want to load the included CSS file you can check this box."),
  );

  // Make use of the Checkall module if it's installed.
  if (module_exists('checkall')) {
    $form['site_map_content']['site_map_show_books']['#checkall'] = TRUE;
    $form['site_map_content']['site_map_show_menus']['#checkall'] = TRUE;
    $form['site_map_content']['site_map_show_vocabularies']['#checkall'] = TRUE;
  }*/
  //echo "OH YEEEAH";
//if ($form['manualSync']->'#submit' => array('manualSync')
  
//$form['#suffix'] .= print_r($form['manualSync']);
/*
$form['manualSync'] = array(
    '#type' => 'submit',
    '#value' => 'Kickoff Sync',
    '#submit' => array('manualSync')
  );*/
	
  return system_settings_form($form);
}

function manualSync() {
	
	
	$params = array(
		"username" => variable_get('idss_integration_username', ''),
		"password" => variable_get('idss_integration_password', ''),
		"webservice_url" => variable_get('idss_integration_webservice_url', ''),
		"webservice_namespace" => variable_get('idss_integration_webservice_namespace', ''),
	);
	
	idss_integration_autoupdate_run($params); 
	
	//custom_drupal_mail('Rapid City', 'asawyer@boomyourbrand.com', 'manual', 'manual');
	drupal_set_message('Manual Sync Completed.');
  
	//print('<div id="yall">Manual Sync Kicked Off</div>');
}

function imageSync() {
	
	
	$params = array(
		"username" => variable_get('idss_integration_username', ''),
		"password" => variable_get('idss_integration_password', ''),
		"webservice_url" => variable_get('idss_integration_webservice_url', ''),
		"webservice_namespace" => variable_get('idss_integration_webservice_namespace', ''),
	);
	
	idss_integration_image_sync($params); 
	
	//custom_drupal_mail('Rapid City', 'asawyer@boomyourbrand.com', 'manual', 'manual');
	drupal_set_message('Image Sync Completed.');
  
}

function manualFileDownloadSync()
{
	idss_integration_download_file();
	drupal_set_message('Download of IDSS Data Completed.');
}

//unused now
function manualLatLon() {
	
	
	$params = array(
		"username" => variable_get('idss_integration_username', ''),
		"password" => variable_get('idss_integration_password', ''),
		"webservice_url" => variable_get('idss_integration_webservice_url', ''),
		"webservice_namespace" => variable_get('idss_integration_webservice_namespace', ''),
	);
	
	idss_integration_autoupdate_run($params); 
	
	//custom_drupal_mail('Rapid City', 'asawyer@boomyourbrand.com', 'manual', 'manual');
	drupal_set_message('Manual Sync Completed.');
  
	//print('<div id="yall">Manual Sync Kicked Off</div>');
}