<?php

/**
 * @file
 * Contains geofieldProximityGeocoder.
 */

class geofieldProximityGeocoder extends geofieldProximityBase implements geofieldProximityPluginInterface {
  public function option_definition(&$options, $views_plugin) {
    $options['geofield_proximity_geocoder'] = array('default' => '');
    $options['geofield_proximity_geocoder_engine'] = array('default' => 'google');
	$options['geofield_proximity_geocoder_html5_geolocation'] = array('default' => FALSE);
  }

  public function options_form(&$form, &$form_state, $views_plugin) {
    $form['geofield_proximity_geocoder'] = array(
      '#type' => 'geofield_geocoder_textfield',
      '#title' => t('Source'),
      '#default_value' => $views_plugin->options['geofield_proximity_geocoder'],
      '#dependency' => array(
        'edit-options-source' => array('geocoder'),
      ),
      '#proximity_plugin_value_element' => TRUE,
    );

    $geocoders_raw = geocoder_handler_info('text');
    $geocoder_options = array();
    foreach ($geocoders_raw as $key => $geocoder) {
      $geocoder_options[$key] = $geocoder['title'];
    }

    $form['geofield_proximity_geocoder_engine'] = array(
      '#type' => 'select',
      '#title' => t('Geocoding Service'),
      '#options' => $geocoder_options,
      '#default_value' => $views_plugin->options['geofield_proximity_geocoder_engine'],
      '#dependency' => array(
        'edit-options-source' => array('geocoder'),
      ),
    );
	
	$form['geofield_proximity_geocoder_html5_geolocation'] = array(
		'#type' => 'checkbox',
		'#title' => t('HTML5 Geolocate'),
		'#default_value' => $views_plugin->options['geofield_proximity_geocoder_html5_geolocation'],
		'#dependency' => array(
			'edit-options-source' => array('geocoder'),
		),
	);
  }
	
  public function value_form(&$form, &$form_state, $views_plugin) {
	$form['value']['#origin_element'] = 'geofield_geocoder_textfield';

	$geolocation = $views_plugin->options['geofield_proximity_geocoder_html5_geolocation'];
	$form['value']['#origin_options'] = array(
		'#geolocation' => $geolocation,
		'#geolocation_auto' => $geolocation && (empty($form_state['form_key']) || $form_state['form_key'] != 'config-item'),
		'#geolocation_auto_submit' => $geolocation && (empty($form_state['form_key']) || $form_state['form_key'] != 'config-item'),
	);
  }

  public function options_validate(&$form, &$form_state, $views_plugin) {
    if (!empty($form_state['values']['options']['geofield_proximity_geocoder']) && !geocoder($form_state['values']['options']['geofield_proximity_geocoder_engine'], $form_state['values']['options']['geofield_proximity_geocoder'])) {
      form_set_error('options][geofield_proximity_geocoder', t('Geocoder cannot find this location. Check your connection or add a findable location.'));
    }
  }

  public function getSourceValue($views_plugin) {
    $geocoder_engine = $views_plugin->options['geofield_proximity_geocoder_engine'];
	$value = (isset($views_plugin->value)) ? $views_plugin->value['origin'] : $views_plugin->options['geofield_proximity_geocoder'];

    if (!empty($value['lat']) && !empty($value['lon'])) {
      return array(
        'latitude' => $value['lat'],
        'longitude' => $value['lon'],
      );
    }
    elseif (!empty($value['location'])) {
      $geocoded_data_raw = geocoder($geocoder_engine, $value['location']);

      if ($geocoded_data_raw) {
        return array(
          'latitude' => $geocoded_data_raw->getY(),
          'longitude' => $geocoded_data_raw->getX(),
        );
      }
    }

    return FALSE;
  }

}
